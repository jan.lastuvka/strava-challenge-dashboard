# Strava challenge dashboard

## Setup
- Create `.env` file from `.env.template`
- Create `app.config.js` from `app.config.js.template`
- Run application and setup database

## Run application

To run the application in development, you can run the command:

```bash
docker-compose up -d
```
After that, open `http://localhost:8080` in your browser.

### Database schema

Connect to database (connection credentials in `.env.template`) and create following tables:

```sql
CREATE TABLE IF NOT EXISTS `activities` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `stravaId` bigint(11) NOT NULL,
    `athleteId` int(11) NOT NULL,
    `datetime` datetime NOT NULL,
    `startDatetime` datetime NOT NULL DEFAULT current_timestamp(),
    `name` varchar(100) COLLATE utf8_czech_ci NOT NULL,
    `distance` decimal(10,2) NOT NULL,
    `time` decimal(10,2) NOT NULL,
    `elevation` decimal(10,2) NOT NULL,
    `calories` decimal(10,2) NOT NULL,
    `sportType` varchar(50) COLLATE utf8_czech_ci NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;

CREATE TABLE IF NOT EXISTS `athletes` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `accessToken` varchar(200) COLLATE utf8_czech_ci NOT NULL,
    `refreshToken` varchar(200) COLLATE utf8_czech_ci NOT NULL,
    `stravaId` int(11) NOT NULL,
    `username` varchar(100) COLLATE utf8_czech_ci NOT NULL,
    `firstname` varchar(100) COLLATE utf8_czech_ci NOT NULL,
    `lastname` varchar(100) COLLATE utf8_czech_ci NOT NULL,
    `sex` varchar(1) COLLATE utf8_czech_ci NOT NULL,
    `avatar` varchar(200) COLLATE utf8_czech_ci NOT NULL,
    `department` varchar(5) COLLATE utf8_czech_ci NOT NULL,
    `joinDate` date NULL DEFAULT current_timestamp(),
    `team` enum('webnode','iubenda') COLLATE utf8_czech_ci NOT NULL DEFAULT 'webnode',
    PRIMARY KEY (`id`),
    KEY `stravaId` (`stravaId`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1;

CREATE TABLE IF NOT EXISTS `dayStatistics` (
    `id` int(11) NOT NULL AUTO_INCREMENT,
    `athleteId` int(11) NOT NULL,
    `category` int(11) NOT NULL,
    `date` date NOT NULL,
    `distance` decimal(10,2) NOT NULL DEFAULT 0.00,
    `time` decimal(10,2) NOT NULL DEFAULT 0.00,
    `calories` decimal(10,2) NOT NULL DEFAULT 0.00,
    `elevation` decimal(10,2) NOT NULL DEFAULT 0.00,
    `activitiesCount` int(11) NOT NULL DEFAULT 0,
    PRIMARY KEY (`id`),
    UNIQUE KEY `athletePerDay` (`athleteId`,`date`,`category`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_czech_ci AUTO_INCREMENT=1 ;
```

Run this command in the application directory to run the test suite

```bash
docker run --rm -v $(pwd):/app composer/composer:latest tests
docker run --rm -v $(pwd):/app composer/composer:latest phpstan
docker run --rm -v $(pwd):/app composer/composer:latest phpcs
docker run --rm -v $(pwd):/app composer/composer:latest phpcs-fix
docker run --rm -v $(pwd):/app composer/composer:latest phpcs-tests
docker run --rm -v $(pwd):/app composer/composer:latest phpcs-tests-fix
```
