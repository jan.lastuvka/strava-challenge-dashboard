<?php

declare(strict_types=1);

use App\Application\Actions\Join\JoinChallengeAction;
use App\Domain\Activity\ActivityProcessor;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Settings\SettingsRepositoryInterface;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use App\Infrastructure\API\Strava\StravaAPIClient;
use App\Infrastructure\OAuth2\StravaOAuth2Provider;
use DI\ContainerBuilder;
use GuzzleHttp\Client;
use Monolog\Handler\PushoverHandler;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;
use Monolog\Processor\UidProcessor;
use Psr\Container\ContainerInterface;
use Psr\Log\LoggerInterface;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        LoggerInterface::class => function (ContainerInterface $c) {
            $settings = $c->get(SettingsRepositoryInterface::class);

            $loggerSettings = $settings->get('logger');
            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new StreamHandler($loggerSettings['path'], $loggerSettings['level']);
            $logger->pushHandler($handler);

            return $logger;
        },

        StravaOAuth2Provider::class => function (ContainerInterface $c) {
            /** @var SettingsRepositoryInterface $settings */
            $settings = $c->get(SettingsRepositoryInterface::class);

            return new StravaOAuth2Provider([
                'clientId'  => $settings->get('strava')['clientId'] ?? '',
                'clientSecret' => $settings->get('strava')['clientSecret'] ?? '',
            ]);
        },

        PDO::class => function (ContainerInterface $c) {
            /** @var SettingsRepositoryInterface $settings */
            $settings = $c->get(SettingsRepositoryInterface::class);
            $dbCfg = $settings->get('db', []);

             $dsn = sprintf(
                 'mysql:dbname=%s;host=%s',
                     $dbCfg['database'] ?? '',
                     $dbCfg['host'] ?? ''
             );
            $user = $dbCfg['user'] ?? '';
            $password = $dbCfg['password'] ?? '';

            return new PDO($dsn, $user, $password);
        },

        StravaAPIClient::class => function(ContainerInterface $c) {
            $settings = $c->get(SettingsRepositoryInterface::class);

            $loggerSettings = $settings->get('logger');
            $pushOverKey = $settings->get('pushoverKey');
            $pushoverUsers = $settings->get('pushoverUsers');

            $logger = new Logger($loggerSettings['name']);

            $processor = new UidProcessor();
            $logger->pushProcessor($processor);

            $handler = new PushoverHandler($pushOverKey, $pushoverUsers, title: 'Challenge App');
            $logger->pushHandler($handler);

            return new StravaAPIClient(
                $c->get(Client::class),
                $c->get(AthleteRepositoryInterface::class),
                $c->get(StravaOAuth2Provider::class),
                $logger
            );
        },

        JoinChallengeAction::class => function(ContainerInterface $c) {
            /** @var SettingsRepositoryInterface $settings */
            $settings = $c->get(SettingsRepositoryInterface::class);

            $challengeStartDate = \DateTimeImmutable::createFromFormat(
                'Y-m-d',
                $settings->get('challengeStartDate')
            );

            return new JoinChallengeAction(
                $c->get(LoggerInterface::class),
                $c->get(SettingsRepositoryInterface::class),
                $c->get(StravaOAuth2Provider::class),
                $c->get(AthleteRepositoryInterface::class),
                $c->get(ActivityProcessor::class),
                $c->get(StravaAPIClient::class),
                $c->get(DayStatisticsRepositoryInterface::class),
                $challengeStartDate
            );
        },
    ]);
};
