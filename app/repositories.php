<?php

declare(strict_types=1);

use App\Domain\Activity\ActivityRepositoryInterface;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use App\Infrastructure\Persistence\Activity\ActivityPdoRepository;
use App\Infrastructure\Persistence\Athlete\AthletePdoRepository;
use App\Infrastructure\Persistence\Statistics\DayStatisticsPdoRepository;
use DI\ContainerBuilder;

return function (ContainerBuilder $containerBuilder) {
    $containerBuilder->addDefinitions([
        AthleteRepositoryInterface::class => \DI\autowire(AthletePdoRepository::class),
        ActivityRepositoryInterface::class => \DI\autowire(ActivityPdoRepository::class),
        DayStatisticsRepositoryInterface::class => \DI\autowire(DayStatisticsPdoRepository::class),
    ]);
};
