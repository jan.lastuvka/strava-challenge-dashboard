<?php

declare(strict_types=1);

use App\Application\Actions\Athlete\AthleteDayStatisticsAction;
use App\Application\Actions\Challenge\ActivitiesStatsAction;
use App\Application\Actions\Challenge\ChallengeAction;
use App\Application\Actions\Join\JoinChallengeAction;
use App\Application\Actions\Statistics\TeamStatisticsAction;
use App\Application\Actions\Strava\WebhookProcessAction;
use App\Application\Actions\Strava\WebhookVerifyAction;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\App;
use Slim\Interfaces\RouteCollectorProxyInterface as Group;

return function (App $app) {
    $app->options('/{routes:.*}', function (Request $request, Response $response) {
        // CORS Pre-Flight OPTIONS Request Handler
        return $response;
    });

    $app->get('/', function (Request $request, Response $response) {
        $response->getBody()->write(
            'Find out more about me here: https://gitlab.com/jan.lastuvka/strava-challenge-dashboard'
        );
        return $response;
    });

    $app->group('/athletes', function (Group $group) {
        $group->get('/{athleteId}/dayStatistics', AthleteDayStatisticsAction::class);
    });

    $app->group('/join', function (Group $group) {
        $group->get('', JoinChallengeAction::class);
    });

    $app->group('/challenge', function (Group $group) {
        $group->get('', ChallengeAction::class);
        $group->get('/activitiesStats', ActivitiesStatsAction::class);
    });

    $app->group('/stravaWebhook', function (Group $group) {
        $group->get('', WebhookVerifyAction::class);
        $group->post('', WebhookProcessAction::class);
    });

	$app->group('/statistics', function (Group $group) {
		$group->get('/team', TeamStatisticsAction::class);
	});
};
