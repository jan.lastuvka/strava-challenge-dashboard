<?php

declare(strict_types=1);

use App\Domain\Settings\SettingsRepositoryInterface;
use App\Infrastructure\Settings\InMemorySettingsRepository;
use DI\ContainerBuilder;
use Monolog\Logger;

return function (ContainerBuilder $containerBuilder) {
    // Global Settings Object
    $containerBuilder->addDefinitions([
        SettingsRepositoryInterface::class => function () {
            return new InMemorySettingsRepository([
                'devMode' => (bool)($_ENV['DEV_MODE'] ?? false),
                'displayErrorDetails' => (bool)($_ENV['DEV_MODE'] ?? false),
                'logError' => true,
                'logErrorDetails' => true,
                'logger' => [
                    'name' => 'app',
                    'path' => isset($_ENV['docker']) ? 'php://stdout' : __DIR__ . '/../logs/app.log',
                    'level' => Logger::DEBUG,
                ],
                'strava' => [
                    'clientId' => $_ENV['STRAVA_CLIENT_ID'] ?? null,
                    'clientSecret' => $_ENV['STRAVA_CLIENT_SECRET'] ?? null,
                    'webhookSecret' => $_ENV['STRAVA_WEBHOOK_SECRET'] ?? null,
                    'subscriptionId' => $_ENV['STRAVA_SUBSCRIPTION_ID'] ?? null,
                ],
                'appUrl' => $_ENV['APP_URL'] ?? '',
                'db' => [
                    'host' => $_ENV['DB_HOST'] ?? '',
                    'database' => $_ENV['DB_DATABASE'] ?? '',
                    'port' => $_ENV['DB_PORT'] ?? '',
                    'user' => $_ENV['DB_USER'] ?? '',
                    'password' => $_ENV['DB_PASSWORD'] ?? '',
                ],

                'pushoverKey' => $_ENV['PUSHOVER_TOKEN'] ?? '',
                'pushoverUsers' => [$_ENV['PUSHOVER_USER_TOKEN'] ?? ''],
                'challengeStartDate' => $_ENV['CHALLENGE_START_DATE'] ?? '',
            ]);
        }
    ]);
};
