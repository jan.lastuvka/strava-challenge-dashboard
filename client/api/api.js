import {API_URL} from "@/app.config";

export const getChallengeData = (from, to, orderBy) => {
    return fetch(
        API_URL + '/challenge?from='+from+'&to='+to+'&categories[]=run&categories[]=bike&orderBy='+orderBy
    )
        .then(response => response.json())
        .catch(error => console.error(error));
};

export const getChallengeActivitiesData = (from, to) => {
    return fetch(
        API_URL + '/challenge/activitiesStats?from='+from+'&to='+to
    )
        .then(response => response.json())
        .catch(error => console.error(error));
};

export const getAthleteDayStatistics = (id, from, to, category) => {
    return fetch(
        API_URL + '/athletes/'+id+'/dayStatistics?from='+from+'&to='+to+'&category='+category
    )
        .then(response => response.json())
        .catch(error => console.error(error));
};

export const getTeamYearDistanceStatistic = (team, year) => {
    return fetch(
        API_URL + '/statistics/team?team='+team+'&year='+year
    )
        .then(response => response.json())
        .catch(error => console.error(error));
};
