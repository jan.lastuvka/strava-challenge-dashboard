import React from 'react';
import styles from '@/client/styles/Modals.module.css'
import StatsRow from "@/client/components/StatsRow";
import DepartmentBadge from "@/client/components/DepartmentBadge";
import NameTitle from "@/client/components/NameTitle";
import Avatar from "@/client/components/Avatar";
import Distance from "@/client/components/Distance";
import {getAthleteDayStatistics} from "@/client/api/api";
import Workouts from "@/client/components/Workouts";

export default class AthleteDetailModal extends React.Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            dayStatistic: [],
        };
    }

    componentDidMount() {
        getAthleteDayStatistics(
            this.props.athlete.athlete.id,
            this.props.challengeFrom,
            this.props.challengeTo,
            this.props.category
        ).then(
            json => this.setState({
                dayStatistic: json.data,
                loading: false,
            })
        );
    }

    render() {
        let {athlete, category, onClose} = this.props;

        const {distance, time, calories, elevation, activitiesCount} = athlete;
        const {avatar, name, department} = athlete.athlete;

        return (
            <div className={`modal ${styles.modalOverlay}`} style={{display: 'block'}}>
                <div className={`modal-dialog modal-dialog-centered`}>
                    <div className={`modal-content ${styles.modal}`}>
                        <div className={`modal-header ${styles.header}`}>
                            <button type="button" className="btn-close btn-close-white"
                                    onClick={() => onClose()}></button>
                        </div>
                        <div className={`modal-body ${styles.athleteDetailModalBody}`}>
                            <div className={`d-flex gap-3 ${styles.athleteDetail} mb-4`}>
                                <div className='d-flex justify-content-center mt-2'>
                                    <Avatar avatar={avatar} width={60} height={60} />
                                </div>

                                <div>
                                    <div className='d-flex gap-2'>
                                        <div className='d-inline-block'><NameTitle name={name} size={24} /></div>
                                        <div className='d-inline-block'><DepartmentBadge department={department}/></div>
                                    </div>

                                    {this.props.challengeMetric === 'activitiesCount' 
                                        ?
                                            <Workouts count={activitiesCount} showIcon={true} category={category} />
                                        :
                                            <Distance distance={distance} showIcon={true} category={category} />
                                    }
                                </div>
                            </div>

                            <div className={styles.athleteStats}>
                                <StatsRow time={time} calories={calories} elevation={elevation} compact={true} />
                            </div>

                            {this.state.loading && <div className='mt-5'>
                                <div className="d-flex justify-content-center">
                                    <div className="spinner-border" role="status">
                                        <span className="visually-hidden">Loading...</span>
                                    </div>
                                </div>
                            </div>}

                            <div className='mt-4'>
                                {this.state.dayStatistic.map((item, key) => {
                                    const date = new Date(item.date);
                                    const month = date.getMonth() + 1;
                                    const day = date.getDate();

                                    return (
                                        <div className={`d-flex justify-content-between ${styles.statsRow}`} key={key}>
                                            <span>{day}.{month}.</span>
                                            <span className={styles.statsItem}>
                                                {item[this.props.challengeMetric]}
                                                {this.props.challengeMetric === 'distance' && ' km'}
                                                {this.props.challengeMetric === 'calories' && ' Cal'}
                                                {this.props.challengeMetric === 'time' && ' min'}
                                                {this.props.challengeMetric === 'elevation' && ' m'}
                                                {this.props.challengeMetric === 'activitiesCount' && ' x'}
                                            </span>
                                        </div>
                                    );
                                })}
                            </div>

                        </div>

                    </div>
                </div>
            </div>
        );
    }
}
