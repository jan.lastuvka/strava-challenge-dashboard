import React from 'react';
import styles from "@/client/styles/Avatar.module.css";

export default function Avatar({avatar, width, height, isBig = false}) {
    let classList = styles.avatar;

    if (isBig)
    {
        classList = classList + ' ' + styles.avatarBig;
    }

    return (
        <>
            {avatar ?
                <img className={classList} src={avatar} width={width} height={height} />
                :
                <div className={classList} style={{width, height}} />
            }
        </>
    );
}
