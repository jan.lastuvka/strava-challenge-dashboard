import React from 'react';
import styles from '@/client/styles/Buttons.module.css'
import RunIcon from "@/client/components/RunIcon";
import BikeIcon from "@/client/components/BikeIcon";
import WorkoutIcon from "@/client/components/WorkoutIcon";

export default function CategoryButton({category, onChange, categories}) {
    const runBtnClass = category === 'run' ? styles.btnCategoryActive : '';
    const bikeBtnClass = category === 'bike' ? styles.btnCategoryActive : '';
    const workoutBtnClass = category === 'workouts' ? styles.btnCategoryActive : '';

    return (
        <div className={`btn-group ${styles.btnGroupCategory}`} role="group">
            {categories.includes('run') && <button
                type="button"
                className={`btn ${styles.btnCategory} ${runBtnClass}`}
                onClick={() => onChange('run')}
            >
                <RunIcon width={20} height={20} fill={category === 'run' ? '#0D1C2C' : '#D1D6E0'} />
                Running
            </button>}
            {categories.includes('bike') && <button
                type="button"
                className={`btn ${styles.btnCategory} ${bikeBtnClass}`}
                onClick={() => onChange('bike')}
            >
                <BikeIcon width={20} height={20} fill={category === 'bike' ? '#0D1C2C' : '#D1D6E0'} />
                Cycling
            </button>}
            {categories.includes('workouts') && <button
                type="button"
                className={`btn ${styles.btnCategory} ${workoutBtnClass}`}
                onClick={() => onChange('workouts')}
            >
                <WorkoutIcon width={20} height={20} fill={category === 'workouts' ? '#0D1C2C' : '#D1D6E0'} />
                Workouts
            </button>}
        </div>
    );
}
