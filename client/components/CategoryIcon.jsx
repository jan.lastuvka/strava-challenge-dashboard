import React from 'react';
import RunIcon from "@/client/components/RunIcon";
import BikeIcon from "@/client/components/BikeIcon";
import WorkoutIcon from "@/client/components/WorkoutIcon";

export default function CategoryIcon({category}) {
    return (
        <>
            {category === 'run' && <RunIcon width={17} height={22} />}
            {category === 'bike' && <BikeIcon width={21} height={20} />}
            {category === 'workouts' && <WorkoutIcon width={21} height={20} />}
        </>
    );
}
