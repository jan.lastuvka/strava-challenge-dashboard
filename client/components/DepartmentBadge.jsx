import React from 'react';
import styles from '@/client/styles/Badges.module.css'

export default function DepartmentBadge({department, compact = false}) {
    const compactClass = compact ? styles.badgeDepartmentCompact : '';

    return (
        <span className={`badge ${styles.badgeDepartment} ${compactClass}`}>{department}</span>
    );
}
