import React from 'react';
import styles from "@/client/styles/Distance.module.css";
import CategoryIcon from "@/client/components/CategoryIcon";

export default function Distance({distance, showIcon, category, compact = false}) {
    let classList = styles.distance;

    if (compact)
    {
        classList = classList + ' ' + styles.distanceCompact;
    }

    if (showIcon)
    {
        classList = classList + ' ' + styles.distanceWithIcon;
    }

    return (
        <>
            {showIcon && <CategoryIcon category={category} />}
            <span className={classList}>{distance} km</span>
        </>
    );
}
