import React from 'react';

import styles from "@/client/styles/Firework.module.css";

export default function Firework({challengeEndDate}) {
    const today = new Date();
    today.setHours(0, 0, 0);
    const endDate = new Date(challengeEndDate);
    endDate.setHours(23, 59, 59);

    if (endDate >= today)
    {
        return (<></>);
    }

    return (
        <>
            <div className={`${styles.firework}`} />
            <div className={`${styles.firework} ${styles.secondFirework}`} />
            <div className={`${styles.firework} ${styles.thirdFirework}`} />
        </>
    );
}
