import React from 'react';
import styles from '@/client/styles/Buttons.module.css'

export default function JoinButton({activeTeam, compact}) {
    return (
        <a className={`btn ${styles.btnJoin}`} href={`/join?team=${activeTeam}`}>
            {compact ? 'Join challenge' : <>Join {activeTeam} team</>}
        </a>
    );
}
