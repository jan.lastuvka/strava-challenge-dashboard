import React, {useEffect, useState} from 'react';
import styles from "@/client/styles/Notifications.module.css";
import {useSearchParams} from "next/navigation";

export default function JoinUsNotification() {
    const searchParams = useSearchParams();

    const joined = searchParams.has('joined');

    const [hide, setHide] = useState(false);

    useEffect(() => {
        setTimeout(() => setHide(true), 10000);
    }, []);

    if (!joined || hide)
    {
        return <></>;
    }

    return (
        <div className={styles.message}>
            <div className={`alert alert-success ` + styles.alert}>
                Successfully connected
            </div>
        </div>
    );
}
