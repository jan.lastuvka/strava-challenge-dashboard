import React from 'react';
import styles from '@/client/styles/Cards.module.css'
import DepartmentBadge from "@/client/components/DepartmentBadge";
import StatsRow from "@/client/components/StatsRow";
import Avatar from "@/client/components/Avatar";
import NameTitle from "@/client/components/NameTitle";
import Distance from "@/client/components/Distance";
import Place from "@/client/components/Place";
import Workouts from "@/client/components/Workouts";


export default function LoserCard({id, name, department, place, avatar, time, calories, elevation, distance, activitiesCount, onClick, category, challengeOrderBy}) {
    return (
        <div className={`card ${styles.cardLoser}`} key={id} onClick={() => onClick(id)}>
            <div className='row align-items-center'>
                <div className='col-12 col-lg-1 mb-2 mb-lg-0'>
                    <div className='d-flex justify-content-between'>
                        <Place place={place} compact={true} />

                        <div className='d-lg-none'>
                            <DepartmentBadge department={department} />
                        </div>
                    </div>
                </div>

                <div className='col-3 col-lg-1'>
                    <Avatar avatar={avatar} width={50} height={50} />
                </div>

                <div className='col-9 col-lg-4'>
                    <NameTitle name={name} size={15} compact={true} />
                    {challengeOrderBy === 'activitiesCount' 
                        ?
                            <Workouts count={activitiesCount} category={category} showIcon={false} compact={true} />
                        :
                            <Distance distance={distance} category={category} showIcon={false} compact={true} />
                    }
                </div>

                <div className='col-2 d-none d-lg-block'>
                    <DepartmentBadge department={department} />
                </div>

                <div className='col-12 col-lg-4 mt-3 mt-lg-0'>
                    <StatsRow time={time} calories={calories} elevation={elevation} />
                </div>
            </div>
        </div>
    );
}
