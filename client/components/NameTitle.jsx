import React from 'react';
import styles from "@/client/styles/NameTitle.module.css";
import {Racing_Sans_One} from "next/font/google";

const racingSansOne = Racing_Sans_One({ subsets: ['latin'], weight: '400' });

export default function NameTitle({name, size, compact = false, noWrap = false}) {
    let classList = null;

    if (!compact)
    {
        classList = `${racingSansOne.className} ${styles.name}`;
    }
    else
    {
        classList = styles.nameCompact;
    }

    if (noWrap)
    {
        classList = classList + ' ' + styles.nowrap;
    }

    return (
        <h3
            className={classList}
            style={{fontSize: size}}
        >
            {name}
        </h3>
    );
}
