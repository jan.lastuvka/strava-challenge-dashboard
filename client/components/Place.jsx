import React from 'react';
import styles from "@/client/styles/Place.module.css";

export default function Place({place, compact}) {
    const places = ['Gold', 'Silver', 'Bronze'];
    const placeColor = ['#F0CB6E', '#DAE0E2', '#CA9163'];

    let placeSuffix = 'th';
    if (place <= 2 || place >= 20)
    {
        switch (place % 10)
        {
            case 0:
                placeSuffix = 'st';
                break;
            case 1:
                placeSuffix = 'nd';
                break;
            case 2:
                placeSuffix = 'rd';
                break;
        }
    }

    return (
        <>
            {compact
                ?
                <div>
                    <span className={styles.placeCompact}>{place + 1}{placeSuffix}</span>
                </div>
                :
                <div className={styles.place}>
                    <span className={styles.ellipse} style={{backgroundColor: placeColor[place]}}></span>
                    <span>{places[place]}</span>
                </div>
            }
        </>
    );
}
