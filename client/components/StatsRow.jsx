import React from 'react';
import styles from '@/client/styles/Stats.module.css'
import TimerIcon from "@/client/components/TimerIcon";
import FireIcon from "@/client/components/FireIcon";
import AltitudeIcon from "@/client/components/AltitudeIcon";

export default function StatsRow({time, calories, elevation, compact = false}) {
    const hours = Math.floor(time / 60);
    const minutes = Math.round(time % 60);
    const contentClass = compact ? 'gap-4' : 'justify-content-between';

    return (
        <div className={`d-flex ${contentClass} ${styles.stats}`}>
            <div className={styles.statsItem}>
                <TimerIcon />
                {hours}h {minutes}m
            </div>
            <div className={styles.statsItem}>
                <FireIcon />
                {calories} Cal
            </div>
            <div className={styles.statsItem}>
                <AltitudeIcon />
                {elevation} m
            </div>
        </div>
    );
}
