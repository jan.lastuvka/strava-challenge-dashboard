import React from 'react';
import pageStyles from "@/client/styles/Page.module.css";
import TeamLogo from "@/client/components/TeamLogo";
import RunIcon from "@/client/components/RunIcon";
import BikeIcon from "@/client/components/BikeIcon";
import styles from '@/client/styles/TeamsStatistics.module.css'
import Place from "@/client/components/Place";
import statsStyles from '@/client/styles/Stats.module.css'

export default function TeamsStatistics({teams, athletes, challengeMetric}) {
    const teamsStatistics = {};
    teams.forEach((team) => {
        teamsStatistics[team] = {run: 0, bike: 0, total: 0, team};
    });

    if (athletes['run'])
    {
        athletes['run'].forEach((athlete) => {
            const team = athlete.athlete.team;
            const add = athlete[challengeMetric];
            teamsStatistics[team]['run'] += add;
            teamsStatistics[team]['total'] += add;
        });
    }

    if (athletes['bike'])
    {
        athletes['bike'].forEach((athlete) => {
            const team = athlete.athlete.team;
            const add = athlete[challengeMetric];
            teamsStatistics[team]['bike'] += add;
            teamsStatistics[team]['total'] += add;
        });
    }

    let unit = '';
    switch (challengeMetric)
    {
        case 'distance':
            unit = 'km'; break;
        case 'calories':
            unit = 'Cal'; break;
        case 'time':
            unit = 'min'; break;
        case 'elevation':
            unit = 'm'; break;
    }

    const teamsFinal = Object.values(teamsStatistics);
    teamsFinal.sort((a, b) => {
        return b.total - a.total;
    });

    return (
        <div className={`row mb-4`}>
            <div className='col-12'>
                <h2 className={pageStyles.subTitle}>Teams</h2>

                {teamsFinal.map((data, place) => {
                    const team = data.team;
                    const run = Math.round(data.run);
                    const bike = Math.round(data.bike);
                    const total = run + bike;
                    return (
                        <div className={`card ${styles.cardTeam}`}>
                            <div className='row align-items-center'>
                                <div className={`col-6 col-sm-6 col-md-4 col-lg-3`}>
                                    <div className='d-flex justify-content-between justify-content-sm-start gap-3 align-items-center'>
                                        <div className='d-none d-sm-block'><Place place={place} compact={true} /></div>
                                        <div className={`${styles.teamLogo}`}>
                                            <TeamLogo team={team} />
                                        </div>
                                    </div>
                                </div>

                                <div className={`col-6 col-sm-6 col-md-4 col-lg-6 ${styles.total}`}>
                                    {total} {unit}
                                </div>

                                <div className={`col-12 col-md-4 col-lg-3 mt-2 mt-sm-0`}>
                                    <div className={`d-flex justify-content-between ${statsStyles.stats}`}>
                                        <div className={statsStyles.statsItem}>
                                            <RunIcon width={20} height={20} fill={'#A2AFC6'} /> {run} {unit}
                                        </div>
                                        <div className={statsStyles.statsItem}>
                                            <BikeIcon width={20} height={20} fill={'#A2AFC6'} /> {bike} {unit}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                })}
            </div>
        </div>
    );
}
