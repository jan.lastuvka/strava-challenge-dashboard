import React from 'react';
import buttonStyles from '@/client/styles/Buttons.module.css'
import classNames from "classnames";
import TeamLogo from "@/client/components/TeamLogo";

export default function TeamsSwitcher({teams, activeTeam, changeTeam}) {
    if (teams.length === 1 && teams.includes('webnode')) {
        return <TeamLogo team={'webnode'} variant={'light'} />;
    }

    return (
        <div className={`btn-group ${buttonStyles.btnGroupTeams} mb-2`} role="group">
            {teams.includes('webnode') &&
                <button
                    type="button"
                    className={classNames(
                        'btn',
                        buttonStyles.btnTeam,
                        {[`${buttonStyles.btnTeamActive}`]: activeTeam === 'webnode'}
                    )}
                    onClick={() => changeTeam('webnode')}
                >
                    <TeamLogo team={'webnode'} />
                </button>
            }
            {teams.includes('iubenda') &&
                <button
                    type="button"
                    className={classNames(
                        'btn',
                        buttonStyles.btnTeam,
                        {[`${buttonStyles.btnTeamActive}`]: activeTeam === 'iubenda'}
                    )}
                    onClick={() => changeTeam('iubenda')}
                >
                    <TeamLogo team={'iubenda'} />
                </button>
            }
        </div>
    );
}
