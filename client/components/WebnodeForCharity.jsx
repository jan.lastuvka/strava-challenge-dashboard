import React from 'react';
import styles from '@/client/styles/WebnodeForCharity.module.css'
import {CHALLENGE_CHARITY_LINK} from "@/app.config";
import classNames from "classnames";

export default function WebnodeForCharity({
    yearTotalDistance,
    yearGoal,
    challengeTotalDistance,
    challengeGoal
}) {
    const moneyYearGoal = yearGoal * 5;
    const totalYearMoney = Math.min(yearTotalDistance * 5, moneyYearGoal);
    const yearProgress = Math.min(100, yearTotalDistance / (yearGoal / 100)).toFixed(0);

    let totalChallengeMoney = 0;
    let moneyChallengeGoal = 0;
    let challengeProgress = 0;

    if (challengeGoal !== null) {
        totalChallengeMoney = challengeTotalDistance * 5;
        moneyChallengeGoal = challengeGoal * 5;
        challengeProgress = Math.min(100, challengeTotalDistance / (challengeGoal / 100)).toFixed(0);
    }

    const challengeCharityLink = CHALLENGE_CHARITY_LINK;

    return (
        <div className={'row mb-5'}>
            <div className={classNames(
                'col-12 col-md-6 col-lg-4',
                {
                    'd-none d-md-block': challengeGoal !== null
                }
            )}>
                <div className={styles.wrapper}>
                    <h2 className={styles.title}>Raised for charity:</h2>
                    <h3 className={styles.total}>
                        {totalYearMoney} Kč <span className={styles.goalNote}>/ {moneyYearGoal} Kč</span>
                    </h3>
                    <div className={styles.progressLine}>
                        <div className={styles.progress} style={{width: yearProgress + '%'}}></div>
                    </div>
                </div>
            </div>
            {challengeGoal === null && <div className={'col-6 col-lg-4 d-none d-md-block'}>
                <div className={styles.wrapper}>
                    <h2 className={styles.title}>Km total:</h2>
                    <h3 className={styles.total}>
                        {yearTotalDistance} km <span className={styles.goalNote}>/ {yearGoal} km</span>
                    </h3>
                    <div className={styles.progressLine}>
                        <div className={styles.progress} style={{width: yearProgress + '%'}}></div>
                    </div>
                </div>
            </div>}
            {challengeGoal !== null && <div className={'col-12 col-md-6 col-lg-4'}>
                <div className={styles.wrapper}>
                    <h2 className={styles.title}>
                        Challenge for a good cause
                        {challengeCharityLink && <>: <a href={challengeCharityLink} target="_blank">Link</a></>}
                    </h2>
                    <h3 className={styles.total}>
                        {totalChallengeMoney} Kč <span className={styles.goalNote}>/ {moneyChallengeGoal} Kč</span>
                    </h3>
                    <div className={styles.progressLine}>
                        <div className={styles.progress} style={{width: challengeProgress + '%'}}></div>
                    </div>
                </div>
            </div>}
        </div>
    );
}
