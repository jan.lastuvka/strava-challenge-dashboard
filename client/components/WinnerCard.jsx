import React from 'react';
import styles from '@/client/styles/Cards.module.css'
import DepartmentBadge from "@/client/components/DepartmentBadge";
import StatsRow from "@/client/components/StatsRow";
import Avatar from "@/client/components/Avatar";
import NameTitle from "@/client/components/NameTitle";
import Distance from "@/client/components/Distance";
import Place from "@/client/components/Place";
import Workouts from "@/client/components/Workouts";

export default function WinnerCard({id, name, department, place, avatar, time, calories, elevation, category, distance, activitiesCount, onClick, challengeOrderBy}) {
    const cardClass = place === 0 ? styles.cardFirstPlace : '';
    const classes = [styles.gold, styles.silver, styles.bronze];

    return (
        <div className={`card ${styles.cardWinner} ${cardClass} ${classes[place]}`} onClick={() => onClick(id)} key={id}>
            <div className={styles.header}>
                <Place place={place} />
                <div>
                    <DepartmentBadge department={department} />
                </div>
            </div>

            <div className='d-flex flex-nowrap flex-lg-wrap justify-content-start gap-3 gap-lg-0 justify-content-lg-center'>
                <div className='d-flex justify-content-center mt-3 mt-lg-2 mb-4'>
                    <Avatar avatar={avatar} width={124} height={124} isBig={true} />
                </div>

                <div className={`mt-3 mt-lg-0 ${styles.nameSection}`}>
                    <NameTitle name={name} size={28} noWrap={true} />

                    <div className={`text-left text-lg-center mb-3 mb-lg-5`}>
                        {challengeOrderBy === 'activitiesCount' 
                            ?
                                <Workouts count={activitiesCount} showIcon={true} category={category} />
                            :
                                <Distance distance={distance} showIcon={true} category={category} />
                        }
                    </div>
                </div>
            </div>

            <StatsRow time={time} calories={calories} elevation={elevation} />
        </div>
    );
}
