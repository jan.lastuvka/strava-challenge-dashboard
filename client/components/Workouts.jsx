import React from 'react';
import styles from "@/client/styles/Distance.module.css";
import CategoryIcon from "@/client/components/CategoryIcon";

export default function Workouts({count, showIcon, category, compact = false}) {
    let classList = styles.distance;

    if (compact)
    {
        classList = classList + ' ' + styles.distanceCompact;
    }

    if (showIcon)
    {
        classList = classList + ' ' + styles.distanceWithIcon;
    }

    const workoutsText = count === 1 ? 'workout' : 'workouts';

    return (
        <>
            {showIcon && <CategoryIcon category={category} />}
            <span className={classList}>{count} {compact ? ' x' : ' ' + workoutsText}</span>
        </>
    );
}
