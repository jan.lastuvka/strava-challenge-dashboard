import Head from 'next/head'
import styles from '@/client/styles/ActivitiesStats.module.css'
import {Component} from "react";
import {getChallengeActivitiesData} from "@/client/api/api";
import {CHALLENGE_END_DATE, CHALLENGE_START_DATE} from "@/app.config";

export default class Home extends Component {
    constructor(props) {
        super(props);

        this.challengeFrom = CHALLENGE_START_DATE
        this.challengeTo = CHALLENGE_END_DATE;

        this.state = {
            stats: [],
        };
    }

    componentDidMount() {
        getChallengeActivitiesData(this.challengeFrom, this.challengeTo).then(
            json => this.setState({
                stats: json.data
            })
        );
    }

    render() {
        return (
            <>
                <Head>
                    <title>Activities statistics</title>
                    <meta name="viewport" content="width=device-width, initial-scale=1"/>
                </Head>
                <main>
                    <div className='container'>
                        <div className='row'>
                            <div className='col-12'>
                                <table className={`table ${styles.statsTable}`}>
                                    <thead className="thead-dark">
                                        <tr>
                                            <th>Name</th>
                                            <th>Activities</th>
                                            <th>Avg. / w</th>
                                            {this.state.stats[0] && Object.keys(this.state.stats[0].weeks).map((week) => {
                                                return <th key={week}>week {week}</th>
                                            })}
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {this.state.stats.map((row) => {
                                            return (
                                                <tr key={row.athlete.id}>
                                                    <th>{row.athlete.name} {row.avg >= 4 && '✅'}</th>
                                                    <td>{row.total}</td>
                                                    <td>{row.avg}</td>
                                                    {Object.keys(row.weeks).map((week) => {
                                                        const count = row.weeks[week];
                                                        return <td key={week}>{count}</td>
                                                    })}
                                                </tr>
                                            )
                                        })}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </main>
            </>
        )
    }
}
