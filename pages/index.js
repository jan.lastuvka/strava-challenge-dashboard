import Head from 'next/head'
import {IBM_Plex_Mono, Racing_Sans_One} from 'next/font/google'
import styles from '@/client/styles/Page.module.css'

const racingSansOne = Racing_Sans_One({ subsets: ['latin'], weight: '400' });
const ibmPlexMono = IBM_Plex_Mono({ subsets: ['latin'], weight: ['400', '500', '600'], style: ['italic', 'normal'] });

import JoinButton from '@/client/components/JoinButton';
import WinnerCard from "@/client/components/WinnerCard";
import LoserCard from "@/client/components/LoserCard";
import CategoryButton from "@/client/components/CategoryButton";
import React, {Component} from "react";
import GitlabIcon from "@/client/components/GitlabIcon";
import AthleteDetailModal from "@/client/components/AthleteDetailModal";
import {getChallengeData, getTeamYearDistanceStatistic} from "@/client/api/api";
import Firework from "@/client/components/Firework";
import TeamsSwitcher from "@/client/components/TeamsSwitcher";
import TeamsStatistics from "@/client/components/TeamsStatistics";
import classNames from "classnames";
import JoinUsNotification from "@/client/components/JoinUsNotification";
import WebnodeForCharity from "@/client/components/WebnodeForCharity";
import {
    CHALLENGE_CHARITY_GOAL,
    CHALLENGE_END_DATE,
    CHALLENGE_START_DATE,
    CHALLENGE_YEAR,
    YEAR_CHARITY_GOAL
} from "@/app.config";

export default class Home extends Component {
    constructor(props) {
        super(props);

        this.categories = ['run', 'bike', 'workouts'];
        this.challengeFrom = CHALLENGE_START_DATE
        this.challengeTo = CHALLENGE_END_DATE;
        this.charityYearGoal = YEAR_CHARITY_GOAL;
        this.charityChallengeGoal = CHALLENGE_CHARITY_GOAL;
        this.challengeOrderBy = 'distance';
        this.teams = ['webnode', /*'iubenda'*/];

        this.state = {
            category: 'run',
            overlay: true,
            loading: true,
            shownModal: null, // 'athleteDetail', 'joinUs'
            athleteDetailData: null,
            athletes: {
                run: [],
                bike: [],
                workout: [],
            },
            team: this.teams[0],
            webnodeForCharityDistance: null,
        };
    }

    changeTeam = (team) => {
        localStorage.setItem('team', team);
        this.setState({team});
    }

    changeCategory = (category) => {
        localStorage.setItem('category', category);
        this.setState({category});
    }

    componentDidMount() {
        getChallengeData(this.challengeFrom, this.challengeTo, this.challengeOrderBy).then(
            json => this.setState({
                athletes: json.data,
                overlay: false,
                loading: false
            })
        );

        getTeamYearDistanceStatistic('webnode', CHALLENGE_YEAR).then(
            json => this.setState({
                webnodeForCharityDistance: json.data.totalDistance,
            })
        );

        const savedTeam = typeof window !== 'undefined' ? localStorage.getItem('team') : null;
        if (savedTeam !== null && this.teams.includes(savedTeam) && savedTeam !== this.state.team)
        {
            this.setState({team: savedTeam});
        }

        const savedCategory = typeof window !== 'undefined' ? localStorage.getItem('category') : null;
        if (savedCategory !== null && this.categories.includes(savedCategory) && savedCategory !== this.state.category)
        {
            this.setState({category: savedCategory});
        }
    }

    showOverlay = (show) => {
        document.body.style.overflow = show ? 'hidden' : 'auto';
        document.body.style.paddingRight = show ? '15px' : '0';
        this.setState({overlay: show});
    };

    showAthleteDetail = (id) => {
        const category = this.state.category;
        const athlete = this.state.athletes[category].find((athlete) => athlete.athlete.id === id);

        if (!athlete)
        {
            return;
        }

        this.showOverlay(true);
        this.setState({
            shownModal: 'athleteDetail',
            athleteDetailData: athlete,
        });
    };

    closeModal = () => {
        this.showOverlay(false);
        this.setState({
            shownModal: null,
        });
    };

    getChallengeTotalDistance = () => {
        let distance = 0;

        this.categories.forEach((category) => {
            const allAthletes = this.state.athletes[category] ?? [];
            const athletes = allAthletes.filter((athlete) => athlete.athlete.team === this.state.team);

            athletes.forEach((athlete) => {
                distance += athlete.distance;
            });
        });

        return Math.round(distance);
    };

    render() {
        const category = this.state.category;

        const allAthletes = this.state.athletes[category] ?? [];
        const athletes = allAthletes.filter((athlete) => athlete.athlete.team === this.state.team);

        const challengeOrderBy = this.state.category === 'workouts' ? 'activitiesCount' : this.challengeOrderBy;

        return (
            <>
                <Head>
                    <title>Webnode challenge</title>
                    <meta name="viewport" content="width=device-width, initial-scale=1"/>
                </Head>
                <main className={ibmPlexMono.className}>
                    <div className={`container ${styles.appContainer}`}>
                        <div className="row mt-3 mt-md-5 mb-md-4 mb-5">
                            <div className="col-md-8">
                                <TeamsSwitcher
                                    teams={this.teams}
                                    activeTeam={this.state.team}
                                    changeTeam={(team) => this.changeTeam(team)}
                                />
                                <h1 className={`${styles.title} ${racingSansOne.className}`}>Sport challenge</h1>
                            </div>
                            <div className="col-md-4 text-md-end mt-3 mt-md-0">
                                <JoinButton activeTeam={this.state.team} compact={this.teams.length <= 1}  />
                            </div>
                        </div>

                        {this.teams.length > 1 &&
                            <TeamsStatistics
                                teams={this.teams}
                                athletes={this.state.athletes}
                                challengeMetric={challengeOrderBy}
                            />
                        }

                        {
                            this.state.team === 'webnode' &&
                            this.state.webnodeForCharityDistance !== null &&
                            <WebnodeForCharity
                                // used if challenge has separate charity goal
                                challengeTotalDistance={
                                    this.charityChallengeGoal !== null ? this.getChallengeTotalDistance() : 0
                                }
                                challengeGoal={this.charityChallengeGoal}
                                // whole year charity goal
                                yearTotalDistance={this.state.webnodeForCharityDistance}
                                yearGoal={this.charityYearGoal}
                            />
                        }

                        <div className={
                            classNames(
                                'row',
                                'mb-4',
                                {'mt-3': this.teams.length > 1},
                            )}
                        >
                            <div className='col-12'>
                                <CategoryButton
                                    category={category}
                                    onChange={(value) => this.changeCategory(value)}
                                    categories={this.categories}
                                />
                            </div>
                        </div>

                        {athletes.length > 0 && <h2 className={styles.subTitle}>Winners</h2>}

                        <div className={`row mb-2 mb-lg-5 mt-3 ${styles.rowWinners}`}>
                            {athletes.slice(0, 3).map((athlete, key) => {
                                const classes = [styles.first, styles.second, styles.third];
                                return (
                                    <div
                                        className={`col-lg-4 ${classes[key]}`}
                                        key={key}
                                    >
                                        <WinnerCard
                                            id={athlete.athlete.id}
                                            name={athlete.athlete.name}
                                            department={athlete.athlete.department}
                                            distance={athlete.distance}
                                            time={athlete.time}
                                            calories={athlete.calories}
                                            elevation={athlete.elevation}
                                            activitiesCount={athlete.activitiesCount}
                                            avatar={athlete.athlete.avatar}
                                            place={key}
                                            category={category}
                                            onClick={(id) => this.showAthleteDetail(id)}
                                            challengeOrderBy={challengeOrderBy}
                                        />
                                    </div>
                                );
                            })}
                        </div>

                        {athletes.length > 3 && <h2 className={styles.subTitle}>Others</h2>}

                        <div className='row mt-2'>
                            <div className='col-12'>
                                {athletes.length > 3 && athletes.slice(3).map((athlete, key) => {
                                    return (
                                        <LoserCard
                                            key={key}
                                            id={athlete.athlete.id}
                                            name={athlete.athlete.name}
                                            department={athlete.athlete.department}
                                            distance={athlete.distance}
                                            time={athlete.time}
                                            calories={athlete.calories}
                                            elevation={athlete.elevation}
                                            activitiesCount={athlete.activitiesCount}
                                            avatar={athlete.athlete.avatar}
                                            place={key+3}
                                            category={category}
                                            onClick={(id) => this.showAthleteDetail(id)}
                                            challengeOrderBy={challengeOrderBy}
                                        />
                                    );
                                })}
                            </div>
                        </div>

                        <div className='row mt-auto pt-4 pb-4'>
                            <div className='col-12'>
                                <div className={'d-flex justify-content-center'}>
                                    <div className={`${styles.footer} text-end`} style={{paddingRight: '15px'}}>
                                        Created with 🧡 by Jan Laštůvka <br />
                                        Designed with 🖤 by Martin Rašek
                                    </div>
                                    <div style={{borderLeft: '1px solid #A2AFC6'}}>
                                        <a
                                            href='https://gitlab.com/jan.lastuvka/strava-challenge-dashboard'
                                            target={'_blank'}
                                        >
                                            <GitlabIcon width={44} height={44} />
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>

                    <div
                        className={styles.overlay}
                        style={{visibility: this.state.overlay ? 'visible' : 'hidden'}}
                        onClick={() => this.state.shownModal && this.closeModal()}
                    />

                    <div
                        className={styles.spinner}
                        style={{visibility: this.state.loading ? 'visible' : 'hidden'}}
                    >
                        <div className="d-flex justify-content-center">
                            <div className="spinner-border" role="status">
                                <span className="visually-hidden">Loading...</span>
                            </div>
                        </div>
                    </div>

                    {this.state.shownModal === 'athleteDetail' &&
                        <AthleteDetailModal
                            athlete={this.state.athleteDetailData}
                            category={this.state.category}
                            onClose={() => this.closeModal()}
                            challengeFrom={this.challengeFrom}
                            challengeTo={this.challengeTo}
                            challengeMetric={challengeOrderBy}
                        />
                    }

                    <Firework challengeEndDate={this.challengeTo} />

                    <JoinUsNotification />
                </main>
            </>
        )
    }
}
