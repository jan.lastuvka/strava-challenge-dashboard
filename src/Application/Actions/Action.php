<?php

declare(strict_types=1);

namespace App\Application\Actions;

use App\Domain\DomainException\DomainRecordNotFoundException;
use Fig\Http\Message\StatusCodeInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;

abstract class Action
{
    protected LoggerInterface $logger;

    protected Request $request;

    protected Response $response;

    /**
     * @var array<string, mixed>
     */
    protected array $args;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param Request $request
     * @param Response $response
     * @param array<string, mixed> $args
     * @return Response
     * @throws HttpNotFoundException
     * @throws HttpBadRequestException
     */
    public function __invoke(Request $request, Response $response, array $args): Response
    {
        $this->request = $request;
        $this->response = $response;
        $this->args = $args;

        try {
            return $this->action();
        } catch (DomainRecordNotFoundException $e) {
            throw new HttpNotFoundException($this->request, $e->getMessage());
        }
    }

    /**
     * @throws DomainRecordNotFoundException
     * @throws HttpBadRequestException
     */
    abstract protected function action(): Response;

    /**
     * @param string $name
     * @return mixed
     * @throws HttpBadRequestException
     */
    protected function resolveArg(string $name): mixed
    {
        if (!isset($this->args[$name])) {
            throw new HttpBadRequestException($this->request, "Could not resolve argument `{$name}`.");
        }

        return $this->args[$name];
    }

    protected function getRequestParam(string $name): mixed
    {
        $params = $this->request->getQueryParams();

        return $params[$name] ?? null;
    }

    /**
     * @param array<mixed>|object|null $data
     */
    protected function respondWithData(array|object|null $data = null, int $statusCode = 200): Response
    {
        $payload = new ActionPayload($statusCode, $data);

        return $this->respond($payload);
    }

    protected function respond(ActionPayload $payload): Response
    {
        $json = (string)json_encode($payload, JSON_PRETTY_PRINT);

        return $this->respondWithJson($json, $payload->getStatusCode());
    }

    protected function respondWithJson(string $json, int $statusCode): Response
    {
        $this->response->getBody()->write($json);

        return $this->response
            ->withHeader('Content-Type', 'application/json')
            ->withStatus($statusCode);
    }

    protected function redirect(string $location, int $status = StatusCodeInterface::STATUS_FOUND): Response
    {
        return $this->response
            ->withStatus($status)
            ->withHeader('Location', $location);
    }

    protected function parseDateFromRequest(string $paramName): \DateTimeImmutable
    {
        $stringDate = (string)$this->getRequestParam($paramName);
        $date = \DateTimeImmutable::createFromFormat('Y-m-d', $stringDate);
        if (!$date) {
            throw new HttpBadRequestException(
                $this->request,
                '"' . $paramName . '" has invalid value, use format Y-m-d'
            );
        }

        return $date;
    }
}
