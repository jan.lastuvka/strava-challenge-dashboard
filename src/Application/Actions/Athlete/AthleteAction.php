<?php

declare(strict_types=1);

namespace App\Application\Actions\Athlete;

use App\Application\Actions\Action;
use App\Domain\Athlete\AthleteRepositoryInterface;
use Psr\Log\LoggerInterface;

abstract class AthleteAction extends Action
{
    public function __construct(LoggerInterface $logger, protected AthleteRepositoryInterface $athleteRepository)
    {
        parent::__construct($logger);
    }
}
