<?php

declare(strict_types=1);

namespace App\Application\Actions\Athlete;

use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Categories\Category;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;

final class AthleteDayStatisticsAction extends AthleteAction
{
    public function __construct(
        LoggerInterface $logger,
        AthleteRepositoryInterface $athleteRepository,
        private readonly DayStatisticsRepositoryInterface $dayStatisticRepository,
    ) {
        parent::__construct($logger, $athleteRepository);
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $athleteId = (int)$this->resolveArg('athleteId');
        $athlete = $this->athleteRepository->getById($athleteId);

        $fromDateTime = $this->parseDateFromRequest('from');
        $toDateTime = $this->parseDateFromRequest('to');

        $categoryName = (string)$this->getRequestParam('category');
        $category = Category::tryFrom($categoryName);
        if (!$category) {
            throw new HttpBadRequestException($this->request, '"Category" has invalid value, use "run" or "bike"');
        }

        $statistics = $this->dayStatisticRepository->findByCategory(
            $athlete,
            $fromDateTime,
            $toDateTime,
            $category
        );

        return $this->respondWithData($statistics);
    }
}
