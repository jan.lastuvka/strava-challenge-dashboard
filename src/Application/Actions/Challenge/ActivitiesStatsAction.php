<?php

declare(strict_types=1);

namespace App\Application\Actions\Challenge;

use App\Application\Actions\Action;
use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

final class ActivitiesStatsAction extends Action
{
    public function __construct(
        LoggerInterface $logger,
        private readonly AthleteRepositoryInterface $athleteRepository,
        private readonly DayStatisticsRepositoryInterface $dayStatisticsRepository,
    ) {
        parent::__construct($logger);
    }


    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $fromDateTime = $this->parseDateFromRequest('from');
        $toDateTime = $this->parseDateFromRequest('to');

        $todayDateTime = new \DateTimeImmutable();
        if ($this->getRequestParam('today') !== null) {
            $todayDateTime = $this->parseDateFromRequest('today');
        }

        if ($todayDateTime > $toDateTime) {
            $todayDateTime = $toDateTime;
        }

        return $this->respondWithData($this->generateStats($fromDateTime, $toDateTime, $todayDateTime));
    }

    /**
     * @param \DateTimeImmutable $from
     * @param \DateTimeImmutable $to
     * @param \DateTimeImmutable $today
     * @return array<int, array{
     *     athlete: Athlete, total: int, avg: string, weeks: array<int, int>}
     * >
     */
    private function generateStats(
        \DateTimeImmutable $from,
        \DateTimeImmutable $to,
        \DateTimeImmutable $today,
    ): array {
        $weeks = $this->generateWeeksArray($from, $to);
        $numberOfPassedWeeks = count($this->generateWeeksArray(
            $from,
            $today,
        ));
        $athletes = $this->athleteRepository->findAll();

        $result = [];

        foreach ($athletes as $athlete) {
            $stats = $this->dayStatisticsRepository->find($athlete, $from, $to);

            if (empty($stats)) {
                continue;
            }

            $activitiesCount = 0;
            $athleteWeekActivities = $weeks;

            foreach ($stats as $item) {
                $week = (int)$item->date->format('W');
                $athleteWeekActivities[$week] = ($athleteWeekActivities[$week] ?? 0) + $item->activitiesCount;
                $activitiesCount += $item->activitiesCount;
            }

            $avg = round($activitiesCount / $numberOfPassedWeeks, 2);

            $athleteData = [
                'athlete' => $athlete,
                'weeks' => $athleteWeekActivities,
                'avg' => number_format($avg, 2),
                'total' => $activitiesCount,
            ];

            $result[] = $athleteData;
        }

        usort($result, function ($item1, $item2) {
            return $item2['avg'] <=> $item1['avg'];
        });

        return $result;
    }

    /**
     * @param \DateTimeImmutable $from
     * @param \DateTimeImmutable $to
     * @return int[]
     */
    private function generateWeeksArray(\DateTimeImmutable $from, \DateTimeImmutable $to): array
    {
        $weeks = [];
        while ($from <= $to) {
            $week = (int)$from->format('W');
            $weeks[$week] = 0;
            $from = $from->modify('+1 day');
        }

        return $weeks;
    }
}
