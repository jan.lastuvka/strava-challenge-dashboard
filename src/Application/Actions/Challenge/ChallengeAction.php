<?php

declare(strict_types=1);

namespace App\Application\Actions\Challenge;

use App\Application\Actions\Action;
use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Categories\Category;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;

final class ChallengeAction extends Action
{
    public function __construct(
        LoggerInterface $logger,
        private readonly AthleteRepositoryInterface $athleteRepository,
        private readonly DayStatisticsRepositoryInterface $dayStatisticsRepository,
    ) {
        parent::__construct($logger);
    }


    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $fromDateTime = $this->parseDateFromRequest('from');
        $toDateTime = $this->parseDateFromRequest('to');

        $orderBy = $this->getRequestParam('orderBy') ?: 'distance';
        if (!in_array($orderBy, ['distance', 'time', 'calories', 'elevation', 'activitiesCount'])) {
            throw new HttpBadRequestException(
                $this->request,
                '"orderBy" has invalid value, use: "distance", "time", "calories", "elevation" or "activitiesCount"'
            );
        }

        $athletes = $this->athleteRepository->findAll();

        return $this->respondWithData([
            'run' => $this->getCategoryChallengeData(
                $athletes,
                $fromDateTime,
                $toDateTime,
                Category::RUN,
                $orderBy
            ),
            'bike' => $this->getCategoryChallengeData(
                $athletes,
                $fromDateTime,
                $toDateTime,
                Category::BIKE,
                $orderBy
            ),
            'workouts' => $this->getCategoryChallengeData(
                $athletes,
                $fromDateTime,
                $toDateTime,
                Category::WORKOUTS,
                'activitiesCount'
            ),
        ]);
    }

    /**
     * @param Athlete[] $athletes
     * @param \DateTimeImmutable $from
     * @param \DateTimeImmutable $to
     * @param Category $category
     * @param string $orderBy
     * @return array<int, array{
     *     athlete: Athlete, distance: float, time: float, calories: float, elevation: float, activitiesCount: int}
     * >
     */
    private function getCategoryChallengeData(
        array $athletes,
        \DateTimeImmutable $from,
        \DateTimeImmutable $to,
        Category $category,
        string $orderBy
    ): array {
        $result = [];
        foreach ($athletes as $athlete) {
            $stats = $this->dayStatisticsRepository->findByCategory($athlete, $from, $to, $category);

            if (empty($stats)) {
                continue;
            }

            $distance = 0;
            $time = 0;
            $calories = 0;
            $elevation = 0;
            $activitiesCount = 0;

            foreach ($stats as $item) {
                $distance += $item->distance;
                $time += $item->time;
                $calories += $item->calories;
                $elevation += $item->elevation;
                $activitiesCount += $item->activitiesCount;
            }

            $athleteData = [
                'athlete' => $athlete,
                'distance' => round($distance, 2),
                'time' => $time,
                'calories' => round($calories),
                'elevation' => round($elevation),
                'activitiesCount' => $activitiesCount,
            ];

            $result[] = $athleteData;
        }

        usort($result, function ($item1, $item2) use ($orderBy) {
            return $item2[$orderBy] <=> $item1[$orderBy];
        });

        return $result;
    }
}
