<?php

declare(strict_types=1);

namespace App\Application\Actions\Join;

use App\Application\Actions\Action;
use App\Domain\Activity\ActivityProcessor;
use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteNotFoundException;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Athlete\Team;
use App\Domain\Categories\Category;
use App\Domain\Settings\SettingsRepositoryInterface;
use App\Domain\Statistics\DayStatistic;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use App\Infrastructure\API\Strava\StravaAPIClient;
use App\Infrastructure\OAuth2\StravaOAuth2Provider;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;

final class JoinChallengeAction extends Action
{
    private const REQUIRED_SCOPE = 'read,activity:read_all';

    public function __construct(
        LoggerInterface $logger,
        private readonly SettingsRepositoryInterface $settings,
        private readonly StravaOAuth2Provider $stravaOAuth2Provider,
        private readonly AthleteRepositoryInterface $athleteRepository,
        private readonly ActivityProcessor $activityProcessor,
        private readonly StravaAPIClient $client,
        private readonly DayStatisticsRepositoryInterface $dayStatisticsRepository,
        private readonly \DateTimeImmutable $challengeStartDate,
    ) {
        parent::__construct($logger);
    }


    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $appUrl = $this->settings->get('appUrl');

        if ($this->getRequestParam('code')) {
            $code = $this->getRequestParam('code');
            $scope = $this->getRequestParam('scope');

            if ($scope === self::REQUIRED_SCOPE) {
                try {
                    $token = $this->stravaOAuth2Provider->getAccessToken(
                        'authorization_code',
                        [
                            'code' => $code
                        ]
                    );
                } catch (\Exception) {
                    return $this->redirectToAuthorizePage();
                }

                try {
                    $athleteData = $token->getValues()['athlete'] ?? [];
                    if (isset($athleteData['id'])) {
                        $athlete = $this->athleteRepository->getByStravaId((int)$athleteData['id']);

                        $athlete = $this->updateAthleteFromTokenData($athlete, $token);
                        if ($athlete->joinDate === null) {
                            $this->createEmptyAthleteStatistics($athlete, $this->challengeStartDate);
                            $athlete->joinDate = new \DateTimeImmutable();
                        }

                        $this->athleteRepository->update($athlete);

                        $this->processLastAthleteActivities($athlete);

                        return $this->redirect($appUrl . '?joined');
                    }
                } catch (AthleteNotFoundException) {
                }

                $athlete = $this->registerAthlete($token);

                $this->createEmptyAthleteStatistics($athlete, $this->challengeStartDate);

                $this->processLastAthleteActivities($athlete);

                return $this->redirect($appUrl . '?joined');
            }
        }

        return $this->redirectToAuthorizePage();
    }

    private function redirectToAuthorizePage(): ResponseInterface
    {
        $appUrl = $this->settings->get('appUrl');
        $appId = $this->settings->get('strava')['clientId'] ?? 0;
        $team = $this->getTeamFromRequest()->value;

        $data = [
            'client_id' => $appId,
            'redirect_uri' => $appUrl . '/join',
            'response_type' => 'code',
            'approval_prompt' => 'auto',
            'scope' => self::REQUIRED_SCOPE,
            'state' => 'join-' . $team,
        ];

        $params = http_build_query($data);

        return $this->redirect(
            'https://www.strava.com/oauth/authorize?' . $params
        );
    }

    private function registerAthlete(AccessTokenInterface $token): Athlete
    {
        $athlete = new Athlete();

        $athlete = $this->updateAthleteFromTokenData($athlete, $token);

        $this->athleteRepository->create($athlete);

        return $athlete;
    }

    private function createEmptyAthleteStatistics(Athlete $athlete, \DateTimeImmutable $dateTime): void
    {
        foreach ([Category::BIKE, Category::RUN, Category::WORKOUTS] as $category) {
            $dayStatistic = new DayStatistic();
            $dayStatistic->athlete = $athlete;
            $dayStatistic->date = $dateTime;
            $dayStatistic->category = $category;

            $this->dayStatisticsRepository->create($dayStatistic);
        }
    }

    private function updateAthleteFromTokenData(Athlete $athlete, AccessTokenInterface $token): Athlete
    {
        $athleteData = $token->getValues()['athlete'] ?? [];

        $athlete->accessToken = $token->getToken();
        $athlete->refreshToken = (string)($token->getRefreshToken());
        $athlete->firstname = (string)($athleteData['firstname'] ?? '');
        $athlete->lastname = (string)($athleteData['lastname'] ?? '');
        $athlete->sex = (string)($athleteData['sex'] ?? '');
        $athlete->avatar = (string)($athleteData['profile'] ?? '');
        $athlete->username = (string)($athleteData['username'] ?? '');
        $athlete->stravaId = (int)($athleteData['id'] ?? 0);
        $athlete->team = $this->getTeamFromRequest();

        return $athlete;
    }

    private function processLastAthleteActivities(Athlete $athlete): void
    {
        $activities = $this->client->loadLastAthleteActivities($athlete);

        foreach ($activities as $activity) {
            $this->activityProcessor->processAthleteActivity($athlete, $activity);
        }
    }

    private function getTeamFromRequest(): Team
    {
        $team = $this->getRequestParam('team');
        if ($team !== null) {
            return Team::tryFrom($team) ?: Team::WEBNODE;
        }

        $state = $this->getRequestParam('state');
        if (is_string($state)) {
            $team = str_replace('join-', '', $state);

            return Team::tryFrom($team) ?: Team::WEBNODE;
        }

        return Team::WEBNODE;
    }
}
