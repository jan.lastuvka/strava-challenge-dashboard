<?php

declare(strict_types=1);

namespace App\Application\Actions\Statistics;

use App\Application\Actions\Action;
use App\Domain\Activity\ActivityProcessor;
use App\Domain\Athlete\Team;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpBadRequestException;

final class TeamStatisticsAction extends Action
{
    public function __construct(
        LoggerInterface $logger,
        private readonly DayStatisticsRepositoryInterface $statisticsRepository,
    ) {
        parent::__construct($logger);
    }

    /**
     * {@inheritdoc}
     */
    protected function action(): Response
    {
        $year = (int)$this->getRequestParam('year');
        $team = (string)$this->getRequestParam('team');

        $team = Team::tryFrom($team);
        if ($team === null) {
            throw new HttpBadRequestException($this->request, '"team" has invalid value, unknown team');
        }

        if ($year < 2023 || $year > 2050) {
            throw new HttpBadRequestException($this->request, 'It is not possible to count statistics for this year');
        }

        return $this->respondWithData([
            'totalDistance' => $this->statisticsRepository->getYearTeamDistanceStatistics(
                $team,
                $year,
                ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            )
        ]);
    }
}
