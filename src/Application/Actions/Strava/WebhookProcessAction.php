<?php

declare(strict_types=1);

namespace App\Application\Actions\Strava;

use App\Application\Actions\Action;
use App\Domain\Activity\ActivityNotFoundException;
use App\Domain\Activity\ActivityProcessor;
use App\Domain\Athlete\AthleteNotFoundException;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Settings\SettingsRepositoryInterface;
use App\Infrastructure\API\Strava\StravaAPIClient;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpUnauthorizedException;

final class WebhookProcessAction extends Action
{
    public const ACTION_CREATE = 'create';
    public const ACTION_UPDATE = 'update';
    public const ACTION_DELETE = 'delete';
    public const OBJECT_ACTIVITY = 'activity';

    public function __construct(
        LoggerInterface $logger,
        private readonly SettingsRepositoryInterface $settings,
        private readonly AthleteRepositoryInterface $athleteRepository,
        private readonly ActivityProcessor $activityProcessor,
        private readonly StravaAPIClient $client,
    ) {
        parent::__construct($logger);
    }

    protected function action(): Response
    {
        $this->logger->debug(
            sprintf(
                "Webhook request\nURL: %s\nBody: %s",
                $this->request->getUri(),
                $this->request->getBody(),
            )
        );

        $this->checkSubscriptionId();

        $requestData = (array)$this->request->getParsedBody();

        $action = $requestData['aspect_type'] ?? 'unknown';

        return match ($action) {
            self::ACTION_CREATE => $this->processCreate(),
            self::ACTION_DELETE => $this->processDelete(),
            self::ACTION_UPDATE => $this->processUpdate(),
            default => $this->unknownAction(),
        };
    }

    private function checkSubscriptionId(): void
    {
        $requestData = (array)$this->request->getParsedBody();
        $subscriptionId = isset($this->settings->get('strava')['subscriptionId'])
            ? (int)$this->settings->get('strava')['subscriptionId'] : null;

        if (!isset($requestData['subscription_id']) || $requestData['subscription_id'] !== $subscriptionId) {
            throw new HttpUnauthorizedException($this->request, 'Unknown subscription_id');
        }
    }

    private function unknownAction(): ResponseInterface
    {
        return $this->respondWithJson(
            (string)json_encode(['result' => 'unknown_action']),
            200
        );
    }

    private function processCreate(): ResponseInterface
    {
        $requestData = (array)$this->request->getParsedBody();
        $objectType = $requestData['object_type'] ?? 'unknown';

        $result = match ($objectType) {
            self::OBJECT_ACTIVITY => $this->processActivityCreate(),
            default => false,
        };

        return $this->respondWithJson(
            (string)json_encode(['result' => $result]),
            200
        );
    }

    private function processUpdate(): ResponseInterface
    {
        $requestData = (array)$this->request->getParsedBody();
        $objectType = $requestData['object_type'] ?? 'unknown';

        $result = match ($objectType) {
            self::OBJECT_ACTIVITY => $this->processActivityUpdate(),
            default => false,
        };

        return $this->respondWithJson(
            (string)json_encode(['result' => $result]),
            200
        );
    }

    private function processActivityCreate(): bool
    {
        $requestData = (array)$this->request->getParsedBody();
        $athleteId = $requestData['owner_id'] ?? -1;
        $activityId = $requestData['object_id'] ?? -1;

        try {
            $athlete = $this->athleteRepository->getByStravaId($athleteId);
        } catch (AthleteNotFoundException) {
            $this->logger->debug("Webhook request result: unable to load athlete");

            return false;
        }

        $activity = $this->client->loadAthleteActivityById($athlete, $activityId);
        if (!$activity) {
            $this->logger->debug("Webhook request result: unable to load activity");

            return false;
        }

        $this->activityProcessor->processNewStravaActivity($athlete, $activity);

        $this->logger->debug("Webhook request result: successfully processed new activity");

        return true;
    }

    private function processActivityUpdate(): bool
    {
        $requestData = (array)$this->request->getParsedBody();
        $athleteId = $requestData['owner_id'] ?? -1;
        $activityId = $requestData['object_id'] ?? -1;

        try {
            $athlete = $this->athleteRepository->getByStravaId($athleteId);
        } catch (AthleteNotFoundException) {
            $this->logger->debug("Webhook request result: unable to load athlete");

            return false;
        }

        $activity = $this->client->loadAthleteActivityById($athlete, $activityId);
        if (!$activity) {
            $this->logger->debug("Webhook request result: unable to load activity");

            return false;
        }

        try {
            $this->activityProcessor->processStravaActivityUpdate($athlete, $activity);
        } catch (ActivityNotFoundException) {
            $this->logger->debug("Webhook request result: unable to load original activity");

            return false;
        }

        $this->logger->debug("Webhook request result: successfully processed updated activity");

        return true;
    }

    private function processActivityDelete(): bool
    {
        $requestData = (array)$this->request->getParsedBody();
        $athleteId = $requestData['owner_id'] ?? -1;
        $activityId = $requestData['object_id'] ?? -1;

        try {
            $athlete = $this->athleteRepository->getByStravaId($athleteId);
        } catch (AthleteNotFoundException) {
            $this->logger->debug("Webhook request result: unable to load athlete");

            return false;
        }

        $result = $this->activityProcessor->processStravaActivityDelete($athlete, $activityId);

        if ($result) {
            $this->logger->debug("Webhook request result: activity successfully deleted");
        } else {
            $this->logger->debug("Webhook request result: unable to delete activity");
        }

        return $result;
    }

    private function processDelete(): ResponseInterface
    {
        $requestData = (array)$this->request->getParsedBody();
        $objectType = $requestData['object_type'] ?? 'unknown';

        $result = match ($objectType) {
            self::OBJECT_ACTIVITY => $this->processActivityDelete(),
            default => false,
        };

        return $this->respondWithJson(
            (string)json_encode(['result' => $result]),
            200
        );
    }
}
