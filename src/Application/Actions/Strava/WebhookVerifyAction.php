<?php

declare(strict_types=1);

namespace App\Application\Actions\Strava;

use App\Application\Actions\Action;
use App\Domain\Settings\SettingsRepositoryInterface;
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Log\LoggerInterface;
use Slim\Exception\HttpUnauthorizedException;

final class WebhookVerifyAction extends Action
{
    public function __construct(
        LoggerInterface $logger,
        private readonly SettingsRepositoryInterface $settings,
    ) {
        parent::__construct($logger);
    }

    protected function action(): Response
    {
        $challenge = (string)$this->getRequestParam('hub_challenge');
        $token = (string)$this->getRequestParam('hub_verify_token');

        if ($token === ($this->settings->get('strava')['webhookSecret'] ?? null)) {
            return $this->respondWithJson(
                (string)json_encode(['hub.challenge' => $challenge]),
                200
            );
        }

        throw new HttpUnauthorizedException($this->request, 'Unauthorized');
    }
}
