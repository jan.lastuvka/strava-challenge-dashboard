<?php

declare(strict_types=1);

namespace App\Domain\Activity;

use App\Domain\Athlete\Athlete;
use App\Domain\SportType\Types;

class Activity
{
    public function __construct(
        public ?int $id = null,
        public int $stravaId = 0,
        public Athlete $athlete = new Athlete(),
        public \DateTimeImmutable $dateTime = new \DateTimeImmutable(),
        public \DateTimeImmutable $startDateTime = new \DateTimeImmutable(),
        public string $name = '',
        public float $distance = 0,
        public float $time = 0,
        public float $elevation = 0,
        public float $calories = 0,
        public Types $sportType = Types::Run,
    ) {
    }
}
