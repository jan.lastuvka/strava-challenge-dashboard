<?php

declare(strict_types=1);

namespace App\Domain\Activity;

use App\Domain\DomainException\DomainRecordNotFoundException;

final class ActivityNotFoundException extends DomainRecordNotFoundException
{
    /**
     * @var string
     */
    public $message = 'The Activity you requested does not exist.';
}
