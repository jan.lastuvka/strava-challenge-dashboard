<?php

declare(strict_types=1);

namespace App\Domain\Activity;

use App\Domain\Athlete\Athlete;
use App\Domain\Categories\Category;
use App\Domain\SportType\Types;
use App\Domain\Statistics\DayStatistic;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use App\Domain\Strava\StravaActivity;

class ActivityProcessor
{
    public const MIN_ACTIVITY_LENGTH = 30;

    public function __construct(
        private readonly ActivityRepositoryInterface $activityRepository,
        private readonly DayStatisticsRepositoryInterface $dayStatisticRepository,
    ) {
    }

    /**
     * @param Athlete $athlete
     * @param StravaActivity $stravaActivity
     * @return Activity
     */
    public function processAthleteActivity(Athlete $athlete, StravaActivity $stravaActivity): Activity
    {
        try {
            $this->activityRepository->getByStravaId($stravaActivity->id);

            return $this->processStravaActivityUpdate($athlete, $stravaActivity);
        } catch (ActivityNotFoundException) {
            return $this->processNewStravaActivity($athlete, $stravaActivity);
        }
    }

    /**
     * @param Athlete $athlete
     * @param StravaActivity $stravaActivity
     * @return Activity
     */
    public function processNewStravaActivity(Athlete $athlete, StravaActivity $stravaActivity): Activity
    {
        $activity = new Activity();

        $activityId = $stravaActivity->id;

        try {
            return $this->activityRepository->getByStravaId($activityId);
        } catch (ActivityNotFoundException) {
        }

        $activity->stravaId = $activityId;
        $activity->athlete = $athlete;
        $activity->dateTime = new \DateTimeImmutable();
        $activity->startDateTime = $stravaActivity->startDate;
        $activity->name = $stravaActivity->name;
        $activity->distance = $stravaActivity->distance;
        $activity->time = $stravaActivity->movingTime;
        $activity->elevation = $stravaActivity->totalElevationGain;
        $activity->calories = $stravaActivity->calories;
        $activity->sportType = Types::tryFrom($stravaActivity->sportType) ?: Types::Unknown;

        $this->activityRepository->create($activity);

        if ($this->activityShouldBeCountedToDayStatistics($athlete, $activity)) {
            $this->addToDayStatistics($athlete, $activity);
        }

        return $activity;
    }

    /**
     * @param Athlete $athlete
     * @param StravaActivity $stravaActivity
     * @return Activity
     * @throws ActivityNotFoundException
     */
    public function processStravaActivityUpdate(Athlete $athlete, StravaActivity $stravaActivity): Activity
    {
        $activity = $this->activityRepository->getByStravaId($stravaActivity->id);

        if ($this->activityShouldBeCountedToDayStatistics($athlete, $activity)) {
            $this->removeFromDayStatistics($athlete, $activity);
        }

        $activity->startDateTime = $stravaActivity->startDate;
        $activity->name = $stravaActivity->name;
        $activity->distance = $stravaActivity->distance;
        $activity->time = $stravaActivity->movingTime;
        $activity->elevation = $stravaActivity->totalElevationGain;
        $activity->calories = $stravaActivity->calories > 0 ? $stravaActivity->calories : $activity->calories;
        $activity->sportType = Types::tryFrom($stravaActivity->sportType) ?: Types::Unknown;

        $this->activityRepository->update($activity);

        if ($this->activityShouldBeCountedToDayStatistics($athlete, $activity)) {
            $this->addToDayStatistics($athlete, $activity);
        }

        return $activity;
    }

    public function processStravaActivityDelete(Athlete $athlete, int $id): bool
    {
        try {
            $activity = $this->activityRepository->getByStravaId($id);
        } catch (ActivityNotFoundException) {
            return false;
        }

        $this->activityRepository->deleteById((int)$activity->id);

        if ($this->activityShouldBeCountedToDayStatistics($athlete, $activity)) {
            return $this->removeFromDayStatistics($athlete, $activity);
        }

        return true;
    }

    private function addToDayStatistics(Athlete $athlete, Activity $activity): DayStatistic
    {
        $category = $activity->sportType->toCategory();
        $date = $activity->startDateTime;

        $dayStatistic = $this->dayStatisticRepository->findOne($athlete, $date, $category);

        if (!$dayStatistic) {
            $dayStatistic = new DayStatistic();
            $dayStatistic->athlete = $athlete;
            $dayStatistic->category = $category;
            $dayStatistic->date = $date;
        }

        $dayStatistic->distance += ($activity->distance / 1000); //km
        $dayStatistic->time += ($activity->time / 60); // min
        $dayStatistic->elevation += $activity->elevation;
        $dayStatistic->calories += $activity->calories;
        $dayStatistic->activitiesCount += 1;

        if ($dayStatistic->id !== null) {
            $this->dayStatisticRepository->update($dayStatistic);
        } else {
            $this->dayStatisticRepository->create($dayStatistic);
        }

        return $dayStatistic;
    }

    private function removeFromDayStatistics(Athlete $athlete, Activity $activity): bool
    {
        $category = $activity->sportType->toCategory();
        $date = $activity->startDateTime;

        $dayStatistic = $this->dayStatisticRepository->findOne($athlete, $date, $category);

        if (!$dayStatistic) {
            return true;
        }

        $dayStatistic->distance -= ($activity->distance / 1000); //km
        $dayStatistic->time -= ($activity->time / 60); // min
        $dayStatistic->elevation -= $activity->elevation;
        $dayStatistic->calories -= $activity->calories;
        $dayStatistic->activitiesCount -= 1;

        $this->dayStatisticRepository->update($dayStatistic);

        return true;
    }

    private function activityShouldBeCountedToDayStatistics(Athlete $athlete, Activity $activity): bool
    {
        if ($athlete->joinDate === null) {
            return false;
        }

        $category = $activity->sportType->toCategory();

        $joinDate = $athlete->joinDate->setTime(0, 0, 0);
        $startDate = $activity->startDateTime->setTime(0, 0, 0);

        $time = $activity->time;

        return $startDate >= $joinDate &&
            $category !== Category::NOT_CLASSIFIED &&
            $time >= (self::MIN_ACTIVITY_LENGTH * 60);
    }
}
