<?php

declare(strict_types=1);

namespace App\Domain\Activity;

interface ActivityRepositoryInterface
{
    public function create(Activity $activity): Activity;

    /**
     * @param int $id
     * @return Activity
     * @throws ActivityNotFoundException
     */
    public function getByStravaId(int $id): Activity;

    public function deleteById(int $id): bool;

    public function update(Activity $activity): Activity;
}
