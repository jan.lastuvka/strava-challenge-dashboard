<?php

declare(strict_types=1);

namespace App\Domain\Athlete;

use JsonSerializable;

class Athlete implements JsonSerializable
{
    public function __construct(
        public ?int $id = null,
        public string $accessToken = '',
        public string $refreshToken = '',
        public int $stravaId = 0,
        public string $username = '',
        public string $firstname = '',
        public string $lastname = '',
        public string $sex = '',
        public string $avatar = '',
        public string $department = '',
        public ?\DateTimeImmutable $joinDate = new \DateTimeImmutable(),
        public Team $team = Team::WEBNODE,
    ) {
    }

    /**
     * @return array<string, mixed>
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'name' => trim($this->firstname . ' ' . $this->lastname),
            'avatar' => $this->avatar,
            'department' => $this->department,
            'team' => $this->team->value,
        ];
    }
}
