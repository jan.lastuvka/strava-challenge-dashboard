<?php

declare(strict_types=1);

namespace App\Domain\Athlete;

use App\Domain\DomainException\DomainRecordNotFoundException;

final class AthleteNotFoundException extends DomainRecordNotFoundException
{
    /**
     * @var string
     */
    public $message = 'The Athlete you requested does not exist.';
}
