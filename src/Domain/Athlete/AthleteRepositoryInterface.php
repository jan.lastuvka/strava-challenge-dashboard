<?php

declare(strict_types=1);

namespace App\Domain\Athlete;

interface AthleteRepositoryInterface
{
    public function create(Athlete $athlete): Athlete;

    public function update(Athlete $athlete): Athlete;

    /**
     * @return Athlete[]
     */
    public function findAll(): array;

    /**
     * @param int $id
     * @return Athlete
     * @throws AthleteNotFoundException
     */
    public function getById(int $id): Athlete;

    /**
     * @param int $id
     * @return Athlete
     * @throws AthleteNotFoundException
     */
    public function getByStravaId(int $id): Athlete;
}
