<?php

namespace App\Domain\Athlete;

enum Team: string
{
    case WEBNODE = 'webnode';
    case IUBENDA = 'iubenda';
}
