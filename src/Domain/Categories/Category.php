<?php

namespace App\Domain\Categories;

enum Category: string
{
    case RUN = 'run';
    case BIKE = 'bike';
    case WORKOUTS = 'workouts';
    case NOT_CLASSIFIED = 'not-classified';

    public static function fromId(int $id): Category
    {
        return match ($id) {
            1 => self::RUN,
            2 => self::BIKE,
            3 => self::WORKOUTS,
            default => self::NOT_CLASSIFIED,
        };
    }

    public function toId(): int
    {
        return match ($this) {
            self::NOT_CLASSIFIED => 0,
            self::RUN => 1,
            self::BIKE => 2,
            self::WORKOUTS => 3,
        };
    }
}
