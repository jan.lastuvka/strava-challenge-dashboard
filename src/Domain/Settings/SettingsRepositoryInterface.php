<?php

declare(strict_types=1);

namespace App\Domain\Settings;

interface SettingsRepositoryInterface
{
    public function get(string $key = '', mixed $default = null): mixed;
}
