<?php

namespace App\Domain\SportType;

use App\Domain\Categories\Category;

enum Types: string
{
    case AlpineSki = 'AlpineSki';
    case BackcountrySki = 'BackcountrySki';
    case Badminton = 'Badminton';
    case Canoeing = 'Canoeing';
    case Crossfit = 'Crossfit';
    case EBikeRide = 'EBikeRide';
    case Elliptical = 'Elliptical';
    case EMountainBikeRide = 'EMountainBikeRide';
    case Golf = 'Golf';
    case GravelRide = 'GravelRide';
    case Handcycle = 'Handcycle';
    case HighIntensityIntervalTraining = 'HighIntensityIntervalTraining';
    case Hike = 'Hike';
    case IceSkate = 'IceSkate';
    case InlineSkate = 'InlineSkate';
    case Kayaking = 'Kayaking';
    case Kitesurf = 'Kitesurf';
    case MountainBikeRide = 'MountainBikeRide';
    case NordicSki = 'NordicSki';
    case Pickleball = 'Pickleball';
    case Pilates = 'Pilates';
    case Racquetball = 'Racquetball';
    case Ride = 'Ride';
    case RockClimbing = 'RockClimbing';
    case RollerSki = 'RollerSki';
    case Rowing = 'Rowing';
    case Run = 'Run';
    case Sail = 'Sail';
    case Skateboard = 'Skateboard';
    case Snowboard = 'Snowboard';
    case Snowshoe = 'Snowshoe';
    case Soccer = 'Soccer';
    case Squash = 'Squash';
    case StairStepper = 'StairStepper';
    case StandUpPaddling = 'StandUpPaddling';
    case Surfing = 'Surfing';
    case Swim = 'Swim';
    case TableTennis = 'TableTennis';
    case Tennis = 'Tennis';
    case TrailRun = 'TrailRun';
    case Velomobile = 'Velomobile';
    case VirtualRide = 'VirtualRide';
    case VirtualRow = 'VirtualRow';
    case VirtualRun = 'VirtualRun';
    case Walk = 'Walk';
    case WeightTraining = 'WeightTraining';
    case Wheelchair = 'Wheelchair';
    case Windsurf = 'Windsurf';
    case Workout = 'Workout';
    case Yoga = 'Yoga';
    case Unknown = 'Unknown';

    public function toCategory(): Category
    {
        return match ($this) {
            self::AlpineSki => Category::BIKE,
            self::BackcountrySki => Category::RUN,
            self::Badminton => Category::WORKOUTS,
            self::Canoeing => Category::BIKE,
            self::Crossfit => Category::WORKOUTS,
            self::EBikeRide => Category::BIKE,
            self::Elliptical => Category::RUN,
            self::EMountainBikeRide => Category::BIKE,
            self::Golf => Category::RUN,
            self::GravelRide => Category::BIKE,
            self::Handcycle => Category::BIKE,
            self::HighIntensityIntervalTraining => Category::WORKOUTS,
            self::Hike => Category::RUN,
            self::IceSkate => Category::BIKE,
            self::InlineSkate => Category::BIKE,
            self::Kayaking => Category::BIKE,
            self::Kitesurf => Category::BIKE,
            self::MountainBikeRide => Category::BIKE,
            self::NordicSki => Category::BIKE,
            self::Pickleball => Category::WORKOUTS,
            self::Pilates => Category::WORKOUTS,
            self::Racquetball => Category::WORKOUTS,
            self::Ride => Category::BIKE,
            self::RockClimbing => Category::WORKOUTS,
            self::RollerSki => Category::BIKE,
            self::Rowing => Category::RUN,
            self::Run => Category::RUN,
            self::Sail => Category::WORKOUTS,
            self::Skateboard => Category::BIKE,
            self::Snowboard => Category::BIKE,
            self::Snowshoe => Category::BIKE,
            self::Soccer => Category::RUN,
            self::Squash => Category::WORKOUTS,
            self::StairStepper => Category::WORKOUTS,
            self::StandUpPaddling => Category::WORKOUTS,
            self::Surfing => Category::WORKOUTS,
            self::Swim => Category::RUN,
            self::TableTennis => Category::WORKOUTS,
            self::Tennis => Category::WORKOUTS,
            self::TrailRun => Category::RUN,
            self::Velomobile => Category::WORKOUTS,
            self::VirtualRide => Category::BIKE,
            self::VirtualRow => Category::RUN,
            self::VirtualRun => Category::RUN,
            self::Walk => Category::RUN,
            self::WeightTraining => Category::WORKOUTS,
            self::Wheelchair => Category::BIKE,
            self::Windsurf => Category::WORKOUTS,
            self::Workout => Category::WORKOUTS,
            self::Yoga => Category::WORKOUTS,
            self::Unknown => Category::NOT_CLASSIFIED,
        };
    }
}
