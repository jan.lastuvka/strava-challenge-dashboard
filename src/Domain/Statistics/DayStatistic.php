<?php

declare(strict_types=1);

namespace App\Domain\Statistics;

use App\Domain\Athlete\Athlete;
use App\Domain\Categories\Category;
use JsonSerializable;

class DayStatistic implements JsonSerializable
{
    public function __construct(
        public ?int $id = null,
        public Athlete $athlete = new Athlete(),
        public Category $category = Category::NOT_CLASSIFIED,
        public \DateTimeImmutable $date = new \DateTimeImmutable(),
        public float $distance = 0,
        public float $time = 0,
        public float $calories = 0,
        public float $elevation = 0,
        public int $activitiesCount = 0,
    ) {
    }

    /**
     * @return array<string, null|int|float|string>
     */
    public function jsonSerialize(): array
    {
        return [
            'id' => $this->id,
            'athleteId' => $this->athlete->id,
            'category' => $this->category->value,
            'date' => $this->date->format('Y-m-d'),
            'distance' => $this->distance,
            'time' => $this->time,
            'calories' => $this->calories,
            'elevation' => $this->elevation,
            'activitiesCount' => $this->activitiesCount,
        ];
    }
}
