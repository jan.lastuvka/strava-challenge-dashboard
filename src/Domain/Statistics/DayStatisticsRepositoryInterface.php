<?php

declare(strict_types=1);

namespace App\Domain\Statistics;

use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\Team;
use App\Domain\Categories\Category;

interface DayStatisticsRepositoryInterface
{
    public function create(DayStatistic $statistic): DayStatistic;

    public function update(DayStatistic $statistic): DayStatistic;

    /**
     * @param Athlete $athlete
     * @param \DateTimeImmutable $from
     * @param \DateTimeImmutable $to
     * @param Category $category
     * @return DayStatistic[]
     */
    public function findByCategory(
        Athlete $athlete,
        \DateTimeImmutable $from,
        \DateTimeImmutable $to,
        Category $category
    ): array;

    /**
     * @param Athlete $athlete
     * @param \DateTimeImmutable $from
     * @param \DateTimeImmutable $to
     * @return DayStatistic[]
     */
    public function find(
        Athlete $athlete,
        \DateTimeImmutable $from,
        \DateTimeImmutable $to
    ): array;

    /**
     * @param Athlete $athlete
     * @param \DateTimeImmutable $date
     * @param Category $category
     * @return DayStatistic|null
     */
    public function findOne(
        Athlete $athlete,
        \DateTimeImmutable $date,
        Category $category
    ): ?DayStatistic;

    public function getYearTeamDistanceStatistics(
        Team $team,
        int $year,
        int $minActivityTime,
    ): int;
}
