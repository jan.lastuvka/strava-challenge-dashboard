<?php

declare(strict_types=1);

namespace App\Domain\Strava;

use DateTimeImmutable;

class StravaActivity
{
    public function __construct(
        public readonly int $id = -1,
        public readonly DateTimeImmutable $startDate = new DateTimeImmutable(),
        public readonly string $name = '',
        public readonly float $distance = 0,
        public readonly float $movingTime = 0,
        public readonly float $totalElevationGain = 0,
        public readonly float $calories = 0,
        public readonly string $sportType = 'Unknown',
    ) {
    }
}
