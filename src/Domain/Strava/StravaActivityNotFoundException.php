<?php

declare(strict_types=1);

namespace App\Domain\Strava;

use App\Domain\DomainException\DomainException;

final class StravaActivityNotFoundException extends DomainException
{
}
