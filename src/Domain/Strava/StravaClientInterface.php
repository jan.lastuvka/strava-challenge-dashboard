<?php

declare(strict_types=1);

namespace App\Domain\Strava;

use App\Domain\Athlete\Athlete;

interface StravaClientInterface
{
    /**
     * @return StravaActivity[]
     */
    public function loadLastAthleteActivities(Athlete $athlete, int $limit = 100): array;

    /**
     * @return null|StravaActivity
     */
    public function loadAthleteActivityById(Athlete $athlete, int $id): ?StravaActivity;
}
