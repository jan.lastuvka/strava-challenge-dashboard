<?php

declare(strict_types=1);

namespace App\Infrastructure\API\Strava;

use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Strava\StravaActivity;
use App\Domain\Strava\StravaClientInterface;
use App\Infrastructure\OAuth2\StravaOAuth2Provider;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Request;
use League\OAuth2\Client\Provider\Exception\IdentityProviderException;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;

class StravaAPIClient implements StravaClientInterface
{
    public function __construct(
        private readonly Client $client,
        private readonly AthleteRepositoryInterface $athleteRepository,
        private readonly StravaOAuth2Provider $stravaOAuth2Provider,
        private readonly LoggerInterface $logger,
    ) {
    }

    /**
     * @inheritDoc
     */
    public function loadLastAthleteActivities(Athlete $athlete, int $limit = 100): array
    {
        $request = new Request(
            'GET',
            'https://www.strava.com/api/v3/athlete/activities?per_page=' . $limit
        );

        $response = $this->doRequest($request, $athlete);

        if ($response->getStatusCode() === 200) {
            $responseContent = (string)$response->getBody();
            $data = (array)json_decode($responseContent, true);

            $result = [];
            foreach ($data as $activityData) {
                $result[] = $this->createStravaActivityFromData($activityData);
            }

            return $result;
        }

        return [];
    }

    /**
     * @inheritDoc
     */
    public function loadAthleteActivityById(Athlete $athlete, int $id): ?StravaActivity
    {
        $request = new Request(
            'GET',
            'https://www.strava.com/api/v3/activities/' . $id,
        );

        $response = $this->doRequest($request, $athlete);

        if ($response->getStatusCode() === 200) {
            $responseContent = (string)$response->getBody();
            $data = (array)json_decode($responseContent, true);

            return $this->createStravaActivityFromData($data);
        } else {
            $this->logger->error(
                'Unable to load activity data. Athlete id: ' . $athlete->id .
                ' | Activity id: ' . $id . ' | Status code: ' . $response->getStatusCode()
            );
        }

        return null;
    }

    private function getAthleteToken(Athlete $athlete): AccessTokenInterface
    {
        return new AccessToken([
            'access_token' => $athlete->accessToken,
            'refresh_token' => $athlete->refreshToken
        ]);
    }

    private function doRequest(RequestInterface $request, Athlete $athlete): ResponseInterface
    {
        $token = $this->getAthleteToken($athlete);

        $authorizedRequest = $request->withHeader('Authorization', 'Bearer ' . $token->getToken());
        $response = $this->client->sendRequest($authorizedRequest);

        if ($response->getStatusCode() === 401 && $token->getRefreshToken() !== null) {
            try {
                $token = $this->stravaOAuth2Provider->getAccessToken(
                    'refresh_token',
                    [
                        'refresh_token' => $token->getRefreshToken()
                    ]
                );
                $tokenRefreshSuccessFul = true;
            } catch (IdentityProviderException | \InvalidArgumentException) {
                $tokenRefreshSuccessFul = false;
            }

            if ($tokenRefreshSuccessFul) {
                $athlete->accessToken = $token->getToken();
                $athlete->refreshToken = (string)($token->getRefreshToken());

                $this->athleteRepository->update($athlete);

                $authorizedRequest = $request->withHeader('Authorization', 'Bearer ' . $token->getToken());
                $response = $this->client->sendRequest($authorizedRequest);
            }

            if ($response->getStatusCode() === 401) {
                $this->logger->critical(
                    'Unable to load athlete data. Athlete id: ' . $athlete->id .
                    ' | Token refresh: ' . ($tokenRefreshSuccessFul ? 'successful' : 'failed')
                );
            }

            return $response;
        }

        return $response;
    }

    /**
     * @param array<mixed> $data
     * @return StravaActivity
     */
    private function createStravaActivityFromData(array $data): StravaActivity
    {
        try {
            $datetime = new \DateTimeImmutable((string)($data['start_date'] ?? ''));
        } catch (\Exception) {
            $datetime = new \DateTimeImmutable();
        }

        return new StravaActivity(
            (int)($data['id'] ?? -1),
            $datetime,
            (string)($data['name'] ?? ''),
            (float)($data['distance'] ?? 0),
            (float)($data['moving_time'] ?? 0),
            (float)($data['total_elevation_gain'] ?? 0),
            (float)($data['calories'] ?? 0),
            (string)($data['sport_type'] ?? '')
        );
    }
}
