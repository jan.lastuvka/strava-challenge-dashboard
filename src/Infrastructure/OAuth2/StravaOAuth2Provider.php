<?php

declare(strict_types=1);

namespace App\Infrastructure\OAuth2;

use League\OAuth2\Client\Provider\AbstractProvider;
use League\OAuth2\Client\Provider\ResourceOwnerInterface;
use League\OAuth2\Client\Token\AccessToken;
use League\OAuth2\Client\Tool\BearerAuthorizationTrait;
use Psr\Http\Message\ResponseInterface;
use RuntimeException;

class StravaOAuth2Provider extends AbstractProvider
{
    use BearerAuthorizationTrait;

    /**
     * @var string[]
     */
    public array $scopes = ['write'];

    /**
     * @return string
     */
    public function getBaseAuthorizationUrl(): string
    {
        return 'https://www.strava.com/oauth/authorize';
    }

    /**
     * @param array<mixed> $params
     * @return string
     */
    public function getBaseAccessTokenUrl(array $params): string
    {
        return 'https://www.strava.com/oauth/token';
    }

    /**
     * @param AccessToken $token
     * @return string
     */
    public function getResourceOwnerDetailsUrl(AccessToken $token): string
    {
        return '';
    }

    /**
     * @return string[]
     */
    protected function getDefaultScopes(): array
    {
        return $this->scopes;
    }

    /**
     * @param ResponseInterface $response
     * @param array<mixed>|string $data
     * @return void
     */
    protected function checkResponse(ResponseInterface $response, $data): void
    {
    }

    /**
     * @param array<mixed> $response
     * @param AccessToken $token
     * @return ResourceOwnerInterface
     */
    protected function createResourceOwner(array $response, AccessToken $token): ResourceOwnerInterface
    {
        throw new RuntimeException('Not implemented');
    }
}
