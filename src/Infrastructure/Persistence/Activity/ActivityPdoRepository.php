<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Activity;

use App\Domain\Activity\Activity;
use App\Domain\Activity\ActivityNotFoundException;
use App\Domain\Activity\ActivityRepositoryInterface;
use App\Domain\Athlete\Athlete;
use App\Domain\SportType\Types;

final class ActivityPdoRepository implements ActivityRepositoryInterface
{
    public function __construct(
        private readonly \PDO $db
    ) {
    }

    public function create(Activity $activity): Activity
    {
        if ($activity->id !== null) {
            throw new \RuntimeException('Cannot create already created activity');
        }

        $sql = 'INSERT INTO `activities` 
                (`stravaId`, `athleteId`, `datetime`, `startDatetime`, `name`, `distance`,
                 `time`, `elevation`, `calories`, `sportType`)
                 VALUES (:stravaId, :athleteId, :datetime, :startDatetime, :name, 
                 :distance, :time, :elevation, :calories, :sportType)';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('stravaId', $activity->stravaId);
        $stmt->bindParam('athleteId', $activity->athlete->id);
        $stmt->bindValue('datetime', $activity->dateTime->format('Y-m-d H:i:s'));
        $stmt->bindValue('startDatetime', $activity->startDateTime->format('Y-m-d H:i:s'));
        $stmt->bindParam('name', $activity->name);
        $stmt->bindParam('distance', $activity->distance);
        $stmt->bindParam('time', $activity->time);
        $stmt->bindParam('elevation', $activity->elevation);
        $stmt->bindParam('calories', $activity->calories);
        $stmt->bindValue('sportType', $activity->sportType->value);

        $stmt->execute();

        $activity->id = (int)$this->db->lastInsertId();

        return $activity;
    }

    public function update(Activity $activity): Activity
    {
        if ($activity->id === null) {
            throw new \RuntimeException('Cannot update activity that does not exist');
        }

        $sql = 'UPDATE `activities` SET
                `startDatetime` = :startDatetime, `name` = :name, `distance` = :distance, 
                `time` = :time, `elevation` = :elevation, `calories` = :calories, `sportType` = :sportType
                WHERE `id` = :id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindValue('startDatetime', $activity->startDateTime->format('Y-m-d H:i:s'));
        $stmt->bindParam('name', $activity->name);
        $stmt->bindParam('distance', $activity->distance);
        $stmt->bindParam('time', $activity->time);
        $stmt->bindParam('elevation', $activity->elevation);
        $stmt->bindParam('calories', $activity->calories);
        $stmt->bindValue('sportType', $activity->sportType->value);
        $stmt->bindParam('id', $activity->id);

        $stmt->execute();

        return $activity;
    }

    public function getByStravaId(int $id): Activity
    {
        $sql = 'SELECT * FROM `activities` WHERE `stravaId` = :id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $data = $stmt->fetch();

        if (!$data) {
            throw new ActivityNotFoundException();
        }

        return $this->createFromDbData($data);
    }

    /**
     * @param array<string, mixed> $data
     * @return Activity
     */
    private function createFromDbData(array $data): Activity
    {
        $type = Types::tryFrom((string)($data['sportType'] ?? '')) ?: Types::Unknown;

        return new Activity(
            (int)($data['id'] ?? 0),
            (int)($data['stravaId']),
            new Athlete(id: (int)($data['athleteId'])),
            new \DateTimeImmutable($data['datetime']),
            new \DateTimeImmutable($data['startDatetime']),
            (string)($data['name'] ?? ''),
            (float)($data['distance'] ?? 0),
            (float)($data['time'] ?? 0),
            (float)($data['elevation'] ?? 0),
            (float)($data['calories'] ?? 0),
            $type
        );
    }

    public function deleteById(int $id): bool
    {
        $sql = 'DELETE FROM `activities` WHERE `id` = :id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('id', $id);
        return $stmt->execute();
    }
}
