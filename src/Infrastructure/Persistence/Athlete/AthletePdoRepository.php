<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Athlete;

use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteNotFoundException;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Athlete\Team;

final class AthletePdoRepository implements AthleteRepositoryInterface
{
    public function __construct(
        private readonly \PDO $db
    ) {
    }

    public function create(Athlete $athlete): Athlete
    {
        if ($athlete->id !== null) {
            throw new \RuntimeException('Cannot create already existing athlete');
        }

        $sql = 'INSERT INTO `athletes` 
                (`accessToken`, `refreshToken`, `stravaId`, `username`, `firstname`, 
                 `lastname`, `sex`, `avatar`, `department`, `joinDate`, `team`)
                 VALUES (:accessToken, :refreshToken, :stravaId, :username, :firstname, 
                 :lastname, :sex, :avatar, :department, :joinDate, :team)';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('accessToken', $athlete->accessToken);
        $stmt->bindParam('refreshToken', $athlete->refreshToken);
        $stmt->bindParam('stravaId', $athlete->stravaId);
        $stmt->bindParam('username', $athlete->username);
        $stmt->bindParam('firstname', $athlete->firstname);
        $stmt->bindParam('lastname', $athlete->lastname);
        $stmt->bindParam('sex', $athlete->sex);
        $stmt->bindParam('avatar', $athlete->avatar);
        $stmt->bindParam('department', $athlete->department);
        $stmt->bindValue('joinDate', $athlete->joinDate?->format('Y-m-d'));
        $stmt->bindValue('team', $athlete->team->value);

        $stmt->execute();

        $athlete->id = (int)$this->db->lastInsertId();

        return $athlete;
    }

    public function update(Athlete $athlete): Athlete
    {
        if ($athlete->id === null) {
            throw new \RuntimeException('Cannot update non existing athlete');
        }

        $sql = 'UPDATE `athletes` SET 
                `accessToken` = :accessToken, `refreshToken` = :refreshToken, `stravaId` = :stravaId,
                `username` = :username, `firstname` = :firstname, `lastname` = :lastname,
                `sex` = :sex, `avatar` = :avatar, `department` = :department, `team` = :team, `joinDate` = :joinDate
                WHERE `id` = :id';

        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('accessToken', $athlete->accessToken);
        $stmt->bindParam('refreshToken', $athlete->refreshToken);
        $stmt->bindParam('stravaId', $athlete->stravaId);
        $stmt->bindParam('username', $athlete->username);
        $stmt->bindParam('firstname', $athlete->firstname);
        $stmt->bindParam('lastname', $athlete->lastname);
        $stmt->bindParam('sex', $athlete->sex);
        $stmt->bindParam('avatar', $athlete->avatar);
        $stmt->bindParam('department', $athlete->department);
        $stmt->bindParam('id', $athlete->id);
        $stmt->bindValue('joinDate', $athlete->joinDate?->format('Y-m-d'));
        $stmt->bindValue('team', $athlete->team->value);

        $stmt->execute();

        return $athlete;
    }

    public function getById(int $id): Athlete
    {
        $sql = 'SELECT * FROM `athletes` WHERE `id` = :id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $data = $stmt->fetch();

        if (!$data) {
            throw new AthleteNotFoundException();
        }

        return $this->createFromDbData($data);
    }


    public function getByStravaId(int $id): Athlete
    {
        $sql = 'SELECT * FROM `athletes` WHERE `stravaId` = :id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('id', $id);
        $stmt->execute();
        $data = $stmt->fetch();

        if (!$data) {
            throw new AthleteNotFoundException();
        }

        return $this->createFromDbData($data);
    }

    /**
     * @param array<string, mixed> $data
     * @return Athlete
     */
    private function createFromDbData(array $data): Athlete
    {
        return new Athlete(
            (int)($data['id'] ?? 0),
            (string)($data['accessToken'] ?? ''),
            (string)($data['refreshToken'] ?? ''),
            (int)($data['stravaId'] ?? 0),
            (string)($data['username'] ?? ''),
            (string)($data['firstname'] ?? ''),
            (string)($data['lastname'] ?? ''),
            (string)($data['sex'] ?? ''),
            (string)($data['avatar'] ?? ''),
            (string)($data['department'] ?? ''),
            $data['joinDate'] === null ? null : new \DateTimeImmutable($data['joinDate']),
            Team::from($data['team'] ?? '')
        );
    }

    public function findAll(): array
    {
        $sql = 'SELECT * FROM `athletes`';
        $stmt = $this->db->prepare($sql);
        $stmt->execute();
        $data = $stmt->fetchAll();

        if (!$data) {
            return [];
        }

        $result = [];

        foreach ($data as $row) {
            $result[] = $this->createFromDbData($row);
        }

        return $result;
    }
}
