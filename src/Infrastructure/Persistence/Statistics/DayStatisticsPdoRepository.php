<?php

declare(strict_types=1);

namespace App\Infrastructure\Persistence\Statistics;

use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\Team;
use App\Domain\Categories\Category;
use App\Domain\Statistics\DayStatistic;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;

final class DayStatisticsPdoRepository implements DayStatisticsRepositoryInterface
{
    public function __construct(
        private readonly \PDO $db
    ) {
    }

    public function create(DayStatistic $statistic): DayStatistic
    {
        if ($statistic->id !== null) {
            throw new \RuntimeException('Cannot create already created day statistics');
        }

        $sql = 'INSERT INTO `dayStatistics` 
                (`athleteId`, `category`, `date`, `distance`, `time`, `calories`, `elevation`, activitiesCount)
                 VALUES (:athleteId, :category, :date, :distance, :time, 
                 :calories, :elevation, :activitiesCount)';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('athleteId', $statistic->athlete->id);
        $stmt->bindValue('category', $statistic->category->toId());
        $stmt->bindValue('date', $statistic->date->format('Y-m-d'));
        $stmt->bindParam('distance', $statistic->distance);
        $stmt->bindParam('time', $statistic->time);
        $stmt->bindParam('calories', $statistic->calories);
        $stmt->bindParam('elevation', $statistic->elevation);
        $stmt->bindParam('activitiesCount', $statistic->activitiesCount);

        $stmt->execute();

        $statistic->id = (int)$this->db->lastInsertId();

        return $statistic;
    }

    public function update(DayStatistic $statistic): DayStatistic
    {
        if ($statistic->id === null) {
            throw new \RuntimeException('Cannot update non existing day statistics');
        }

        $sql = 'UPDATE `dayStatistics` 
                SET `distance` = :distance, `time` = :time, `calories` = :calories, 
                    `elevation` = :elevation, `activitiesCount` = :activitiesCount WHERE `id` = :id';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('id', $statistic->id);
        $stmt->bindParam('distance', $statistic->distance);
        $stmt->bindParam('time', $statistic->time);
        $stmt->bindParam('calories', $statistic->calories);
        $stmt->bindParam('elevation', $statistic->elevation);
        $stmt->bindParam('activitiesCount', $statistic->activitiesCount);

        $stmt->execute();

        return $statistic;
    }

    /**
     * @inheritDoc
     */
    public function findByCategory(
        Athlete $athlete,
        \DateTimeImmutable $from,
        \DateTimeImmutable $to,
        Category $category
    ): array {
        $sql = 'SELECT * FROM `dayStatistics` WHERE `athleteId` = :athleteId 
                AND `category` = :categoryId AND `date` >= :from AND `date` <= :to ORDER BY `date` DESC';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('athleteId', $athlete->id);
        $stmt->bindValue('categoryId', $category->toId());
        $stmt->bindValue('from', $from->format('Y-m-d'));
        $stmt->bindValue('to', $to->format('Y-m-d'));
        $stmt->execute();
        $data = $stmt->fetchAll();

        if (!$data) {
            return [];
        }

        $result = [];

        foreach ($data as $row) {
            $result[] = $this->createFromDbData($athlete, $row);
        }

        return $result;
    }


    /**
     * @inheritDoc
     */
    public function find(Athlete $athlete, \DateTimeImmutable $from, \DateTimeImmutable $to): array
    {
        $sql = 'SELECT * FROM `dayStatistics` WHERE `athleteId` = :athleteId 
                AND `date` >= :from AND `date` <= :to ORDER BY `date` DESC';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('athleteId', $athlete->id);
        $stmt->bindValue('from', $from->format('Y-m-d'));
        $stmt->bindValue('to', $to->format('Y-m-d'));
        $stmt->execute();
        $data = $stmt->fetchAll();

        if (!$data) {
            return [];
        }

        $result = [];

        foreach ($data as $row) {
            $result[] = $this->createFromDbData($athlete, $row);
        }

        return $result;
    }

    /**
     * @inheritDoc
     */
    public function findOne(Athlete $athlete, \DateTimeImmutable $date, Category $category): ?DayStatistic
    {
        $sql = 'SELECT * FROM `dayStatistics` WHERE `athleteId` = :athleteId 
                AND `category` = :categoryId AND `date` = :date';
        $stmt = $this->db->prepare($sql);
        $stmt->bindParam('athleteId', $athlete->id);
        $stmt->bindValue('categoryId', $category->toId());
        $stmt->bindValue('date', $date->format('Y-m-d'));
        $stmt->execute();
        $data = $stmt->fetch();

        if (!$data) {
            return null;
        }

        return $this->createFromDbData($athlete, $data);
    }

    /**
     * @param array<string, mixed> $data
     * @return DayStatistic
     */
    private function createFromDbData(Athlete $athlete, array $data): DayStatistic
    {
        $category = Category::fromId((int)($data['category'] ?? 0));
        $date = new \DateTimeImmutable($data['date'] ?? '');

        return new DayStatistic(
            (int)($data['id'] ?? 0),
            $athlete,
            $category,
            $date,
            (float)($data['distance'] ?? 0),
            (float)($data['time'] ?? 0),
            (float)($data['calories'] ?? 0),
            (float)($data['elevation'] ?? 0),
            (int)($data['activitiesCount'] ?? 0),
        );
    }

    public function getYearTeamDistanceStatistics(Team $team, int $year, int $minActivityTime,): int
    {
        $sql = 'SELECT SUM(distance) as distance
				FROM activities 
				JOIN athletes ON activities.athleteId = athletes.id
				WHERE athletes.team = :team AND time >= :minActivityTime 
				AND startDatetime >= :minDatetime AND startDatetime <= :maxDatetime';

        $stmt = $this->db->prepare($sql);
        $stmt->bindValue('team', $team->value);
        $stmt->bindValue('minActivityTime', $minActivityTime);
        $stmt->bindValue('minDatetime', "$year-01-01");
        $stmt->bindValue('maxDatetime', "$year-12-31");
        $stmt->execute();
        $data = $stmt->fetch();

        if (!$data) {
            return 0;
        }

        $distance = isset($data['distance']) ? (int)$data['distance'] : 0;

        return (int)round($distance / 1000, 0);
    }
}
