<?php

declare(strict_types=1);

namespace App\Infrastructure\Settings;

use App\Domain\Settings\SettingsRepositoryInterface;

final class InMemorySettingsRepository implements SettingsRepositoryInterface
{
    /**
     * @param array<string, mixed> $settings
     */
    public function __construct(private readonly array $settings)
    {
    }

    public function get(string $key = '', mixed $default = null): mixed
    {
        return empty($key) ? $this->settings : ($this->settings[$key] ?? $default);
    }
}
