<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Athlete;

use App\Application\Actions\ActionPayload;
use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteNotFoundException;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Categories\Category;
use App\Domain\Statistics\DayStatistic;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use DI\Container;
use PHPUnit\Framework\MockObject\MockObject;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Tests\TestCase;

final class AthleteDayStatisticsActionTest extends TestCase
{
    private AthleteRepositoryInterface&MockObject $athleteRepository;
    private DayStatisticsRepositoryInterface&MockObject $dayStatisticsRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $app = $this->createAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $this->athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $this->dayStatisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);

        $container->set(AthleteRepositoryInterface::class, $this->athleteRepository);
        $container->set(DayStatisticsRepositoryInterface::class, $this->dayStatisticsRepository);
    }

    public function testGetAthleteDayStatistics(): void
    {
        $athlete = new Athlete(id: 1);

        $dayStatistics = [
            new DayStatistic(
                id: 1,
                athlete: $athlete,
                category: Category::RUN,
                date: new \DateTimeImmutable('2023-05-15'),
                distance: 100,
            ),
            new DayStatistic(
                id: 3,
                athlete: $athlete,
                category: Category::RUN,
                date: new \DateTimeImmutable('2023-05-18'),
                distance: 20,
            ),
        ];

        $this->athleteRepository->method('getById')->with(1)->willReturn($athlete);

        $this->dayStatisticsRepository->method('findByCategory')->willReturnCallback(
            function (
                Athlete $requestAthlete,
                \DateTimeImmutable $from,
                \DateTimeImmutable $to,
                Category $category
            ) use (
                $athlete,
                $dayStatistics
            ): array {
                $this->assertEquals($athlete, $requestAthlete);
                $this->assertEquals('2023-05-15', $from->format('Y-m-d'));
                $this->assertEquals('2023-05-30', $to->format('Y-m-d'));
                $this->assertEquals(Category::RUN, $category);

                return $dayStatistics;
            }
        );

        $request = $this->createRequest(
            'GET',
            '/athletes/1/dayStatistics?from=2023-05-15&to=2023-05-30&category=run'
        );

        $response = $this->handleRequest($request);

        $payload = (string)$response->getBody();
        $expectedPayload = new ActionPayload(200, $dayStatistics);
        $serializedPayload = json_encode($expectedPayload, JSON_PRETTY_PRINT);

        $this->assertEquals($serializedPayload, $payload);
    }

    public function testGetWithUnknownCategory(): void
    {
        $athlete = new Athlete(id: 1);
        $this->athleteRepository->method('getById')->with(1)->willReturn($athlete);

        $request = $this->createRequest(
            'GET',
            '/athletes/1/dayStatistics?from=2023-05-15&to=2023-05-30&category=xxx'
        );

        $this->expectExceptionMessage('"Category" has invalid value, use "run" or "bike"');
        $this->expectException(HttpBadRequestException::class);

        $this->handleRequest($request);
    }

    public function testGetWithUnknownAthlete(): void
    {
        $this->athleteRepository
            ->method('getById')
            ->with(1)
            ->willThrowException(new AthleteNotFoundException());

        $request = $this->createRequest(
            'GET',
            '/athletes/1/dayStatistics?from=2023-05-15&to=2023-05-30&category=run'
        );

        $this->expectExceptionMessage('The Athlete you requested does not exist.');
        $this->expectException(HttpNotFoundException::class);

        $this->handleRequest($request);
    }

    public function testGetWithInvalidFromDate(): void
    {
        $athlete = new Athlete(id: 1);
        $this->athleteRepository->method('getById')->with(1)->willReturn($athlete);

        $request = $this->createRequest(
            'GET',
            '/athletes/1/dayStatistics?from=xxx&to=2023-05-30&category=bike'
        );

        $this->expectExceptionMessage('"from" has invalid value, use format Y-m-d');
        $this->expectException(HttpBadRequestException::class);

        $this->handleRequest($request);
    }

    public function testGetWithInvalidToDate(): void
    {
        $athlete = new Athlete(id: 1);
        $this->athleteRepository->method('getById')->with(1)->willReturn($athlete);

        $request = $this->createRequest(
            'GET',
            '/athletes/1/dayStatistics?from=2023-05-15&category=bike'
        );

        $this->expectExceptionMessage('"to" has invalid value, use format Y-m-d');
        $this->expectException(HttpBadRequestException::class);

        $this->handleRequest($request);
    }
}
