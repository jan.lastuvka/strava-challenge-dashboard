<?php

declare(strict_types=1);

namespace Application\Actions\Challenge;

use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Categories\Category;
use App\Domain\Statistics\DayStatistic;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use DI\Container;
use PHPUnit\Framework\MockObject\MockObject;
use Slim\Exception\HttpBadRequestException;
use Tests\TestCase;

final class ActivitiesStatsActionTest extends TestCase
{
    private AthleteRepositoryInterface&MockObject $athleteRepository;
    private DayStatisticsRepositoryInterface&MockObject $dayStatisticsRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $app = $this->createAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $this->athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $this->dayStatisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);

        $container->set(AthleteRepositoryInterface::class, $this->athleteRepository);
        $container->set(DayStatisticsRepositoryInterface::class, $this->dayStatisticsRepository);

        $firstAthlete = new Athlete(id: 1);
        $secondAthlete = new Athlete(id: 2);
        $thirdAthlete = new Athlete(id: 2);

        $this->athleteRepository
            ->method('findAll')
            ->willReturn([$firstAthlete, $secondAthlete, $thirdAthlete]);

        $this->dayStatisticsRepository->method('find')->willReturnCallback(
            function (
                Athlete $requestAthlete,
                \DateTimeImmutable $from,
                \DateTimeImmutable $to
            ) use (
                $firstAthlete,
                $secondAthlete,
                $thirdAthlete
            ): array {
                $this->assertEquals('2023-05-15', $from->format('Y-m-d'));
                $this->assertEquals('2023-05-30', $to->format('Y-m-d'));

                if ($requestAthlete === $firstAthlete) {
                    return [
                        new DayStatistic(
                            id: 1,
                            athlete: $firstAthlete,
                            category: Category::RUN,
                            date: new \DateTimeImmutable('2023-05-15'),
                            activitiesCount: 1,
                        ),
                        new DayStatistic(
                            id: 2,
                            athlete: $firstAthlete,
                            category: Category::RUN,
                            date: new \DateTimeImmutable('2023-05-16'),
                            activitiesCount: 1,
                        ),
                        new DayStatistic(
                            id: 3,
                            athlete: $firstAthlete,
                            category: Category::BIKE,
                            date: new \DateTimeImmutable('2023-05-15'),
                            activitiesCount: 1,
                        ),
                        new DayStatistic(
                            id: 4,
                            athlete: $firstAthlete,
                            category: Category::BIKE,
                            date: new \DateTimeImmutable('2023-05-17'),
                            activitiesCount: 1,
                        ),
                        new DayStatistic(
                            id: 5,
                            athlete: $firstAthlete,
                            category: Category::WORKOUTS,
                            date: new \DateTimeImmutable('2023-05-23'),
                            activitiesCount: 2,
                        ),
                        new DayStatistic(
                            id: 6,
                            athlete: $firstAthlete,
                            category: Category::WORKOUTS,
                            date: new \DateTimeImmutable('2023-05-22'),
                            activitiesCount: 1,
                        ),
                    ];
                }


                if ($requestAthlete === $secondAthlete) {
                    return [
                        new DayStatistic(
                            id: 7,
                            athlete: $secondAthlete,
                            category: Category::BIKE,
                            date: new \DateTimeImmutable('2023-05-15'),
                            activitiesCount: 1,
                        ),
                        new DayStatistic(
                            id: 8,
                            athlete: $secondAthlete,
                            category: Category::RUN,
                            date: new \DateTimeImmutable('2023-05-15'),
                            activitiesCount: 1,
                        ),

                        new DayStatistic(
                            id: 9,
                            athlete: $secondAthlete,
                            category: Category::WORKOUTS,
                            date: new \DateTimeImmutable('2023-05-16'),
                            activitiesCount: 1,
                        ),
                        new DayStatistic(
                            id: 10,
                            athlete: $secondAthlete,
                            category: Category::RUN,
                            date: new \DateTimeImmutable('2023-05-17'),
                            activitiesCount: 1,
                        ),
                        new DayStatistic(
                            id: 11,
                            athlete: $secondAthlete,
                            category: Category::RUN,
                            date: new \DateTimeImmutable('2023-05-18'),
                            activitiesCount: 1,
                        ),
                    ];
                }

                if ($requestAthlete === $thirdAthlete) {
                    return [];
                }

                $this->fail('Unknown athlete and category');
            }
        );
    }

    public function testGetActivitiesStats(): void
    {
        $request = $this->createRequest(
            'GET',
            '/challenge/activitiesStats?from=2023-05-15&to=2023-05-30&today=2023-05-23'
        );

        $response = $this->handleRequest($request);

        $this->assertEquals(200, $response->getStatusCode());
        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);

        $firstAthleteData = ['id' => 1, 'name' => '', 'avatar' => '', 'department' => '', 'team' => 'webnode'];
        $secondAthleteData = ['id' => 2, 'name' => '', 'avatar' => '', 'department' => '', 'team' => 'webnode'];

        $this->assertEquals(
            [
                [
                    'athlete' => $firstAthleteData,
                    'weeks' => [20 => 4, 21 => 3, 22 => 0],
                    'avg' => 7 / 2,
                    'total' => 7,
                ],
                [
                    'athlete' => $secondAthleteData,
                    'weeks' => [20 => 5, 21 => 0, 22 => 0],
                    'avg' => 5 / 2,
                    'total' => 5,
                ]
            ],
            $data['data'] ?? []
        );
    }


    public function testInvalidFromTime(): void
    {
        $request = $this->createRequest(
            'GET',
            '/challenge/activitiesStats?to=2023-05-30'
        );

        $this->expectExceptionMessage('"from" has invalid value, use format Y-m-d');
        $this->expectException(HttpBadRequestException::class);
        $this->handleRequest($request);
    }

    public function testInvalidToTime(): void
    {
        $request = $this->createRequest(
            'GET',
            '/challenge/activitiesStats?from=2023-05-30'
        );

        $this->expectExceptionMessage('"to" has invalid value, use format Y-m-d');
        $this->expectException(HttpBadRequestException::class);
        $this->handleRequest($request);
    }
}
