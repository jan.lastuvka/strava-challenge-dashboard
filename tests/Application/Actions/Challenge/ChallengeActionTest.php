<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Challenge;

use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Categories\Category;
use App\Domain\Statistics\DayStatistic;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use DI\Container;
use PHPUnit\Framework\MockObject\MockObject;
use Slim\Exception\HttpBadRequestException;
use Tests\TestCase;

final class ChallengeActionTest extends TestCase
{
    private AthleteRepositoryInterface&MockObject $athleteRepository;
    private DayStatisticsRepositoryInterface&MockObject $dayStatisticsRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $app = $this->createAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $this->athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $this->dayStatisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);

        $container->set(AthleteRepositoryInterface::class, $this->athleteRepository);
        $container->set(DayStatisticsRepositoryInterface::class, $this->dayStatisticsRepository);

        $firstAthlete = new Athlete(id: 1);
        $secondAthlete = new Athlete(id: 2);

        $this->athleteRepository->method('findAll')->willReturn([$firstAthlete, $secondAthlete]);

        $this->dayStatisticsRepository->method('findByCategory')->willReturnCallback(
            function (
                Athlete $requestAthlete,
                \DateTimeImmutable $from,
                \DateTimeImmutable $to,
                Category $category
            ) use (
                $firstAthlete,
                $secondAthlete
            ): array {
                $this->assertEquals('2023-05-15', $from->format('Y-m-d'));
                $this->assertEquals('2023-05-30', $to->format('Y-m-d'));

                if ($requestAthlete === $firstAthlete && $category === Category::RUN) {
                    return [
                        new DayStatistic(
                            id: 1,
                            athlete: $firstAthlete,
                            category: Category::RUN,
                            date: new \DateTimeImmutable('2023-05-15'),
                            distance: 1,
                            time: 2,
                            calories: 3,
                            elevation: 4,
                            activitiesCount: 1,
                        ),
                        new DayStatistic(
                            id: 2,
                            athlete: $firstAthlete,
                            category: Category::RUN,
                            date: new \DateTimeImmutable('2023-05-18'),
                            distance: 1,
                            time: 2,
                            calories: 3,
                            elevation: 4,
                            activitiesCount: 1,
                        ),
                    ];
                }

                if ($requestAthlete === $firstAthlete && $category === Category::BIKE) {
                    return [
                        new DayStatistic(
                            id: 3,
                            athlete: $firstAthlete,
                            category: Category::BIKE,
                            date: new \DateTimeImmutable('2023-05-18'),
                            distance: 4,
                            time: 3,
                            calories: 2,
                            elevation: 1,
                            activitiesCount: 1,
                        ),
                        new DayStatistic(
                            id: 4,
                            athlete: $firstAthlete,
                            category: Category::BIKE,
                            date: new \DateTimeImmutable('2023-05-18'),
                            distance: 4,
                            time: 3,
                            calories: 2,
                            elevation: 1,
                            activitiesCount: 1,
                        ),
                    ];
                }

                if ($requestAthlete === $secondAthlete && $category === Category::RUN) {
                    return [
                        new DayStatistic(
                            id: 5,
                            athlete: $secondAthlete,
                            category: Category::RUN,
                            date: new \DateTimeImmutable('2023-05-15'),
                            distance: 4,
                            time: 3,
                            calories: 2,
                            elevation: 1,
                            activitiesCount: 1,
                        ),
                        new DayStatistic(
                            id: 6,
                            athlete: $secondAthlete,
                            category: Category::RUN,
                            date: new \DateTimeImmutable('2023-05-15'),
                            distance: 4,
                            time: 3,
                            calories: 2,
                            elevation: 1,
                            activitiesCount: 1,
                        ),
                    ];
                }

                if ($requestAthlete === $secondAthlete && $category === Category::BIKE) {
                    return [
                        new DayStatistic(
                            id: 5,
                            athlete: $secondAthlete,
                            category: Category::BIKE,
                            date: new \DateTimeImmutable('2023-05-15'),
                            distance: 1,
                            time: 2,
                            calories: 3,
                            elevation: 4,
                            activitiesCount: 1,
                        ),
                        new DayStatistic(
                            id: 6,
                            athlete: $secondAthlete,
                            category: Category::BIKE,
                            date: new \DateTimeImmutable('2023-05-15'),
                            distance: 1,
                            time: 2,
                            calories: 3,
                            elevation: 4,
                            activitiesCount: 1,
                        ),
                    ];
                }

                if ($requestAthlete === $firstAthlete && $category === Category::WORKOUTS) {
                    return [
                        new DayStatistic(
                            id: 7,
                            athlete: $firstAthlete,
                            category: Category::WORKOUTS,
                            date: new \DateTimeImmutable('2023-05-15'),
                            distance: 0,
                            time: 2,
                            calories: 3,
                            elevation: 4,
                            activitiesCount: 2,
                        ),
                        new DayStatistic(
                            id: 8,
                            athlete: $firstAthlete,
                            category: Category::WORKOUTS,
                            date: new \DateTimeImmutable('2023-05-15'),
                            distance: 0,
                            time: 2,
                            calories: 3,
                            elevation: 4,
                            activitiesCount: 1,
                        ),
                    ];
                }

                if ($requestAthlete === $secondAthlete && $category === Category::WORKOUTS) {
                    return [
                        new DayStatistic(
                            id: 9,
                            athlete: $secondAthlete,
                            category: Category::WORKOUTS,
                            date: new \DateTimeImmutable('2023-05-15'),
                            distance: 0,
                            time: 2,
                            calories: 3,
                            elevation: 4,
                            activitiesCount: 1,
                        ),
                    ];
                }

                $this->fail('Unknown athlete and category');
            }
        );
    }

    public function testGetChallengeDataOrderedByDistance(): void
    {
        $request = $this->createRequest(
            'GET',
            '/challenge?from=2023-05-15&to=2023-05-30&orderBy=distance'
        );

        $response = $this->handleRequest($request);

        $this->assertEquals(200, $response->getStatusCode());
        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);

        $firstAthleteData = ['id' => 1, 'name' => '', 'avatar' => '', 'department' => '', 'team' => 'webnode'];
        $secondAthleteData = ['id' => 2, 'name' => '', 'avatar' => '', 'department' => '', 'team' => 'webnode'];

        $this->assertEquals(
            [
                'run' => [
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 8,
                        'time' => 6,
                        'calories' => 4,
                        'elevation' => 2,
                        'activitiesCount' => 2,
                    ],
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 2,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 2,
                    ],
                ],
                'bike' => [
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 8,
                        'time' => 6,
                        'calories' => 4,
                        'elevation' => 2,
                        'activitiesCount' => 2,
                    ],
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 2,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 2,
                    ],
                ],
                'workouts' => [
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 0,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 3,
                    ],
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 0,
                        'time' => 2,
                        'calories' => 3,
                        'elevation' => 4,
                        'activitiesCount' => 1,
                    ],
                ],
            ],
            $data['data'] ?? []
        );
    }

    public function testGetChallengeDataOrderedByCalories(): void
    {
        $request = $this->createRequest(
            'GET',
            '/challenge?from=2023-05-15&to=2023-05-30&orderBy=calories'
        );

        $response = $this->handleRequest($request);

        $this->assertEquals(200, $response->getStatusCode());
        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);

        $firstAthleteData = ['id' => 1, 'name' => '', 'avatar' => '', 'department' => '', 'team' => 'webnode'];
        $secondAthleteData = ['id' => 2, 'name' => '', 'avatar' => '', 'department' => '', 'team' => 'webnode'];

        $this->assertEquals(
            [
                'run' => [
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 2,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 2,
                    ],
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 8,
                        'time' => 6,
                        'calories' => 4,
                        'elevation' => 2,
                        'activitiesCount' => 2,
                    ],
                ],
                'bike' => [
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 2,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 2,
                    ],
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 8,
                        'time' => 6,
                        'calories' => 4,
                        'elevation' => 2,
                        'activitiesCount' => 2,
                    ],
                ],

                'workouts' => [
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 0,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 3,
                    ],
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 0,
                        'time' => 2,
                        'calories' => 3,
                        'elevation' => 4,
                        'activitiesCount' => 1,
                    ],
                ],
            ],
            $data['data'] ?? []
        );
    }

    public function testGetChallengeDataOrderedByTime(): void
    {
        $request = $this->createRequest(
            'GET',
            '/challenge?from=2023-05-15&to=2023-05-30&orderBy=time'
        );

        $response = $this->handleRequest($request);

        $this->assertEquals(200, $response->getStatusCode());
        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);

        $firstAthleteData = ['id' => 1, 'name' => '', 'avatar' => '', 'department' => '', 'team' => 'webnode'];
        $secondAthleteData = ['id' => 2, 'name' => '', 'avatar' => '', 'department' => '', 'team' => 'webnode'];

        $this->assertEquals(
            [
                'run' => [
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 8,
                        'time' => 6,
                        'calories' => 4,
                        'elevation' => 2,
                        'activitiesCount' => 2,
                    ],
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 2,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 2,
                    ],
                ],
                'bike' => [
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 8,
                        'time' => 6,
                        'calories' => 4,
                        'elevation' => 2,
                        'activitiesCount' => 2,
                    ],
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 2,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 2,
                    ],
                ],
                'workouts' => [
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 0,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 3,
                    ],
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 0,
                        'time' => 2,
                        'calories' => 3,
                        'elevation' => 4,
                        'activitiesCount' => 1,
                    ],
                ],
            ],
            $data['data'] ?? []
        );
    }

    public function testGetChallengeDataOrderedByElevation(): void
    {
        $request = $this->createRequest(
            'GET',
            '/challenge?from=2023-05-15&to=2023-05-30&orderBy=elevation'
        );

        $response = $this->handleRequest($request);

        $this->assertEquals(200, $response->getStatusCode());
        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);

        $firstAthleteData = ['id' => 1, 'name' => '', 'avatar' => '', 'department' => '', 'team' => 'webnode'];
        $secondAthleteData = ['id' => 2, 'name' => '', 'avatar' => '', 'department' => '', 'team' => 'webnode'];

        $this->assertEquals(
            [
                'run' => [
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 2,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 2,
                    ],
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 8,
                        'time' => 6,
                        'calories' => 4,
                        'elevation' => 2,
                        'activitiesCount' => 2,
                    ],
                ],
                'bike' => [
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 2,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 2,
                    ],
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 8,
                        'time' => 6,
                        'calories' => 4,
                        'elevation' => 2,
                        'activitiesCount' => 2,
                    ],
                ],

                'workouts' => [
                    [
                        'athlete' => $firstAthleteData,
                        'distance' => 0,
                        'time' => 4,
                        'calories' => 6,
                        'elevation' => 8,
                        'activitiesCount' => 3,
                    ],
                    [
                        'athlete' => $secondAthleteData,
                        'distance' => 0,
                        'time' => 2,
                        'calories' => 3,
                        'elevation' => 4,
                        'activitiesCount' => 1,
                    ],
                ],
            ],
            $data['data'] ?? []
        );
    }

    public function testGetEmptyChallengeData(): void
    {
        $request = $this->createRequest(
            'GET',
            '/challenge?from=2023-05-15&to=2023-05-30&orderBy=distance'
        );

        $athlete = new Athlete(id: 1);
        $athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $athleteRepository->method('findAll')->willReturn([$athlete]);

        $dayStatisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $dayStatisticsRepository->method('findByCategory')->willReturn([]);

        $app = $this->getAppInstance();
        /** @var Container $container */
        $container = $app->getContainer();

        $container->set(AthleteRepositoryInterface::class, $athleteRepository);
        $container->set(DayStatisticsRepositoryInterface::class, $dayStatisticsRepository);

        $response = $this->handleRequest($request);

        $this->assertEquals(200, $response->getStatusCode());
        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);

        $this->assertEquals(
            [
                'run' => [],
                'bike' => [],
                'workouts' => [],
            ],
            $data['data'] ?? []
        );
    }

    public function testUnknownOrderByValue(): void
    {
        $request = $this->createRequest(
            'GET',
            '/challenge?from=2023-05-15&to=2023-05-30&orderBy=xxx'
        );

        $this->expectExceptionMessage(
            '"orderBy" has invalid value, use: "distance", "time", "calories", "elevation" or "activitiesCount"'
        );
        $this->expectException(HttpBadRequestException::class);
        $this->handleRequest($request);
    }

    public function testInvalidFromTime(): void
    {
        $request = $this->createRequest(
            'GET',
            '/challenge?to=2023-05-30&orderBy=distance'
        );

        $this->expectExceptionMessage('"from" has invalid value, use format Y-m-d');
        $this->expectException(HttpBadRequestException::class);
        $this->handleRequest($request);
    }

    public function testInvalidToTime(): void
    {
        $request = $this->createRequest(
            'GET',
            '/challenge?from=2023-05-30&orderBy=distance'
        );

        $this->expectExceptionMessage('"to" has invalid value, use format Y-m-d');
        $this->expectException(HttpBadRequestException::class);
        $this->handleRequest($request);
    }
}
