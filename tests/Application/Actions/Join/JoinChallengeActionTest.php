<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Join;

use App\Domain\Activity\Activity;
use App\Domain\Activity\ActivityProcessor;
use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteNotFoundException;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Athlete\Team;
use App\Domain\Categories\Category;
use App\Domain\Statistics\DayStatistic;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use App\Domain\Strava\StravaActivity;
use App\Infrastructure\API\Strava\StravaAPIClient;
use App\Infrastructure\OAuth2\StravaOAuth2Provider;
use DI\Container;
use League\OAuth2\Client\Token\AccessTokenInterface;
use PHPUnit\Framework\MockObject\MockObject;
use Tests\TestCase;

final class JoinChallengeActionTest extends TestCase
{
    private readonly StravaOAuth2Provider&MockObject $stravaOAuth2Provider;

    private readonly AthleteRepositoryInterface&MockObject $athleteRepositoryInterface;

    private readonly ActivityProcessor&MockObject $activityProcessor;

    private readonly StravaAPIClient&MockObject $stravaAPIClient;
    private readonly DayStatisticsRepositoryInterface&MockObject $dayStatisticsRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->stravaOAuth2Provider = $this->createMock(StravaOAuth2Provider::class);
        $this->athleteRepositoryInterface = $this->createMock(AthleteRepositoryInterface::class);
        $this->activityProcessor = $this->createMock(ActivityProcessor::class);
        $this->stravaAPIClient = $this->createMock(StravaAPIClient::class);
        $this->dayStatisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);

        $app = $this->createAppInstance();
        /** @var Container $container */
        $container = $app->getContainer();

        $container->set(StravaOAuth2Provider::class, $this->stravaOAuth2Provider);
        $container->set(AthleteRepositoryInterface::class, $this->athleteRepositoryInterface);
        $container->set(ActivityProcessor::class, $this->activityProcessor);
        $container->set(StravaAPIClient::class, $this->stravaAPIClient);
        $container->set(DayStatisticsRepositoryInterface::class, $this->dayStatisticsRepository);
    }

    public function testItRedirectsToStravaWhenCodeIsMissing(): void
    {
        $_ENV['STRAVA_CLIENT_ID'] = '123';
        $_ENV['APP_URL'] = 'https://example.com';
        $_ENV['CHALLENGE_START_DATE'] = '2023-05-01';

        $request = $this->createRequest(
            'GET',
            '/join'
        );

        $response = $this->handleRequest($request);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals(
            [
                'https://www.strava.com/oauth/authorize?' .
                'client_id=123&redirect_uri=https%3A%2F%2Fexample.com%2Fjoin&response_type=code' .
                '&approval_prompt=auto&scope=read%2Cactivity%3Aread_all&state=join-webnode'
            ],
            $response->getHeader('Location')
        );
    }

    public function testItRedirectsToStravaWhenCodeIsMissingWithProvidedTeam(): void
    {
        $_ENV['STRAVA_CLIENT_ID'] = '123';
        $_ENV['APP_URL'] = 'https://example.com';
        $_ENV['CHALLENGE_START_DATE'] = '2023-05-01';

        $request = $this->createRequest(
            'GET',
            '/join?team=iubenda'
        );

        $response = $this->handleRequest($request);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals(
            [
                'https://www.strava.com/oauth/authorize?' .
                'client_id=123&redirect_uri=https%3A%2F%2Fexample.com%2Fjoin&response_type=code' .
                '&approval_prompt=auto&scope=read%2Cactivity%3Aread_all&state=join-iubenda'
            ],
            $response->getHeader('Location')
        );
    }

    public function testItRedirectsBackToStravaWhenScopeIsInvalid(): void
    {
        $_ENV['STRAVA_CLIENT_ID'] = '123';
        $_ENV['APP_URL'] = 'https://example.com';
        $_ENV['CHALLENGE_START_DATE'] = '2023-05-01';

        $request = $this->createRequest(
            'GET',
            '/join?code=aaa&scope=read'
        );

        $response = $this->handleRequest($request);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals(
            [
                'https://www.strava.com/oauth/authorize?' .
                'client_id=123&redirect_uri=https%3A%2F%2Fexample.com%2Fjoin&response_type=code' .
                '&approval_prompt=auto&scope=read%2Cactivity%3Aread_all&state=join-webnode'
            ],
            $response->getHeader('Location')
        );
    }

    public function testItRedirectsBackToStravaWhenScopeIsInvalidWithSetState(): void
    {
        $_ENV['STRAVA_CLIENT_ID'] = '123';
        $_ENV['APP_URL'] = 'https://example.com';
        $_ENV['CHALLENGE_START_DATE'] = '2023-05-01';

        $request = $this->createRequest(
            'GET',
            '/join?code=aaa&scope=read&state=join-iubenda'
        );

        $response = $this->handleRequest($request);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals(
            [
                'https://www.strava.com/oauth/authorize?' .
                'client_id=123&redirect_uri=https%3A%2F%2Fexample.com%2Fjoin&response_type=code' .
                '&approval_prompt=auto&scope=read%2Cactivity%3Aread_all&state=join-iubenda'
            ],
            $response->getHeader('Location')
        );
    }

    public function testItRedirectsBackToStravaWhenItIsUnableToGetAccessToken(): void
    {
        $_ENV['STRAVA_CLIENT_ID'] = '123';
        $_ENV['APP_URL'] = 'https://example.com';
        $_ENV['CHALLENGE_START_DATE'] = '2023-05-01';

        $request = $this->createRequest(
            'GET',
            '/join?code=aaa&scope=read%2Cactivity%3Aread_all'
        );

        $this->stravaOAuth2Provider
            ->expects($this->once())
            ->method('getAccessToken')
            ->with('authorization_code', ['code' => 'aaa'])
            ->willThrowException(new \Exception());

        $response = $this->handleRequest($request);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals(
            [
                'https://www.strava.com/oauth/authorize?' .
                'client_id=123&redirect_uri=https%3A%2F%2Fexample.com%2Fjoin&response_type=code' .
                '&approval_prompt=auto&scope=read%2Cactivity%3Aread_all&state=join-webnode'
            ],
            $response->getHeader('Location')
        );
    }

    public function testItRegisterNewAthlete(): void
    {
        $_ENV['APP_URL'] = 'https://example.com';
        $_ENV['CHALLENGE_START_DATE'] = '2023-05-01';

        $request = $this->createRequest(
            'GET',
            '/join?code=aaa&scope=read%2Cactivity%3Aread_all&state=join-iubenda'
        );

        $token = $this->createMock(AccessTokenInterface::class);
        $token->expects($this->once())->method('getToken')->willReturn('xxx');
        $token->expects($this->once())->method('getRefreshToken')->willReturn('yyy');
        $token
            ->expects($this->exactly(2))
            ->method('getValues')
            ->willReturn([
                'athlete' => [
                    'id' => 123,
                    'firstname' => 'John',
                    'lastname' => 'Doe',
                    'sex' => 'M',
                    'profile' => 'https://example.com/avatar.jpg',
                    'username' => 'test',
                ]
            ]);

        $this->athleteRepositoryInterface
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(123)
            ->willThrowException(new AthleteNotFoundException());

        $this->athleteRepositoryInterface
            ->expects($this->once())
            ->method('create')
            ->willReturnCallback(function (Athlete $athlete): Athlete {
                $this->assertEquals(null, $athlete->id);
                $this->assertEquals(123, $athlete->stravaId);
                $this->assertEquals('xxx', $athlete->accessToken);
                $this->assertEquals('yyy', $athlete->refreshToken);
                $this->assertEquals('M', $athlete->sex);
                $this->assertEquals('John', $athlete->firstname);
                $this->assertEquals('Doe', $athlete->lastname);
                $this->assertEquals('test', $athlete->username);
                $this->assertEquals('', $athlete->department);
                $this->assertEquals(Team::IUBENDA, $athlete->team);
                $this->assertEquals('https://example.com/avatar.jpg', $athlete->avatar);
                $this->assertEquals(
                    (new \DateTimeImmutable())->format('Y-m-d'),
                    $athlete->joinDate->format('Y-m-d')
                );

                $athlete->id = 1;

                return $athlete;
            });

        $this->stravaOAuth2Provider
            ->expects($this->once())
            ->method('getAccessToken')
            ->with('authorization_code', ['code' => 'aaa'])
            ->willReturn($token);

        $categories = [Category::BIKE, Category::RUN, Category::WORKOUTS];
        $this->dayStatisticsRepository
            ->expects($this->exactly(3))
            ->method('create')
            ->willReturnCallback(function (DayStatistic $dayStatistic) use (&$categories): DayStatistic {
                $this->assertEquals(null, $dayStatistic->id);
                $this->assertEquals(1, $dayStatistic->athlete->id);
                $category = array_shift($categories);
                $this->assertEquals($category, $dayStatistic->category);
                $this->assertEquals(0, $dayStatistic->distance);
                $this->assertEquals(0, $dayStatistic->time);
                $this->assertEquals(0, $dayStatistic->calories);
                $this->assertEquals(0, $dayStatistic->elevation);
                $this->assertEquals(0, $dayStatistic->activitiesCount);
                $this->assertEquals('2023-05-01', $dayStatistic->date->format('Y-m-d'));

                return $dayStatistic;
            });

        $stravaActivity1 = $this->createMock(StravaActivity::class);
        $stravaActivity2 = $this->createMock(StravaActivity::class);
        $this->stravaAPIClient
            ->expects($this->once())
            ->method('loadLastAthleteActivities')
            ->willReturn([$stravaActivity1, $stravaActivity2]);
        $this->activityProcessor
            ->expects($this->exactly(2))
            ->method('processAthleteActivity')
            ->willReturnCallback(function (Athlete $athlete, StravaActivity $stravaActivity): Activity {
                $this->assertEquals(1, $athlete->id);
                return new Activity();
            });

        $response = $this->handleRequest($request);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals(
            [
                'https://example.com?joined'
            ],
            $response->getHeader('Location')
        );
    }

    public function testExistingAthleteWithoutJoinDateIsJoining(): void
    {
        $_ENV['APP_URL'] = 'https://example.com';
        $_ENV['CHALLENGE_START_DATE'] = '2023-05-01';

        $request = $this->createRequest(
            'GET',
            '/join?code=aaa&scope=read%2Cactivity%3Aread_all&state=join-webnode'
        );

        $token = $this->createMock(AccessTokenInterface::class);
        $token->expects($this->once())->method('getToken')->willReturn('xxx');
        $token->expects($this->once())->method('getRefreshToken')->willReturn('yyy');
        $token
            ->expects($this->exactly(2))
            ->method('getValues')
            ->willReturn([
                'athlete' => [
                    'id' => 123,
                    'firstname' => 'John',
                    'lastname' => 'Doe',
                    'sex' => 'M',
                    'profile' => 'https://example.com/avatar.jpg',
                    'username' => 'test',
                ]
            ]);

        $athlete = new Athlete(
            1,
            'aaa',
            'bbb',
            123,
            'x',
            'Joe',
            'Noe',
            'F',
            'nothing',
            'CC',
            null,
            Team::IUBENDA
        );

        $this->athleteRepositoryInterface
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(123)
            ->willReturn($athlete);

        $this->athleteRepositoryInterface
            ->expects($this->once())
            ->method('update')
            ->willReturnCallback(function (Athlete $athlete): Athlete {
                $this->assertEquals(1, $athlete->id);
                $this->assertEquals(123, $athlete->stravaId);
                $this->assertEquals('xxx', $athlete->accessToken);
                $this->assertEquals('yyy', $athlete->refreshToken);
                $this->assertEquals('M', $athlete->sex);
                $this->assertEquals('John', $athlete->firstname);
                $this->assertEquals('Doe', $athlete->lastname);
                $this->assertEquals('test', $athlete->username);
                $this->assertEquals('CC', $athlete->department);
                $this->assertEquals('https://example.com/avatar.jpg', $athlete->avatar);
                $this->assertEquals(Team::WEBNODE, $athlete->team);
                $this->assertEquals(
                    (new \DateTimeImmutable())->format('Y-m-d'),
                    $athlete->joinDate->format('Y-m-d')
                );

                return $athlete;
            });

        $this->stravaOAuth2Provider
            ->expects($this->once())
            ->method('getAccessToken')
            ->with('authorization_code', ['code' => 'aaa'])
            ->willReturn($token);

        $categories = [Category::BIKE, Category::RUN, Category::WORKOUTS];
        $this->dayStatisticsRepository
            ->expects($this->exactly(3))
            ->method('create')
            ->willReturnCallback(function (DayStatistic $dayStatistic) use (&$categories): DayStatistic {
                $this->assertEquals(null, $dayStatistic->id);
                $this->assertEquals(1, $dayStatistic->athlete->id);
                $category = array_shift($categories);
                $this->assertEquals($category, $dayStatistic->category);
                $this->assertEquals(0, $dayStatistic->distance);
                $this->assertEquals(0, $dayStatistic->time);
                $this->assertEquals(0, $dayStatistic->calories);
                $this->assertEquals(0, $dayStatistic->elevation);
                $this->assertEquals(0, $dayStatistic->activitiesCount);

                return $dayStatistic;
            });

        $stravaActivity1 = $this->createMock(StravaActivity::class);
        $stravaActivity2 = $this->createMock(StravaActivity::class);
        $this->stravaAPIClient
            ->expects($this->once())
            ->method('loadLastAthleteActivities')
            ->willReturn([$stravaActivity1, $stravaActivity2]);
        $this->activityProcessor
            ->expects($this->exactly(2))
            ->method('processAthleteActivity')
            ->willReturnMap([
                [$athlete, $stravaActivity1, new Activity(1)],
                [$athlete, $stravaActivity2, new Activity(2)],
            ]);

        $response = $this->handleRequest($request);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals(
            [
                'https://example.com?joined'
            ],
            $response->getHeader('Location')
        );
    }

    public function testItUpdatesExistingAthlete(): void
    {
        $_ENV['APP_URL'] = 'https://example.com';
        $_ENV['CHALLENGE_START_DATE'] = '2023-05-01';

        $request = $this->createRequest(
            'GET',
            '/join?code=aaa&scope=read%2Cactivity%3Aread_all&state=join-webnode'
        );

        $token = $this->createMock(AccessTokenInterface::class);
        $token->expects($this->once())->method('getToken')->willReturn('xxx');
        $token->expects($this->once())->method('getRefreshToken')->willReturn('yyy');
        $token
            ->expects($this->exactly(2))
            ->method('getValues')
            ->willReturn([
                'athlete' => [
                    'id' => 123,
                    'firstname' => 'John',
                    'lastname' => 'Doe',
                    'sex' => 'M',
                    'profile' => 'https://example.com/avatar.jpg',
                    'username' => 'test',
                ]
            ]);

        $athlete = new Athlete(
            1,
            'aaa',
            'bbb',
            123,
            'x',
            'Joe',
            'Noe',
            'F',
            'nothing',
            'CC',
            new \DateTimeImmutable('2023-05-15'),
            Team::IUBENDA
        );

        $this->athleteRepositoryInterface
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(123)
            ->willReturn($athlete);

        $this->athleteRepositoryInterface
            ->expects($this->once())
            ->method('update')
            ->willReturnCallback(function (Athlete $athlete): Athlete {
                $this->assertEquals(1, $athlete->id);
                $this->assertEquals(123, $athlete->stravaId);
                $this->assertEquals('xxx', $athlete->accessToken);
                $this->assertEquals('yyy', $athlete->refreshToken);
                $this->assertEquals('M', $athlete->sex);
                $this->assertEquals('John', $athlete->firstname);
                $this->assertEquals('Doe', $athlete->lastname);
                $this->assertEquals('test', $athlete->username);
                $this->assertEquals('CC', $athlete->department);
                $this->assertEquals('https://example.com/avatar.jpg', $athlete->avatar);
                $this->assertEquals(Team::WEBNODE, $athlete->team);
                $this->assertEquals(
                    '2023-05-15',
                    $athlete->joinDate->format('Y-m-d')
                );

                return $athlete;
            });

        $this->stravaOAuth2Provider
            ->expects($this->once())
            ->method('getAccessToken')
            ->with('authorization_code', ['code' => 'aaa'])
            ->willReturn($token);

        $this->dayStatisticsRepository->expects($this->never())->method('create');

        $stravaActivity1 = $this->createMock(StravaActivity::class);
        $stravaActivity2 = $this->createMock(StravaActivity::class);
        $this->stravaAPIClient
            ->expects($this->once())
            ->method('loadLastAthleteActivities')
            ->willReturn([$stravaActivity1, $stravaActivity2]);
        $this->activityProcessor
            ->expects($this->exactly(2))
            ->method('processAthleteActivity')
            ->willReturnMap([
                [$athlete, $stravaActivity1, new Activity(1)],
                [$athlete, $stravaActivity2, new Activity(2)],
            ]);

        $response = $this->handleRequest($request);

        $this->assertEquals(302, $response->getStatusCode());
        $this->assertEquals(
            [
                'https://example.com?joined'
            ],
            $response->getHeader('Location')
        );
    }
}
