<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Statistics;

use App\Domain\Athlete\Team;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use DI\Container;
use PHPUnit\Framework\MockObject\MockObject;
use Slim\Exception\HttpBadRequestException;
use Tests\TestCase;

final class TeamStatisticsActionTest extends TestCase
{
    private readonly DayStatisticsRepositoryInterface&MockObject $dayStatisticsRepository;

    protected function setUp(): void
    {
        parent::setUp();

        $this->dayStatisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);

        $app = $this->createAppInstance();
        /** @var Container $container */
        $container = $app->getContainer();

        $container->set(DayStatisticsRepositoryInterface::class, $this->dayStatisticsRepository);
    }

    public function testItGetsTeamStatistic(): void
    {
        $this->dayStatisticsRepository
            ->expects($this->once())
            ->method('getYearTeamDistanceStatistics')
            ->with(Team::WEBNODE, 2023, 1800)
            ->willReturn(100);

        $request = $this->createRequest(
            'GET',
            '/statistics/team?team=webnode&year=2023'
        );

        $response = $this->handleRequest($request);

        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);

        $this->assertEquals(
            ['totalDistance' => 100],
            $data['data']
        );
    }

    public function testItThrowsExceptionWhenYearIsInvalid(): void
    {
        $request = $this->createRequest(
            'GET',
            '/statistics/team?team=webnode&year=1'
        );

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('It is not possible to count statistics for this year');
        $this->handleRequest($request);
    }

    public function testItThrowsExceptionWhenTeamDoesNotExist(): void
    {
        $request = $this->createRequest(
            'GET',
            '/statistics/team?team=unknown&year=2023'
        );

        $this->expectException(HttpBadRequestException::class);
        $this->expectExceptionMessage('"team" has invalid value, unknown team');
        $this->handleRequest($request);
    }
}
