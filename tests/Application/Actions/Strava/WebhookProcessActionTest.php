<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Strava;

use App\Domain\Activity\Activity;
use App\Domain\Activity\ActivityNotFoundException;
use App\Domain\Activity\ActivityProcessor;
use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteNotFoundException;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use App\Domain\Strava\StravaActivity;
use App\Infrastructure\API\Strava\StravaAPIClient;
use DI\Container;
use PHPUnit\Framework\MockObject\MockObject;
use Slim\Exception\HttpUnauthorizedException;
use Tests\TestCase;

final class WebhookProcessActionTest extends TestCase
{
    private AthleteRepositoryInterface&MockObject $athleteRepository;
    private ActivityProcessor&MockObject $activityProcessor;
    private StravaAPIClient&MockObject $stravaAPIClient;

    protected function setUp(): void
    {
        parent::setUp();

        $_ENV['STRAVA_SUBSCRIPTION_ID'] = 123;

        $app = $this->createAppInstance();

        /** @var Container $container */
        $container = $app->getContainer();

        $this->athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $this->activityProcessor = $this->createMock(ActivityProcessor::class);
        $this->stravaAPIClient = $this->createMock(StravaAPIClient::class);

        $container->set(AthleteRepositoryInterface::class, $this->athleteRepository);
        $container->set(ActivityProcessor::class, $this->activityProcessor);
        $container->set(StravaAPIClient::class, $this->stravaAPIClient);
    }

    public function testItThrowsExceptionWhenSubscriptionIdIsMissing(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([]);

        $this->expectException(HttpUnauthorizedException::class);
        $this->handleRequest($request);
    }

    public function testItThrowsExceptionWhenSubscriptionIdIsInvalid(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody(['subscription_id' => 111]);

        $this->expectException(HttpUnauthorizedException::class);
        $this->handleRequest($request);
    }

    public function testItReturnsResultOnUnknownAction(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'fuuuu'
        ]);

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => 'unknown_action'], $data);
    }

    public function testItDeletesActivity(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'delete',
            'object_type' => 'activity',
            'owner_id' => 111,
            'object_id' => 222,
        ]);

        $athlete = new Athlete(id: 1, stravaId: 111);

        $this->athleteRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(111)
            ->willReturn($athlete);

        $this->activityProcessor
            ->expects($this->once())
            ->method('processStravaActivityDelete')
            ->with($athlete, 222)
            ->willReturn(true);

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => true], $data);
    }

    public function testItDeletesActivityReturnsFalseOnDeleteFail(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'delete',
            'object_type' => 'activity',
            'owner_id' => 111,
            'object_id' => 222,
        ]);

        $athlete = new Athlete(id: 1, stravaId: 111);

        $this->athleteRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(111)
            ->willReturn($athlete);

        $this->activityProcessor
            ->expects($this->once())
            ->method('processStravaActivityDelete')
            ->with($athlete, 222)
            ->willReturn(false);

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => false], $data);
    }

    public function testDeleteActivityWithUnknownAthleteReturnsFalse(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'delete',
            'object_type' => 'activity',
            'owner_id' => 111,
            'object_id' => 222,
        ]);

        $this->athleteRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(111)
            ->willThrowException(new AthleteNotFoundException());

        $this->activityProcessor
            ->expects($this->never())
            ->method('processStravaActivityDelete');

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => false], $data);
    }

    public function testDeleteEventWithUnknownObjectReturnsFalse(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'delete',
            'object_type' => 'fuuu',
            'owner_id' => 111,
            'object_id' => 222,
        ]);

        $this->athleteRepository
            ->expects($this->never())
            ->method('getByStravaId');

        $this->activityProcessor
            ->expects($this->never())
            ->method('processStravaActivityDelete');

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => false], $data);
    }

    public function testItCreatesActivity(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'create',
            'object_type' => 'activity',
            'owner_id' => 111,
            'object_id' => 222,
        ]);

        $athlete = new Athlete(id: 1, stravaId: 111);

        $this->athleteRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(111)
            ->willReturn($athlete);

        $stravaActivity = new StravaActivity(id: 222);

        $this->stravaAPIClient
            ->expects($this->once())
            ->method('loadAthleteActivityById')
            ->with($athlete, 222)
            ->willReturn($stravaActivity);

        $this->activityProcessor
            ->expects($this->once())
            ->method('processNewStravaActivity')
            ->with($athlete, $stravaActivity)
            ->willReturn(new Activity(1));

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => true], $data);
    }

    public function testCreateActivityWithUnknownAthleteReturnsFalse(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'create',
            'object_type' => 'activity',
            'owner_id' => 111,
            'object_id' => 222,
        ]);


        $this->athleteRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(111)
            ->willThrowException(new AthleteNotFoundException());

        $this->stravaAPIClient
            ->expects($this->never())
            ->method('loadAthleteActivityById');

        $this->activityProcessor
            ->expects($this->never())
            ->method('processNewStravaActivity');

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => false], $data);
    }

    public function testCreateActivityReturnsFalseWhenItsUnableToLoadActivity(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'create',
            'object_type' => 'activity',
            'owner_id' => 111,
            'object_id' => 222,
        ]);


        $athlete = new Athlete(id: 1, stravaId: 111);

        $this->athleteRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(111)
            ->willReturn($athlete);

        $this->stravaAPIClient
            ->expects($this->once())
            ->method('loadAthleteActivityById')
            ->with($athlete, 222)
            ->willReturn(null);

        $this->activityProcessor
            ->expects($this->never())
            ->method('processNewStravaActivity');

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => false], $data);
    }

    public function testCreateEventWithUnknownObjectReturnsFalse(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'create',
            'object_type' => 'fuuu',
            'owner_id' => 111,
            'object_id' => 222,
        ]);


        $this->athleteRepository
            ->expects($this->never())
            ->method('getByStravaId');

        $this->stravaAPIClient
            ->expects($this->never())
            ->method('loadAthleteActivityById');

        $this->activityProcessor
            ->expects($this->never())
            ->method('processNewStravaActivity');

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => false], $data);
    }

    public function testItUpdatesActivity(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'update',
            'object_type' => 'activity',
            'owner_id' => 111,
            'object_id' => 222,
            'updates' => ['type' => 'Workout'],
        ]);

        $athlete = new Athlete(id: 1, stravaId: 111);

        $this->athleteRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(111)
            ->willReturn($athlete);

        $stravaActivity = new StravaActivity(id: 222);

        $this->stravaAPIClient
            ->expects($this->once())
            ->method('loadAthleteActivityById')
            ->with($athlete, 222)
            ->willReturn($stravaActivity);

        $this->activityProcessor
            ->expects($this->once())
            ->method('processStravaActivityUpdate')
            ->with($athlete, $stravaActivity)
            ->willReturn(new Activity(1));

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => true], $data);
    }

    public function testUpdateActivityWithUnknownAthleteReturnsFalse(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'update',
            'object_type' => 'activity',
            'owner_id' => 111,
            'object_id' => 222,
            'updates' => ['type' => 'Workout'],
        ]);


        $this->athleteRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(111)
            ->willThrowException(new AthleteNotFoundException());

        $this->stravaAPIClient
            ->expects($this->never())
            ->method('loadAthleteActivityById');

        $this->activityProcessor
            ->expects($this->never())
            ->method('processStravaActivityUpdate');

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => false], $data);
    }

    public function testUpdateActivityReturnsFalseWhenItsUnableToLoadActivity(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'update',
            'object_type' => 'activity',
            'owner_id' => 111,
            'object_id' => 222,
            'updates' => ['type' => 'Workout'],
        ]);


        $athlete = new Athlete(id: 1, stravaId: 111);

        $this->athleteRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(111)
            ->willReturn($athlete);

        $this->stravaAPIClient
            ->expects($this->once())
            ->method('loadAthleteActivityById')
            ->with($athlete, 222)
            ->willReturn(null);

        $this->activityProcessor
            ->expects($this->never())
            ->method('processStravaActivityUpdate');

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => false], $data);
    }

    public function testUpdateEventWithUnknownObjectReturnsFalse(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'create',
            'object_type' => 'fuuu',
            'owner_id' => 111,
            'object_id' => 222,
        ]);


        $this->athleteRepository
            ->expects($this->never())
            ->method('getByStravaId');

        $this->stravaAPIClient
            ->expects($this->never())
            ->method('loadAthleteActivityById');

        $this->activityProcessor
            ->expects($this->never())
            ->method('processStravaActivityUpdate');

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => false], $data);
    }

    public function testUpdateEventWithUnknownActivityReturnsFalse(): void
    {
        $request = $this->createRequest(
            'POST',
            '/stravaWebhook'
        );

        $request = $request->withParsedBody([
            'subscription_id' => 123,
            'aspect_type' => 'update',
            'object_type' => 'activity',
            'owner_id' => 111,
            'object_id' => 222,
            'updates' => ['type' => 'Workout'],
        ]);

        $athlete = new Athlete(id: 1, stravaId: 111);

        $this->athleteRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(111)
            ->willReturn($athlete);

        $stravaActivity = new StravaActivity(id: 222);

        $this->stravaAPIClient
            ->expects($this->once())
            ->method('loadAthleteActivityById')
            ->with($athlete, 222)
            ->willReturn($stravaActivity);

        $this->activityProcessor
            ->expects($this->once())
            ->method('processStravaActivityUpdate')
            ->with($athlete, $stravaActivity)
            ->willThrowException(new ActivityNotFoundException());

        $response = $this->handleRequest($request);
        $this->assertEquals(200, $response->getStatusCode());

        $payload = (string)$response->getBody();
        $data = json_decode($payload, true);
        $this->assertEquals(['result' => false], $data);
    }
}
