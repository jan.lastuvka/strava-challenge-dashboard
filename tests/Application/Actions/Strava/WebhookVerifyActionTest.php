<?php

declare(strict_types=1);

namespace Tests\Application\Actions\Strava;

use Slim\Exception\HttpUnauthorizedException;
use Tests\TestCase;

final class WebhookVerifyActionTest extends TestCase
{
    protected function setUp(): void
    {
        parent::setUp();

        $_ENV['STRAVA_WEBHOOK_SECRET'] = 'secret';
    }

    public function testItThrowsExceptionWhenSecretIsInvalid(): void
    {
        $request = $this->createRequest(
            'GET',
            '/stravaWebhook?hub_challenge=aaa&hub_verify_token=xxx'
        );

        $this->expectExceptionMessage('Unauthorized');
        $this->expectException(HttpUnauthorizedException::class);
        $this->handleRequest($request);
    }

    public function testItReturnsChallenge(): void
    {
        $request = $this->createRequest(
            'GET',
            '/stravaWebhook?hub_challenge=aaa&hub_verify_token=secret'
        );

        $response = $this->handleRequest($request);

        $payload = (string) $response->getBody();
        $serializedPayload = json_encode(['hub.challenge' => 'aaa']);

        $this->assertEquals($serializedPayload, $payload);
    }
}
