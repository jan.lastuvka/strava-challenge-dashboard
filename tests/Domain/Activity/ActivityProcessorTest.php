<?php

declare(strict_types=1);

namespace Tests\Domain\Activity;

use App\Domain\Activity\Activity;
use App\Domain\Activity\ActivityNotFoundException;
use App\Domain\Activity\ActivityProcessor;
use App\Domain\Activity\ActivityRepositoryInterface;
use App\Domain\Athlete\Athlete;
use App\Domain\Categories\Category;
use App\Domain\SportType\Types;
use App\Domain\Statistics\DayStatistic;
use App\Domain\Statistics\DayStatisticsRepositoryInterface;
use App\Domain\Strava\StravaActivity;
use Tests\TestCase;

final class ActivityProcessorTest extends TestCase
{
    private function sportTypes(): array
    {
        return [
            [Types::Run],
            [Types::Ride],
            [Types::Workout],
        ];
    }

    /**
     * @dataProvider sportTypes
     */
    public function testItProcessNewActivityAndItCreatesDayStatistics(Types $type): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-14');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);
        $startDate = new \DateTimeImmutable('2023-05-15 00:00:00');
        $stravaActivity = new StravaActivity(
            100,
            $startDate,
            'Test',
            2487.5,
            ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            144,
            287,
            $type->value
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willThrowException(new ActivityNotFoundException());

        $activitiesRepository
            ->expects($this->once())
            ->method('create')
            ->willReturnCallback(
                function (Activity $activity) use ($startDate, $type): Activity {
                    $this->assertEquals(null, $activity->id);
                    $this->assertEquals(1, $activity->athlete->id);
                    $this->assertEquals(2487.5, $activity->distance);
                    $this->assertEquals(ActivityProcessor::MIN_ACTIVITY_LENGTH * 60, $activity->time);
                    $this->assertEquals(144, $activity->elevation);
                    $this->assertEquals(287, $activity->calories);
                    $this->assertEquals($type, $activity->sportType);
                    $this->assertEquals('Test', $activity->name);
                    $this->assertEquals($startDate, $activity->startDateTime);
                    $this->assertEquals(
                        (new \DateTimeImmutable())->format('Y-m-d H:i'),
                        $activity->dateTime->format('Y-m-d H:i')
                    );

                    $activity->id = 1;

                    return $activity;
                }
            );

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->once())
            ->method('findOne')
            ->with($athlete, $startDate, $type->toCategory())
            ->willReturn(null);

        $statisticsRepository
            ->expects($this->once())
            ->method('create')
            ->willReturnCallback(
                function (DayStatistic $statistic) use ($startDate, $type): DayStatistic {
                    $this->assertEquals(null, $statistic->id);
                    $this->assertEquals(1, $statistic->athlete->id);
                    $this->assertEquals($startDate, $statistic->date);
                    $this->assertEquals((2487.5 / 1000), $statistic->distance);
                    $this->assertEquals(ActivityProcessor::MIN_ACTIVITY_LENGTH, $statistic->time);
                    $this->assertEquals(144, $statistic->elevation);
                    $this->assertEquals(287, $statistic->calories);
                    $this->assertEquals($type->toCategory(), $statistic->category);
                    $this->assertEquals(1, $statistic->activitiesCount);

                    return $statistic;
                }
            );

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $activity = $processor->processNewStravaActivity(
            $athlete,
            $stravaActivity
        );

        $this->assertEquals(1, $activity->id);
    }

    public function testProcessNewActivityDoesNotUpdateDayStatisticsWhenActivityTimeIsTooShort(): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-14');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);
        $startDate = new \DateTimeImmutable('2023-05-15 00:00:00');
        $stravaActivity = new StravaActivity(
            100,
            $startDate,
            'Test',
            2487.5,
            (ActivityProcessor::MIN_ACTIVITY_LENGTH - 1) * 60,
            144,
            287,
            'Run'
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willThrowException(new ActivityNotFoundException());

        $activitiesRepository
            ->expects($this->once())
            ->method('create')
            ->willReturnCallback(
                function (Activity $activity) use ($startDate): Activity {
                    $this->assertEquals((ActivityProcessor::MIN_ACTIVITY_LENGTH - 1) * 60, $activity->time);

                    $activity->id = 1;

                    return $activity;
                }
            );

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->never())
            ->method('findOne');

        $statisticsRepository
            ->expects($this->never())
            ->method('create');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $activity = $processor->processNewStravaActivity(
            $athlete,
            $stravaActivity
        );

        $this->assertEquals(1, $activity->id);
    }

    public function testItDoesNotProcessExistingActivity(): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-14');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);
        $stravaActivity = new StravaActivity(
            999
        );

        $activity = new Activity(2);

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(999)
            ->willReturn($activity);

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository->expects($this->never())->method('create');
        $statisticsRepository->expects($this->never())->method('update');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $activity = $processor->processNewStravaActivity(
            $athlete,
            $stravaActivity
        );

        $this->assertEquals(2, $activity->id);
    }

    public function testItProcessNewActivityWithUnknownType(): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-14');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);
        $startDate = new \DateTimeImmutable('2023-05-15 00:00:00');
        $stravaActivity = new StravaActivity(
            100,
            $startDate,
            'Test',
            2487.5,
            ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            144,
            287,
            'xxx'
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willThrowException(new ActivityNotFoundException());

        $activitiesRepository
            ->expects($this->once())
            ->method('create')
            ->willReturnCallback(
                function (Activity $activity) use ($startDate): Activity {
                    $this->assertEquals(Types::Unknown, $activity->sportType);

                    $activity->id = 1;

                    return $activity;
                }
            );

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->never())
            ->method('findOne');

        $statisticsRepository->expects($this->never())->method('create');
        $statisticsRepository->expects($this->never())->method('update');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $activity = $processor->processNewStravaActivity(
            $athlete,
            $stravaActivity
        );

        $this->assertEquals(1, $activity->id);
    }

    public function testItProcessNewActivityAndItUpdatesStatistics(): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-14');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);
        $startDate = new \DateTimeImmutable('2023-05-15 00:00:00');
        $stravaActivity = new StravaActivity(
            100,
            $startDate,
            'Test',
            2487.5,
            ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            144,
            287,
            'Ride'
        );

        $dayStatistic = new DayStatistic(
            41,
            $athlete,
            Category::BIKE,
            new \DateTimeImmutable('2023-05-15'),
            9.2,
            117.9,
            1134,
            75.5,
            1
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willThrowException(new ActivityNotFoundException());

        $activitiesRepository
            ->expects($this->once())
            ->method('create')
            ->willReturnCallback(
                function (Activity $activity) use ($startDate): Activity {
                    $this->assertEquals(Types::Ride, $activity->sportType);

                    $activity->id = 9;

                    return $activity;
                }
            );

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->once())
            ->method('findOne')
            ->with($athlete, $startDate, Category::BIKE)
            ->willReturn($dayStatistic);

        $statisticsRepository
            ->expects($this->once())
            ->method('update')
            ->willReturnCallback(
                function (DayStatistic $statistic) use ($startDate): DayStatistic {
                    $this->assertEquals(41, $statistic->id);
                    $this->assertEquals(1, $statistic->athlete->id);
                    $this->assertEquals(new \DateTimeImmutable('2023-05-15'), $statistic->date);
                    $this->assertEquals(9.2 + (2487.5 / 1000), $statistic->distance);
                    $this->assertEquals(117.9 + (ActivityProcessor::MIN_ACTIVITY_LENGTH), $statistic->time);
                    $this->assertEquals(144 + 75.5, $statistic->elevation);
                    $this->assertEquals(287 + 1134, $statistic->calories);
                    $this->assertEquals(Category::BIKE, $statistic->category);
                    $this->assertEquals(2, $statistic->activitiesCount);

                    return $statistic;
                }
            );

        $statisticsRepository->expects($this->never())->method('create');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $activity = $processor->processNewStravaActivity(
            $athlete,
            $stravaActivity
        );

        $this->assertEquals(9, $activity->id);
    }

    public function testItDeleteActivityAndUpdateStatistics(): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-14');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);
        $startDate = new \DateTimeImmutable('2023-05-15 00:00:00');

        $dayStatistic = new DayStatistic(
            41,
            $athlete,
            Category::RUN,
            new \DateTimeImmutable('2023-05-15'),
            10,
            520,
            1000,
            75
        );

        $activity = new Activity(
            2,
            stravaId: 100,
            startDateTime: $startDate,
            distance: 5000,
            time: ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            elevation: 50,
            calories: 500
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willReturn($activity);
        $activitiesRepository
            ->expects($this->once())
            ->method('deleteById')
            ->with(2)
            ->willReturn(true);

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->once())
            ->method('findOne')
            ->with($athlete, $startDate, Category::RUN)
            ->willReturn($dayStatistic);

        $statisticsRepository
            ->expects($this->once())
            ->method('update')
            ->willReturnCallback(
                function (DayStatistic $statistic) use ($startDate): DayStatistic {
                    $this->assertEquals(41, $statistic->id);
                    $this->assertEquals(1, $statistic->athlete->id);
                    $this->assertEquals(new \DateTimeImmutable('2023-05-15'), $statistic->date);
                    $this->assertEquals(10 - 5, $statistic->distance);
                    $this->assertEquals(520 - (ActivityProcessor::MIN_ACTIVITY_LENGTH), $statistic->time);
                    $this->assertEquals(75 - 50, $statistic->elevation);
                    $this->assertEquals(1000 - 500, $statistic->calories);
                    $this->assertEquals(Category::RUN, $statistic->category);

                    return $statistic;
                }
            );

        $statisticsRepository->expects($this->never())->method('create');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $result = $processor->processStravaActivityDelete(
            $athlete,
            100
        );

        $this->assertTrue($result);
    }

    public function testDeleteActivityDoesNotUpdateDayStatisticsWhenActivityTimeIsTooShort(): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-14');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);
        $startDate = new \DateTimeImmutable('2023-05-15 00:00:00');

        $dayStatistic = new DayStatistic(
            41,
            $athlete,
            Category::RUN,
            new \DateTimeImmutable('2023-05-15'),
            10,
            520,
            1000,
            75
        );

        $activity = new Activity(
            2,
            stravaId: 100,
            startDateTime: $startDate,
            distance: 5000,
            time: (ActivityProcessor::MIN_ACTIVITY_LENGTH - 1) * 60,
            elevation: 50,
            calories: 500
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willReturn($activity);
        $activitiesRepository
            ->expects($this->once())
            ->method('deleteById')
            ->with(2)
            ->willReturn(true);

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->never())
            ->method('findOne');

        $statisticsRepository
            ->expects($this->never())
            ->method('update');

        $statisticsRepository->expects($this->never())->method('create');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $result = $processor->processStravaActivityDelete(
            $athlete,
            100
        );

        $this->assertTrue($result);
    }

    public function testItDeleteActivityWithUnknownTypeWithoutStatisticsUpdate(): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-14');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);
        $startDate = new \DateTimeImmutable('2023-05-15 00:00:00');

        $activity = new Activity(
            2,
            stravaId: 100,
            startDateTime: $startDate,
            distance: 5000,
            time: ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            elevation: 50,
            calories: 500,
            sportType: Types::Unknown,
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willReturn($activity);
        $activitiesRepository
            ->expects($this->once())
            ->method('deleteById')
            ->with(2)
            ->willReturn(true);

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository->expects($this->never())->method('create');
        $statisticsRepository->expects($this->never())->method('findOne');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $result = $processor->processStravaActivityDelete(
            $athlete,
            100
        );

        $this->assertTrue($result);
    }

    public function testDeleteActivityReturnsFalseWhenActivityDoesNotExist(): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-14');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willThrowException(new ActivityNotFoundException());

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository->expects($this->never())->method('findByCategory');
        $statisticsRepository->expects($this->never())->method('findOne');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $result = $processor->processStravaActivityDelete(
            $athlete,
            100
        );

        $this->assertFalse($result);
    }

    public function testDeleteActivityReturnsTrueWhenDayStatisticsDoesNotExist(): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-14');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);
        $startDate = new \DateTimeImmutable('2023-05-15 00:00:00');

        $activity = new Activity(
            2,
            stravaId: 100,
            startDateTime: $startDate,
            distance: 5000,
            time: ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            elevation: 50,
            calories: 500
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willReturn($activity);
        $activitiesRepository
            ->expects($this->once())
            ->method('deleteById')
            ->with(2)
            ->willReturn(true);

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->once())
            ->method('findOne')
            ->with($athlete, $startDate, Category::RUN)
            ->willReturn(null);

        $statisticsRepository
            ->expects($this->never())
            ->method('update');

        $statisticsRepository->expects($this->never())->method('create');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $result = $processor->processStravaActivityDelete(
            $athlete,
            100
        );

        $this->assertTrue($result);
    }

    public function testItProcessNewActivityBeforeAthleteJoinDateWithoutDayStatisticsUpdate(): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-20');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);
        $startDate = new \DateTimeImmutable('2023-05-15 00:00:00');
        $stravaActivity = new StravaActivity(
            100,
            $startDate,
            'Test',
            2487.5,
            ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            144,
            287,
            'Run'
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willThrowException(new ActivityNotFoundException());

        $activitiesRepository
            ->expects($this->once())
            ->method('create')
            ->willReturnCallback(
                function (Activity $activity): Activity {
                    $this->assertEquals('Test', $activity->name);
                    $activity->id = 1;

                    return $activity;
                }
            );

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->never())
            ->method('findOne');

        $statisticsRepository
            ->expects($this->never())
            ->method('create');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $activity = $processor->processNewStravaActivity(
            $athlete,
            $stravaActivity
        );

        $this->assertEquals(1, $activity->id);
    }

    public function testItProcessNewActivityWhenAthleteJoinDateIsNullWithoutDayStatisticsUpdate(): void
    {
        $athlete = new Athlete(id: 1, joinDate: null);
        $startDate = new \DateTimeImmutable('2023-05-15 00:00:00');
        $stravaActivity = new StravaActivity(
            100,
            $startDate,
            'Test',
            2487.5,
            ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            144,
            287,
            'Run'
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willThrowException(new ActivityNotFoundException());

        $activitiesRepository
            ->expects($this->once())
            ->method('create')
            ->willReturnCallback(
                function (Activity $activity): Activity {
                    $this->assertEquals('Test', $activity->name);
                    $activity->id = 1;

                    return $activity;
                }
            );

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->never())
            ->method('findOne');

        $statisticsRepository
            ->expects($this->never())
            ->method('create');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $activity = $processor->processNewStravaActivity(
            $athlete,
            $stravaActivity
        );

        $this->assertEquals(1, $activity->id);
    }

    public function testItProcessActivityUpdateAndUpdatesDayStatistics(): void
    {
        $joinDate = new \DateTimeImmutable('2023-05-14');
        $athlete = new Athlete(id: 1, joinDate: $joinDate);

        $originalStartDateTime = new \DateTimeImmutable('2023-05-15 09:10:00');
        $startDate = new \DateTimeImmutable('2023-05-15 09:15:00');
        $stravaActivity = new StravaActivity(
            100,
            $startDate,
            'Morning run',
            2487.5,
            ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            144,
            287,
            Types::Run->value
        );

        $activity = new Activity(
            id: 1,
            stravaId: 100,
            athlete: $athlete,
            dateTime: new \DateTimeImmutable('2023-05-15 10:00:00'),
            startDateTime: $originalStartDateTime,
            name: 'Morning bike',
            distance: 100,
            time: ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            elevation: 100,
            calories: 100,
            sportType: Types::Ride
        );

        $bikeStatistic = new DayStatistic(
            1,
            $athlete,
            Category::BIKE,
            new \DateTimeImmutable('2023-05-15'),
            100 / 1000,
            ActivityProcessor::MIN_ACTIVITY_LENGTH,
            100,
            100,
            1
        );

        $runStatistic = new DayStatistic(
            2,
            $athlete,
            Category::RUN,
            new \DateTimeImmutable('2023-05-15'),
            0,
            0,
            0,
            0,
            0
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willReturn($activity);

        $activitiesRepository
            ->expects($this->once())
            ->method('update')
            ->willReturnCallback(
                function (Activity $activity) use ($startDate): Activity {
                    $this->assertEquals(1, $activity->id);
                    $this->assertEquals(100, $activity->stravaId);
                    $this->assertEquals(1, $activity->athlete->id);
                    $this->assertEquals(2487.5, $activity->distance);
                    $this->assertEquals(ActivityProcessor::MIN_ACTIVITY_LENGTH * 60, $activity->time);
                    $this->assertEquals(144, $activity->elevation);
                    $this->assertEquals(287, $activity->calories);
                    $this->assertEquals(Types::Run, $activity->sportType);
                    $this->assertEquals('Morning run', $activity->name);
                    $this->assertEquals($startDate, $activity->startDateTime);
                    $this->assertEquals(
                        '2023-05-15 10:00:00',
                        $activity->dateTime->format('Y-m-d H:i:s')
                    );

                    return $activity;
                }
            );

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->exactly(2))
            ->method('findOne')
            ->willReturnMap([
                [$athlete, $originalStartDateTime, Category::BIKE, $bikeStatistic],
                [$athlete, $startDate, Category::RUN, $runStatistic],
            ]);

        $toUpdate = [1 => true, 2 => true];
        $statisticsRepository
            ->expects($this->exactly(2))
            ->method('update')
            ->willReturnCallback(
                function (DayStatistic $statistic) use (&$toUpdate): DayStatistic {
                    if ($statistic->id === 1) {
                        unset($toUpdate[1]);
                        $this->assertEquals(0, $statistic->time);
                        $this->assertEquals(0, $statistic->calories);
                        $this->assertEquals(0, $statistic->distance);
                        $this->assertEquals(0, $statistic->elevation);
                        $this->assertEquals(0, $statistic->activitiesCount);

                        return $statistic;
                    }

                    if ($statistic->id === 2) {
                        unset($toUpdate[2]);
                        $this->assertEquals(ActivityProcessor::MIN_ACTIVITY_LENGTH, $statistic->time);
                        $this->assertEquals(287, $statistic->calories);
                        $this->assertEquals(2487.5 / 1000, $statistic->distance);
                        $this->assertEquals(144, $statistic->elevation);
                        $this->assertEquals(1, $statistic->activitiesCount);

                        return $statistic;
                    }

                    $this->fail('Unknown statistics to update');
                }
            );

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $processor->processStravaActivityUpdate(
            $athlete,
            $stravaActivity
        );

        $this->assertEmpty($toUpdate);
    }

    public function testUpdateActivityThrowsExceptionWhenActivityDoesNotExists(): void
    {
        $athlete = new Athlete(id: 1);

        $stravaActivity = new StravaActivity(
            100
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willThrowException(new ActivityNotFoundException());

        $activitiesRepository
            ->expects($this->never())
            ->method('update');

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->never())
            ->method('findOne');

        $statisticsRepository
            ->expects($this->never())
            ->method('update');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $this->expectException(ActivityNotFoundException::class);
        $processor->processStravaActivityUpdate(
            $athlete,
            $stravaActivity
        );
    }

    public function testItProcessActivityUpdateWithoutStatisticsUpdate(): void
    {
        $athlete = new Athlete(id: 1, joinDate: new \DateTimeImmutable('2023-05-14'));

        $stravaActivity = new StravaActivity(
            100,
            new \DateTimeImmutable('2023-05-15 09:15:00'),
            'Morning run',
            2487.5,
            (ActivityProcessor::MIN_ACTIVITY_LENGTH - 1) * 60,
            144,
            287,
            Types::Run->value
        );

        $activity = new Activity(
            id: 1,
            stravaId: 100,
            athlete: $athlete,
            dateTime: new \DateTimeImmutable('2023-05-15 10:00:00'),
            startDateTime: new \DateTimeImmutable('2023-05-15 09:10:00'),
            name: 'Morning bike',
            distance: 100,
            time: (ActivityProcessor::MIN_ACTIVITY_LENGTH - 2) * 60,
            elevation: 100,
            calories: 100,
            sportType: Types::Ride
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willReturn($activity);

        $activitiesRepository
            ->expects($this->once())
            ->method('update')
            ->willReturnCallback(
                function (Activity $activity): Activity {
                    $this->assertEquals(1, $activity->id);
                    $this->assertEquals((ActivityProcessor::MIN_ACTIVITY_LENGTH - 1) * 60, $activity->time);

                    return $activity;
                }
            );

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->never())
            ->method('findOne');

        $statisticsRepository
            ->expects($this->never())
            ->method('update');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $processor->processStravaActivityUpdate(
            $athlete,
            $stravaActivity
        );
    }

    public function testItProcessActivityUpdateWithOnlyRemoveFromStatistics(): void
    {
        $athlete = new Athlete(id: 1, joinDate: new \DateTimeImmutable('2023-05-14'));

        $originalStartDateTime = new \DateTimeImmutable('2023-05-15 09:10:00');
        $startDate = new \DateTimeImmutable('2023-05-15 09:15:00');
        $stravaActivity = new StravaActivity(
            100,
            $startDate,
            'Morning run',
            2487.5,
            (ActivityProcessor::MIN_ACTIVITY_LENGTH - 1) * 60,
            144,
            287,
            Types::Run->value
        );

        $activity = new Activity(
            id: 1,
            stravaId: 100,
            athlete: $athlete,
            dateTime: new \DateTimeImmutable('2023-05-15 10:00:00'),
            startDateTime: $originalStartDateTime,
            name: 'Morning bike',
            distance: 100,
            time: ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            elevation: 100,
            calories: 100,
            sportType: Types::Ride
        );

        $bikeStatistic = new DayStatistic(
            1,
            $athlete,
            Category::BIKE,
            new \DateTimeImmutable('2023-05-15'),
            100 / 1000,
            ActivityProcessor::MIN_ACTIVITY_LENGTH,
            100,
            100,
            1
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willReturn($activity);

        $activitiesRepository
            ->expects($this->once())
            ->method('update')
            ->willReturnCallback(
                function (Activity $activity) use ($startDate): Activity {
                    $this->assertEquals(1, $activity->id);
                    $this->assertEquals((ActivityProcessor::MIN_ACTIVITY_LENGTH - 1) * 60, $activity->time);

                    return $activity;
                }
            );

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->exactly(1))
            ->method('findOne')
            ->willReturnMap([
                [$athlete, $originalStartDateTime, Category::BIKE, $bikeStatistic],
            ]);

        $toUpdate = [1 => true];
        $statisticsRepository
            ->expects($this->exactly(1))
            ->method('update')
            ->willReturnCallback(
                function (DayStatistic $statistic) use (&$toUpdate): DayStatistic {
                    if ($statistic->id === 1) {
                        unset($toUpdate[1]);
                        $this->assertEquals(0, $statistic->time);
                        $this->assertEquals(0, $statistic->calories);
                        $this->assertEquals(0, $statistic->distance);
                        $this->assertEquals(0, $statistic->elevation);
                        $this->assertEquals(0, $statistic->activitiesCount);

                        return $statistic;
                    }

                    $this->fail('Unknown statistics to update');
                }
            );

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $processor->processStravaActivityUpdate(
            $athlete,
            $stravaActivity
        );

        $this->assertEmpty($toUpdate);
    }

    public function testItProcessActivityUpdateWithOnlyAddToStatistics(): void
    {
        $athlete = new Athlete(id: 1, joinDate: new \DateTimeImmutable('2023-05-14'));

        $originalStartDateTime = new \DateTimeImmutable('2023-05-15 09:10:00');
        $startDate = new \DateTimeImmutable('2023-05-15 09:15:00');
        $stravaActivity = new StravaActivity(
            100,
            $startDate,
            'Morning run',
            2487.5,
            ActivityProcessor::MIN_ACTIVITY_LENGTH * 60,
            144,
            287,
            Types::Run->value
        );

        $activity = new Activity(
            id: 1,
            stravaId: 100,
            athlete: $athlete,
            dateTime: new \DateTimeImmutable('2023-05-15 10:00:00'),
            startDateTime: $originalStartDateTime,
            name: 'Morning bike',
            distance: 100,
            time: (ActivityProcessor::MIN_ACTIVITY_LENGTH - 1) * 60,
            elevation: 100,
            calories: 100,
            sportType: Types::Ride
        );

        $runStatistic = new DayStatistic(
            2,
            $athlete,
            Category::RUN,
            new \DateTimeImmutable('2023-05-15'),
            0,
            0,
            0,
            0,
            0
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->once())
            ->method('getByStravaId')
            ->with(100)
            ->willReturn($activity);

        $activitiesRepository
            ->expects($this->once())
            ->method('update')
            ->willReturnCallback(
                function (Activity $activity) use ($startDate): Activity {
                    $this->assertEquals(1, $activity->id);
                    $this->assertEquals(ActivityProcessor::MIN_ACTIVITY_LENGTH * 60, $activity->time);

                    return $activity;
                }
            );

        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository
            ->expects($this->exactly(1))
            ->method('findOne')
            ->willReturnMap([
                [$athlete, $startDate, Category::RUN, $runStatistic],
            ]);

        $toUpdate = [2 => true];
        $statisticsRepository
            ->expects($this->exactly(1))
            ->method('update')
            ->willReturnCallback(
                function (DayStatistic $statistic) use (&$toUpdate): DayStatistic {
                    if ($statistic->id === 2) {
                        unset($toUpdate[2]);
                        $this->assertEquals(ActivityProcessor::MIN_ACTIVITY_LENGTH, $statistic->time);
                        $this->assertEquals(287, $statistic->calories);
                        $this->assertEquals(2487.5 / 1000, $statistic->distance);
                        $this->assertEquals(144, $statistic->elevation);
                        $this->assertEquals(1, $statistic->activitiesCount);

                        return $statistic;
                    }

                    $this->fail('Unknown statistics to update');
                }
            );

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $processor->processStravaActivityUpdate(
            $athlete,
            $stravaActivity
        );

        $this->assertEmpty($toUpdate);
    }

    public function testProcessAthleteActivityWillUpdateExistingActivity(): void
    {
        $athlete = new Athlete(id: 1, joinDate: null);
        $stravaActivity = new StravaActivity(
            100,
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->exactly(2))
            ->method('getByStravaId')
            ->with(100)
            ->willReturn($activity = new Activity(1));
        $activitiesRepository
            ->expects($this->once())
            ->method('update')
            ->with($activity)
            ->willReturn($activity);
        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository->expects($this->never())->method('findOne');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $processor->processAthleteActivity(
            $athlete,
            $stravaActivity
        );
    }

    public function testProcessAthleteActivityWillCreateNewActivity(): void
    {
        $athlete = new Athlete(id: 1, joinDate: null);
        $stravaActivity = new StravaActivity(
            100,
        );

        $activitiesRepository = $this->createMock(ActivityRepositoryInterface::class);
        $activitiesRepository
            ->expects($this->exactly(2))
            ->method('getByStravaId')
            ->with(100)
            ->willThrowException(new ActivityNotFoundException());
        $activitiesRepository
            ->expects($this->once())
            ->method('create')
            ->willReturnCallback(
                function (Activity $activity) {
                    $this->assertNull($activity->id);
                    $this->assertEquals(100, $activity->stravaId);
                    $this->assertEquals(1, $activity->athlete->id);
                    $activity->id = 333;

                    return $activity;
                }
            );
        $statisticsRepository = $this->createMock(DayStatisticsRepositoryInterface::class);
        $statisticsRepository->expects($this->never())->method('findOne');

        $processor = new ActivityProcessor($activitiesRepository, $statisticsRepository);

        $processor->processAthleteActivity(
            $athlete,
            $stravaActivity
        );
    }
}
