<?php

declare(strict_types=1);

namespace Tests\Domain\Athlete;

use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\Team;
use Tests\TestCase;

final class AthleteTest extends TestCase
{
    public function testItSerializeData(): void
    {
        $athlete = new Athlete(
            1,
            'accessToken',
            'refreshToken',
            200,
            'test',
            'John',
            'Doe',
            'M',
            'https://example.com/avatar.jpg',
            'DEV',
            new \DateTimeImmutable('2023-05-15 00:00:00'),
            Team::WEBNODE
        );

        $expects = [
            'id' => 1,
            'name' => 'John Doe',
            'avatar' => 'https://example.com/avatar.jpg',
            'department' => 'DEV',
            'team' => 'webnode',
        ];

        $this->assertEquals(
            json_encode($expects),
            json_encode($athlete),
        );
    }
}
