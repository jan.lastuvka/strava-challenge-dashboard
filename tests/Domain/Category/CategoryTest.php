<?php

declare(strict_types=1);

namespace Tests\Domain\Category;

use App\Domain\Categories\Category;
use Tests\TestCase;

final class CategoryTest extends TestCase
{
    public function testItGetsCategoryId(): void
    {
        $category = Category::RUN;
        $this->assertEquals(1, $category->toId());

        $category = Category::BIKE;
        $this->assertEquals(2, $category->toId());

        $category = Category::NOT_CLASSIFIED;
        $this->assertEquals(0, $category->toId());
    }

    public function testItGetsFromId(): void
    {
        $category = Category::fromId(0);
        $this->assertEquals(Category::NOT_CLASSIFIED, $category);

        $category = Category::fromId(1);
        $this->assertEquals(Category::RUN, $category);

        $category = Category::fromId(2);
        $this->assertEquals(Category::BIKE, $category);
    }
}
