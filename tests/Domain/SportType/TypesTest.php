<?php

declare(strict_types=1);

namespace Tests\Domain\SportType;

use App\Domain\Categories\Category;
use App\Domain\SportType\Types;
use Tests\TestCase;

final class TypesTest extends TestCase
{
    public function testItGetsCategory(): void
    {
        $type = Types::Run;
        $this->assertEquals(Category::RUN, $type->toCategory());

        $type = Types::Ride;
        $this->assertEquals(Category::BIKE, $type->toCategory());

        $type = Types::Unknown;
        $this->assertEquals(Category::NOT_CLASSIFIED, $type->toCategory());
    }
}
