<?php

declare(strict_types=1);

namespace Tests\Domain\Statistics;

use App\Domain\Athlete\Athlete;
use App\Domain\Categories\Category;
use App\Domain\Statistics\DayStatistic;
use Tests\TestCase;

final class DayStatisticTest extends TestCase
{
    public function testItSerializeData(): void
    {
        $athlete = new DayStatistic(
            2,
            new Athlete(id: 999),
            Category::RUN,
            new \DateTimeImmutable('2023-05-15 00:00:00'),
            154.5,
            244.5,
            40.4,
            200.1,
            1
        );

        $expects = [
            'id' => 2,
            'athleteId' => 999,
            'category' => 'run',
            'date' => '2023-05-15',
            'distance' => 154.5,
            'time' => 244.5,
            'calories' => 40.4,
            'elevation' => 200.1,
            'activitiesCount' => 1,
        ];

        $this->assertEquals(
            json_encode($expects),
            json_encode($athlete),
        );
    }
}
