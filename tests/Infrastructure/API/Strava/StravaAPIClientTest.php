<?php

declare(strict_types=1);

namespace Tests\Infrastructure\API\Strava;

use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteRepositoryInterface;
use App\Domain\Strava\StravaActivity;
use App\Infrastructure\API\Strava\StravaAPIClient;
use App\Infrastructure\OAuth2\StravaOAuth2Provider;
use GuzzleHttp\Client;
use GuzzleHttp\Psr7\Response;
use League\OAuth2\Client\Token\AccessTokenInterface;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerInterface;
use Tests\TestCase;

final class StravaAPIClientTest extends TestCase
{
    public function testItLoadsActivities(): void
    {
        $client = $this->createMock(Client::class);
        $client->method('sendRequest')->willReturnCallback(
            function (RequestInterface $request): ResponseInterface {
                $this->assertEquals('GET', $request->getMethod());
                $this->assertEquals(['Bearer x'], $request->getHeader('Authorization'));
                $this->assertEquals(
                    'https://www.strava.com/api/v3/athlete/activities?per_page=10',
                    $request->getUri()
                );

                return new Response(200, body: file_get_contents(__DIR__ . '/exampleActivities.json'));
            }
        );

        $athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $oauth2Provider = $this->createMock(StravaOAuth2Provider::class);
        $logger = $this->createMock(LoggerInterface::class);

        $client = new StravaAPIClient($client, $athleteRepository, $oauth2Provider, $logger);

        $athlete = new Athlete(accessToken: 'x', refreshToken: 'y');

        $activities = $client->loadLastAthleteActivities($athlete, 10);
        $this->assertCount(1, $activities);
        /** @var StravaActivity $activity */
        $activity = reset($activities);

        $this->assertEquals(123, $activity->id);
        $this->assertEquals('2023-06-16 13:07:41', $activity->startDate->format('Y-m-d H:i:s'));
        $this->assertEquals('Afternoon Walk', $activity->name);
        $this->assertEquals(1813.6, $activity->distance);
        $this->assertEquals(1949, $activity->movingTime);
        $this->assertEquals(9, $activity->totalElevationGain);
        $this->assertEquals(0, $activity->calories);
        $this->assertEquals('Walk', $activity->sportType);
    }

    public function testLoadActivitiesReturnsEmptyResultOnFail(): void
    {
        $client = $this->createMock(Client::class);
        $client->method('sendRequest')->willReturnCallback(
            function (RequestInterface $request): ResponseInterface {
                $accessToken = $request->getHeader('Authorization');

                if ($accessToken === ['Bearer xyz']) {
                    return new Response(401);
                }

                $this->fail('Unknown access token');
            }
        );

        $athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $oauth2Provider = $this->createMock(StravaOAuth2Provider::class);
        $oauth2Provider
            ->method('getAccessToken')
            ->willThrowException(new \InvalidArgumentException());
        $logger = $this->createMock(LoggerInterface::class);

        $client = new StravaAPIClient($client, $athleteRepository, $oauth2Provider, $logger);

        $athlete = new Athlete(id: 1, accessToken: 'xyz', refreshToken: 'y');

        $activities = $client->loadLastAthleteActivities($athlete, 100);

        $this->assertEquals([], $activities);
    }

    public function testItLoadActivityById(): void
    {
        $client = $this->createMock(Client::class);
        $client->method('sendRequest')->willReturnCallback(
            function (RequestInterface $request): ResponseInterface {
                $this->assertEquals('GET', $request->getMethod());
                $this->assertEquals(['Bearer xyz'], $request->getHeader('Authorization'));
                $this->assertEquals(
                    'https://www.strava.com/api/v3/activities/123',
                    $request->getUri()
                );

                return new Response(200, body: file_get_contents(__DIR__ . '/exampleActivity.json'));
            }
        );

        $athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $oauth2Provider = $this->createMock(StravaOAuth2Provider::class);
        $logger = $this->createMock(LoggerInterface::class);

        $client = new StravaAPIClient($client, $athleteRepository, $oauth2Provider, $logger);

        $athlete = new Athlete(accessToken: 'xyz', refreshToken: 'y');

        $activity = $client->loadAthleteActivityById($athlete, 123);

        $this->assertEquals(123, $activity->id);
        $this->assertEquals('2023-06-16 13:07:41', $activity->startDate->format('Y-m-d H:i:s'));
        $this->assertEquals('Afternoon Walk', $activity->name);
        $this->assertEquals(1813.6, $activity->distance);
        $this->assertEquals(1949, $activity->movingTime);
        $this->assertEquals(9, $activity->totalElevationGain);
        $this->assertEquals(164, $activity->calories);
        $this->assertEquals('Walk', $activity->sportType);
    }

    public function testItLoadActivityWithInvalidStartDate(): void
    {
        $client = $this->createMock(Client::class);
        $client->method('sendRequest')->willReturnCallback(
            function (RequestInterface $request): ResponseInterface {
                $this->assertEquals('GET', $request->getMethod());
                $this->assertEquals(['Bearer xyz'], $request->getHeader('Authorization'));
                $this->assertEquals(
                    'https://www.strava.com/api/v3/activities/123',
                    $request->getUri()
                );

                $data = json_decode(file_get_contents(__DIR__ . '/exampleActivity.json'), true);
                $data['start_date'] = '1';

                return new Response(200, body: json_encode($data));
            }
        );

        $athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $oauth2Provider = $this->createMock(StravaOAuth2Provider::class);
        $logger = $this->createMock(LoggerInterface::class);

        $client = new StravaAPIClient($client, $athleteRepository, $oauth2Provider, $logger);

        $athlete = new Athlete(accessToken: 'xyz', refreshToken: 'y');

        $activity = $client->loadAthleteActivityById($athlete, 123);

        $this->assertEquals(123, $activity->id);
        $this->assertEquals(
            (new \DateTimeImmutable())->format('Y-m-d'),
            $activity->startDate->format('Y-m-d')
        );
        $this->assertEquals('Afternoon Walk', $activity->name);
        $this->assertEquals(1813.6, $activity->distance);
        $this->assertEquals(1949, $activity->movingTime);
        $this->assertEquals(9, $activity->totalElevationGain);
        $this->assertEquals(164, $activity->calories);
        $this->assertEquals('Walk', $activity->sportType);
    }

    public function testItRefreshesToken(): void
    {
        $client = $this->createMock(Client::class);
        $client->method('sendRequest')->willReturnCallback(
            function (RequestInterface $request): ResponseInterface {
                $accessToken = $request->getHeader('Authorization');

                if ($accessToken === ['Bearer xyz']) {
                    return new Response(401);
                }

                if ($accessToken === ['Bearer aaa']) {
                    return new Response(200, body: file_get_contents(__DIR__ . '/exampleActivity.json'));
                }

                $this->fail('Unknown access token');
            }
        );

        $token = $this->createMock(AccessTokenInterface::class);
        $token->method('getToken')->willReturn('aaa');
        $token->method('getRefreshToken')->willReturn('bbb');

        $athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $athleteRepository
            ->expects($this->once())
            ->method('update')
            ->willReturnCallback(function (Athlete $athlete): Athlete {
                $this->assertEquals('aaa', $athlete->accessToken);
                $this->assertEquals('bbb', $athlete->refreshToken);

                return $athlete;
            });
        $oauth2Provider = $this->createMock(StravaOAuth2Provider::class);
        $oauth2Provider
            ->expects($this->once())
            ->method('getAccessToken')
            ->with(
                'refresh_token',
                [
                    'refresh_token' => 'y'
                ]
            )
            ->willReturn($token);
        $logger = $this->createMock(LoggerInterface::class);

        $client = new StravaAPIClient($client, $athleteRepository, $oauth2Provider, $logger);

        $athlete = new Athlete(accessToken: 'xyz', refreshToken: 'y');

        $activity = $client->loadAthleteActivityById($athlete, 123);

        $this->assertEquals(123, $activity->id);
    }

    public function testItNotifyWhenItIsUnableToRefreshToken(): void
    {
        $client = $this->createMock(Client::class);
        $client->method('sendRequest')->willReturnCallback(
            function (RequestInterface $request): ResponseInterface {
                $accessToken = $request->getHeader('Authorization');

                if ($accessToken === ['Bearer xyz']) {
                    return new Response(401);
                }

                $this->fail('Unknown access token');
            }
        );

        $athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $athleteRepository
            ->expects($this->never())
            ->method('update');
        $oauth2Provider = $this->createMock(StravaOAuth2Provider::class);
        $oauth2Provider
            ->expects($this->once())
            ->method('getAccessToken')
            ->with(
                'refresh_token',
                [
                    'refresh_token' => 'y'
                ]
            )
            ->willThrowException(new \InvalidArgumentException());
        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('critical')->with(
            'Unable to load athlete data. Athlete id: 1 | Token refresh: failed'
        );

        $client = new StravaAPIClient($client, $athleteRepository, $oauth2Provider, $logger);

        $athlete = new Athlete(id: 1, accessToken: 'xyz', refreshToken: 'y');

        $activity = $client->loadAthleteActivityById($athlete, 123);

        $this->assertEquals(null, $activity);
    }


    public function testItNotifiesWhenLoadActivityByIdFails(): void
    {
        $client = $this->createMock(Client::class);
        $client->method('sendRequest')->willReturnCallback(
            function (RequestInterface $request): ResponseInterface {
                $this->assertEquals('GET', $request->getMethod());
                $this->assertEquals(['Bearer xyz'], $request->getHeader('Authorization'));
                $this->assertEquals(
                    'https://www.strava.com/api/v3/activities/123',
                    $request->getUri()
                );

                return new Response(404);
            }
        );

        $athleteRepository = $this->createMock(AthleteRepositoryInterface::class);
        $oauth2Provider = $this->createMock(StravaOAuth2Provider::class);
        $logger = $this->createMock(LoggerInterface::class);
        $logger->expects($this->once())->method('error')->with(
            'Unable to load activity data. Athlete id: 1 | Activity id: 123 | Status code: 404'
        );

        $client = new StravaAPIClient($client, $athleteRepository, $oauth2Provider, $logger);

        $athlete = new Athlete(id: 1, accessToken: 'xyz', refreshToken: 'y');

        $activity = $client->loadAthleteActivityById($athlete, 123);

        $this->assertEquals(null, $activity);
    }
}
