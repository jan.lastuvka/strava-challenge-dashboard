<?php

declare(strict_types=1);

namespace Tests\Infrastructure\Persistence\Activity;

use App\Domain\Activity\Activity;
use App\Domain\Activity\ActivityNotFoundException;
use App\Domain\Athlete\Athlete;
use App\Domain\SportType\Types;
use App\Infrastructure\Persistence\Activity\ActivityPdoRepository;
use Tests\TestCase;

final class ActivityPdoRepositoryTest extends TestCase
{
    public function testItCreatesActivity(): void
    {
        $athlete = new Athlete(1);
        $dateTime = new \DateTimeImmutable('2023-05-15 15:00:00');
        $startDateTime = new \DateTimeImmutable('2023-05-15 12:00:00');
        $entity = new Activity(
            null,
            123,
            $athlete,
            $dateTime,
            $startDateTime,
            'test',
            100,
            50,
            150,
            2000,
            Types::Hike
        );

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(7))
            ->method('bindParam')
            ->willReturnMap([
                ['stravaId', 123, \PDO::PARAM_STR, 0, null, true,],
                ['athleteId', 1, \PDO::PARAM_STR, 0, null, true,],
                ['name', 'test', \PDO::PARAM_STR, 0, null, true,],
                ['distance', 100.0, \PDO::PARAM_STR, 0, null, true,],
                ['time', 50.0, \PDO::PARAM_STR, 0, null, true,],
                ['elevation', 150.0, \PDO::PARAM_STR, 0, null, true,],
                ['calories', 2000.0, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->exactly(3))
            ->method('bindValue')
            ->willReturnMap([
                ['datetime', '2023-05-15 15:00:00', \PDO::PARAM_STR, true,],
                ['startDatetime', '2023-05-15 12:00:00', \PDO::PARAM_STR, true,],
                ['sportType', 'Hike', \PDO::PARAM_STR, true,],
            ]);
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);
        $pdo->method('lastInsertId')->willReturn("1");

        $repository = new ActivityPdoRepository($pdo);

        $repository->create($entity);

        $this->assertEquals(1, $entity->id);
    }

    public function testCreateThrowsExceptionWhenIdIsNotNull(): void
    {
        $athlete = new Athlete(1);
        $dateTime = new \DateTimeImmutable('2023-05-15 15:00:00');
        $startDateTime = new \DateTimeImmutable('2023-05-15 12:00:00');
        $entity = new Activity(
            1,
            123,
            $athlete,
            $dateTime,
            $startDateTime,
            'test',
            100,
            50,
            150,
            2000,
            Types::Hike
        );

        $pdo = $this->createMock(\PDO::class);

        $repository = new ActivityPdoRepository($pdo);

        $this->expectException(\RuntimeException::class);
        $repository->create($entity);
    }

    public function testUpdateThrowsExceptionWhenIdIsNull(): void
    {
        $entity = new Activity(
            id: null,
        );

        $pdo = $this->createMock(\PDO::class);

        $repository = new ActivityPdoRepository($pdo);

        $this->expectException(\RuntimeException::class);
        $repository->update($entity);
    }

    public function testItDeletesById(): void
    {
        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['id', 1, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->never())
            ->method('bindValue');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);
        $pdo->method('lastInsertId')->willReturn("1");

        $repository = new ActivityPdoRepository($pdo);

        $this->assertTrue($repository->deleteById(1));
    }

    public function testGetByStravaId(): void
    {
        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['id', 123, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->never())
            ->method('bindValue');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetch')
            ->with()
            ->willReturn([
                'sportType' => 'Hike',
                'id' => 1,
                'stravaId' => 123,
                'athleteId' => 1,
                'datetime' => '2023-05-15 15:00:00',
                'startDatetime' => '2023-05-15 12:00:00',
                'name' => 'test',
                'distance' => '10',
                'time' => '100',
                'elevation' => '200',
                'calories' => '1000',
            ]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);
        $pdo->method('lastInsertId')->willReturn("1");

        $repository = new ActivityPdoRepository($pdo);

        $entity = $repository->getByStravaId(123);
        $this->assertEquals(1, $entity->id);
        $this->assertEquals(123, $entity->stravaId);
        $this->assertEquals(1, $entity->athlete->id);
        $this->assertEquals('2023-05-15 12:00:00', $entity->startDateTime->format('Y-m-d H:i:s'));
        $this->assertEquals('2023-05-15 15:00:00', $entity->dateTime->format('Y-m-d H:i:s'));
        $this->assertEquals(Types::Hike, $entity->sportType);
        $this->assertEquals('test', $entity->name);
        $this->assertEquals(10.0, $entity->distance);
        $this->assertEquals(100.0, $entity->time);
        $this->assertEquals(200.0, $entity->elevation);
        $this->assertEquals(1000.0, $entity->calories);
    }

    public function testGetByStravaIdWithUnknownType(): void
    {
        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['id', 123, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->never())
            ->method('bindValue');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetch')
            ->with()
            ->willReturn([
                'sportType' => 'xxx',
                'id' => 1,
                'stravaId' => 123,
                'athleteId' => 1,
                'datetime' => '2023-05-15 15:00:00',
                'startDatetime' => '2023-05-15 12:00:00',
                'name' => 'test',
                'distance' => '10',
                'time' => '100',
                'elevation' => '200',
                'calories' => '1000',
            ]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);
        $pdo->method('lastInsertId')->willReturn("1");

        $repository = new ActivityPdoRepository($pdo);

        $entity = $repository->getByStravaId(123);
        $this->assertEquals(Types::Unknown, $entity->sportType);
    }

    public function testGetByStravaIdThrowsExceptionWhenDataDoesNotExist(): void
    {
        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['id', 123, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->never())
            ->method('bindValue');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetch')
            ->with()
            ->willReturn(false);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new ActivityPdoRepository($pdo);

        $this->expectException(ActivityNotFoundException::class);
        $repository->getByStravaId(123);
    }

    public function testItUpdatesActivity(): void
    {
        $athlete = new Athlete(1);
        $dateTime = new \DateTimeImmutable('2023-05-15 15:00:00');
        $startDateTime = new \DateTimeImmutable('2023-05-15 12:00:00');
        $entity = new Activity(
            10,
            123,
            $athlete,
            $dateTime,
            $startDateTime,
            'test',
            100,
            50,
            150,
            2000,
            Types::Hike
        );

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(6))
            ->method('bindParam')
            ->willReturnMap([
                ['name', 'test', \PDO::PARAM_STR, 0, null, true,],
                ['distance', 100.0, \PDO::PARAM_STR, 0, null, true,],
                ['time', 50.0, \PDO::PARAM_STR, 0, null, true,],
                ['elevation', 150.0, \PDO::PARAM_STR, 0, null, true,],
                ['calories', 2000.0, \PDO::PARAM_STR, 0, null, true,],
                ['id', 10, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->exactly(2))
            ->method('bindValue')
            ->willReturnMap([
                ['startDatetime', '2023-05-15 12:00:00', \PDO::PARAM_STR, true,],
                ['sportType', 'Hike', \PDO::PARAM_STR, true,],
            ]);
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new ActivityPdoRepository($pdo);

        $repository->update($entity);

        $this->assertEquals(10, $entity->id);
    }
}
