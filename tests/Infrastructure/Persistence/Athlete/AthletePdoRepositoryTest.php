<?php

declare(strict_types=1);

namespace Tests\Infrastructure\Persistence\Athlete;

use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\AthleteNotFoundException;
use App\Domain\Athlete\Team;
use App\Infrastructure\Persistence\Athlete\AthletePdoRepository;
use Tests\TestCase;

final class AthletePdoRepositoryTest extends TestCase
{
    public function testItCreatesAthlete(): void
    {
        $athlete = new Athlete(
            null,
            'x',
            'y',
            132,
            'test',
            'John',
            'Doe',
            'M',
            'https://example.com/avatar.jpg',
            'DEV',
            new \DateTimeImmutable('2023-05-15 10:00:00')
        );

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(9))
            ->method('bindParam')
            ->willReturnMap([
                ['accessToken', 'x', \PDO::PARAM_STR, 0, null, true,],
                ['refreshToken', 'y', \PDO::PARAM_STR, 0, null, true,],
                ['stravaId', 132, \PDO::PARAM_STR, 0, null, true,],
                ['username', 'test', \PDO::PARAM_STR, 0, null, true,],
                ['firstname', 'John', \PDO::PARAM_STR, 0, null, true,],
                ['lastname', 'Doe', \PDO::PARAM_STR, 0, null, true,],
                ['sex', 'M', \PDO::PARAM_STR, 0, null, true,],
                ['avatar', 'https://example.com/avatar.jpg', \PDO::PARAM_STR, 0, null, true,],
                ['department', 'DEV', \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->exactly(2))
            ->method('bindValue')
            ->willReturnMap([
                ['joinDate', '2023-05-15', \PDO::PARAM_STR, true,],
                ['team', 'webnode', \PDO::PARAM_STR, true,],
            ]);
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);
        $pdo->method('lastInsertId')->willReturn("1");

        $repository = new AthletePdoRepository($pdo);

        $repository->create($athlete);

        $this->assertEquals(1, $athlete->id);
    }

    public function testCreatesThrowsExceptionWhenIdIsNotNull(): void
    {
        $athlete = new Athlete(
            1,
            'x',
            'y',
            132,
            'test',
            'John',
            'Doe',
            'M',
            'https://example.com/avatar.jpg',
            'DEV',
            new \DateTimeImmutable('2023-05-15 10:00:00')
        );

        $pdo = $this->createMock(\PDO::class);

        $repository = new AthletePdoRepository($pdo);

        $this->expectException(\RuntimeException::class);
        $repository->create($athlete);
    }

    public function testItUpdatesAthlete(): void
    {
        $athlete = new Athlete(
            1,
            'x',
            'y',
            132,
            'test',
            'John',
            'Doe',
            'M',
            'https://example.com/avatar.jpg',
            'DEV',
            new \DateTimeImmutable('2023-05-15 10:00:00')
        );

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(10))
            ->method('bindParam')
            ->willReturnMap([
                ['accessToken', 'x', \PDO::PARAM_STR, 0, null, true,],
                ['refreshToken', 'y', \PDO::PARAM_STR, 0, null, true,],
                ['stravaId', 132, \PDO::PARAM_STR, 0, null, true,],
                ['username', 'test', \PDO::PARAM_STR, 0, null, true,],
                ['firstname', 'John', \PDO::PARAM_STR, 0, null, true,],
                ['lastname', 'Doe', \PDO::PARAM_STR, 0, null, true,],
                ['sex', 'M', \PDO::PARAM_STR, 0, null, true,],
                ['avatar', 'https://example.com/avatar.jpg', \PDO::PARAM_STR, 0, null, true,],
                ['department', 'DEV', \PDO::PARAM_STR, 0, null, true,],
                ['id', 1, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->exactly(2))
            ->method('bindValue')
            ->willReturnMap([
                ['joinDate', '2023-05-15', \PDO::PARAM_STR, true,],
                ['team', 'webnode', \PDO::PARAM_STR, true,],
            ]);
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new AthletePdoRepository($pdo);

        $repository->update($athlete);

        $this->assertEquals(1, $athlete->id);
    }

    public function testUpdateThrowsExceptionWhenIdIsNull(): void
    {
        $athlete = new Athlete(
            null,
            'x',
            'y',
            132,
            'test',
            'John',
            'Doe',
            'M',
            'https://example.com/avatar.jpg',
            'DEV',
            new \DateTimeImmutable('2023-05-15 10:00:00')
        );

        $pdo = $this->createMock(\PDO::class);

        $repository = new AthletePdoRepository($pdo);

        $this->expectException(\RuntimeException::class);
        $repository->update($athlete);
    }

    public function testGetById(): void
    {
        $athlete = new Athlete(
            1,
            'x',
            'y',
            132,
            'test',
            'John',
            'Doe',
            'M',
            'https://example.com/avatar.jpg',
            'DEV',
            new \DateTimeImmutable('2023-05-15 00:00:00'),
            Team::IUBENDA,
        );

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['id', 1, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->never())
            ->method('bindValue');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetch')
            ->with()
            ->willReturn([
                'id' => '1',
                'accessToken' => 'x',
                'refreshToken' => 'y',
                'stravaId' => '132',
                'username' => 'test',
                'firstname' => 'John',
                'lastname' => 'Doe',
                'sex' => 'M',
                'avatar' => 'https://example.com/avatar.jpg',
                'department' => 'DEV',
                'joinDate' => '2023-05-15',
                'team' => 'iubenda'
            ]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new AthletePdoRepository($pdo);

        $athleteById = $repository->getById(1);

        $this->assertEquals($athlete, $athleteById);
    }

    public function testGetByIdThrowsExceptionWhenAthleteDoesNotExist(): void
    {
        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['id', 1, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->never())
            ->method('bindValue');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetch')
            ->with()
            ->willReturn(false);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new AthletePdoRepository($pdo);

        $this->expectException(AthleteNotFoundException::class);
        $repository->getById(1);
    }

    public function testGetByStravaId(): void
    {
        $athlete = new Athlete(
            1,
            'x',
            'y',
            132,
            'test',
            'John',
            'Doe',
            'M',
            'https://example.com/avatar.jpg',
            'DEV',
            new \DateTimeImmutable('2023-05-15 00:00:00'),
            Team::WEBNODE,
        );

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['id', 132, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->never())
            ->method('bindValue');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetch')
            ->with()
            ->willReturn([
                'id' => '1',
                'accessToken' => 'x',
                'refreshToken' => 'y',
                'stravaId' => '132',
                'username' => 'test',
                'firstname' => 'John',
                'lastname' => 'Doe',
                'sex' => 'M',
                'avatar' => 'https://example.com/avatar.jpg',
                'department' => 'DEV',
                'joinDate' => '2023-05-15',
                'team' => 'webnode',
            ]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new AthletePdoRepository($pdo);

        $athleteById = $repository->getByStravaId(132);

        $this->assertEquals($athlete, $athleteById);
    }

    public function testGetByStravaIdThrowsExceptionWhenAthleteDoesNotExist(): void
    {
        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['id', 132, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->never())
            ->method('bindValue');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetch')
            ->with()
            ->willReturn(false);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new AthletePdoRepository($pdo);

        $this->expectException(AthleteNotFoundException::class);
        $repository->getByStravaId(132);
    }

    public function testFindAll(): void
    {
        $athlete1 = new Athlete(
            1,
            'x',
            'y',
            132,
            'test',
            'John',
            'Doe',
            'M',
            'https://example.com/avatar.jpg',
            'DEV',
            new \DateTimeImmutable('2023-05-15 00:00:00'),
            Team::WEBNODE,
        );

        $athlete2 = new Athlete(
            2,
            'xx',
            'yy',
            1324,
            'test1',
            'John1',
            'Doe1',
            'F',
            'https://example.com/avatar1.jpg',
            'CC',
            new \DateTimeImmutable('2023-05-16 00:00:00')
        );

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->never())
            ->method('bindParam');
        $stmt
            ->expects($this->never())
            ->method('bindValue');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetchAll')
            ->with()
            ->willReturn([
                    [
                        'id' => '1',
                        'accessToken' => 'x',
                        'refreshToken' => 'y',
                        'stravaId' => '132',
                        'username' => 'test',
                        'firstname' => 'John',
                        'lastname' => 'Doe',
                        'sex' => 'M',
                        'avatar' => 'https://example.com/avatar.jpg',
                        'department' => 'DEV',
                        'joinDate' => '2023-05-15',
                        'team' => 'webnode',
                    ],
                    [
                        'id' => '2',
                        'accessToken' => 'xx',
                        'refreshToken' => 'yy',
                        'stravaId' => '1324',
                        'username' => 'test1',
                        'firstname' => 'John1',
                        'lastname' => 'Doe1',
                        'sex' => 'F',
                        'avatar' => 'https://example.com/avatar1.jpg',
                        'department' => 'CC',
                        'joinDate' => '2023-05-16',
                        'team' => 'webnode',
                    ],
                ]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new AthletePdoRepository($pdo);

        $athletes = $repository->findAll();

        $this->assertEquals([$athlete1, $athlete2], $athletes);
    }

    public function testFindReturnsEmptyResultOnFetchAllFail(): void
    {
        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->never())
            ->method('bindParam');
        $stmt
            ->expects($this->never())
            ->method('bindValue');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetchAll')
            ->with()
            ->willReturn([]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new AthletePdoRepository($pdo);

        $athletes = $repository->findAll();

        $this->assertEquals([], $athletes);
    }
}
