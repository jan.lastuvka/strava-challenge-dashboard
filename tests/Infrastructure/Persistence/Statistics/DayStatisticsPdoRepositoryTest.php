<?php

declare(strict_types=1);

namespace Tests\Infrastructure\Persistence\Statistics;

use App\Domain\Athlete\Athlete;
use App\Domain\Athlete\Team;
use App\Domain\Categories\Category;
use App\Domain\Statistics\DayStatistic;
use App\Infrastructure\Persistence\Statistics\DayStatisticsPdoRepository;
use Tests\TestCase;

final class DayStatisticsPdoRepositoryTest extends TestCase
{
    public function testItCreatesDayStatistics(): void
    {
        $athlete = new Athlete(1);
        $dateTime = new \DateTimeImmutable('2023-05-15');
        $entity = new DayStatistic(
            null,
            $athlete,
            Category::RUN,
            $dateTime,
            10,
            20,
            100,
            50,
            2
        );

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(6))
            ->method('bindParam')
            ->willReturnMap([
                ['athleteId', 1, \PDO::PARAM_STR, 0, null, true,],
                ['distance', 10.0, \PDO::PARAM_STR, 0, null, true,],
                ['time', 20.0, \PDO::PARAM_STR, 0, null, true,],
                ['calories', 100.0, \PDO::PARAM_STR, 0, null, true,],
                ['elevation', 50.0, \PDO::PARAM_STR, 0, null, true,],
                ['activitiesCount', 2, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->exactly(2))
            ->method('bindValue')
            ->willReturnMap([
                ['category', 1, \PDO::PARAM_STR, true,],
                ['date', '2023-05-15', \PDO::PARAM_STR, true,],
            ]);
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);
        $pdo->method('lastInsertId')->willReturn("1");

        $repository = new DayStatisticsPdoRepository($pdo);

        $repository->create($entity);

        $this->assertEquals(1, $entity->id);
    }

    public function testCreateThrowsExceptionWhenIdIsNotNull(): void
    {
        $athlete = new Athlete(1);
        $dateTime = new \DateTimeImmutable('2023-05-15');
        $entity = new DayStatistic(
            2,
            $athlete,
            Category::RUN,
            $dateTime,
            10,
            20,
            100,
            50
        );

        $pdo = $this->createMock(\PDO::class);
        $repository = new DayStatisticsPdoRepository($pdo);

        $this->expectException(\RuntimeException::class);
        $repository->create($entity);
    }

    public function testItUpdatesDayStatistics(): void
    {
        $athlete = new Athlete(1);
        $dateTime = new \DateTimeImmutable('2023-05-15');
        $entity = new DayStatistic(
            2,
            $athlete,
            Category::RUN,
            $dateTime,
            10,
            20,
            100,
            50,
            1
        );

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(6))
            ->method('bindParam')
            ->willReturnMap([
                ['id', 2, \PDO::PARAM_STR, 0, null, true,],
                ['distance', 10.0, \PDO::PARAM_STR, 0, null, true,],
                ['time', 20.0, \PDO::PARAM_STR, 0, null, true,],
                ['calories', 100.0, \PDO::PARAM_STR, 0, null, true,],
                ['elevation', 50.0, \PDO::PARAM_STR, 0, null, true,],
                ['activitiesCount', 1, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt->expects($this->never())->method('bindValue');
        $stmt->expects($this->never())->method('bindColumn');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new DayStatisticsPdoRepository($pdo);

        $repository->update($entity);

        $this->assertEquals(2, $entity->id);
    }

    public function testUpdateThrowsExceptionWhenIdIsNull(): void
    {
        $athlete = new Athlete(1);
        $dateTime = new \DateTimeImmutable('2023-05-15');
        $entity = new DayStatistic(
            null,
            $athlete,
            Category::RUN,
            $dateTime,
            10,
            20,
            100,
            50
        );

        $pdo = $this->createMock(\PDO::class);
        $repository = new DayStatisticsPdoRepository($pdo);

        $this->expectException(\RuntimeException::class);
        $repository->update($entity);
    }

    public function testFindOne(): void
    {
        $athlete = new Athlete(3);
        $dateTime = new \DateTimeImmutable('2023-05-15');
        $category = Category::RUN;

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['athleteId', 3, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->exactly(2))
            ->method('bindValue')
            ->willReturnMap([
                ['categoryId', 1, \PDO::PARAM_STR, true,],
                ['date', '2023-05-15', \PDO::PARAM_STR, true,],
            ]);
        $stmt->expects($this->never())->method('bindColumn');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetch')
            ->with()
            ->willReturn([
                'id' => 2,
                'athleteId' => 3,
                'category' => 1,
                'date' => '2023-05-15',
                'distance' => '10',
                'time' => '20',
                'calories' => '30',
                'elevation' => '40',
            ]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new DayStatisticsPdoRepository($pdo);

        $entity = $repository->findOne($athlete, $dateTime, $category);

        $this->assertEquals(2, $entity->id);
        $this->assertEquals($dateTime, $entity->date);
        $this->assertEquals($category, $entity->category);
        $this->assertEquals($athlete, $entity->athlete);
        $this->assertEquals(10.0, $entity->distance);
        $this->assertEquals(20.0, $entity->time);
        $this->assertEquals(30.0, $entity->calories);
        $this->assertEquals(40.0, $entity->elevation);
    }

    public function testFindOneWhenFetchReturnsFalse(): void
    {
        $athlete = new Athlete(3);
        $dateTime = new \DateTimeImmutable('2023-05-15');
        $category = Category::RUN;

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['athleteId', 3, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->exactly(2))
            ->method('bindValue')
            ->willReturnMap([
                ['categoryId', 1, \PDO::PARAM_STR, true,],
                ['date', '2023-05-15', \PDO::PARAM_STR, true,],
            ]);
        $stmt->expects($this->never())->method('bindColumn');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetch')
            ->with()
            ->willReturn(false);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new DayStatisticsPdoRepository($pdo);

        $entity = $repository->findOne($athlete, $dateTime, $category);

        $this->assertEquals(null, $entity);
    }

    public function testFindByCategory(): void
    {
        $athlete = new Athlete(3);
        $fromDateTime = new \DateTimeImmutable('2023-05-15');
        $toDateTime = new \DateTimeImmutable('2023-05-30');
        $category = Category::RUN;

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['athleteId', 3, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->exactly(3))
            ->method('bindValue')
            ->willReturnMap([
                ['categoryId', 1, \PDO::PARAM_STR, true,],
                ['from', '2023-05-15', \PDO::PARAM_STR, true,],
                ['to', '2023-05-30', \PDO::PARAM_STR, true,],
            ]);
        $stmt->expects($this->never())->method('bindColumn');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetchAll')
            ->with()
            ->willReturn([
                [
                    'id' => 2,
                    'category' => 1,
                    'athleteId' => 3,
                    'date' => '2023-05-15',
                    'distance' => '10',
                    'time' => '20',
                    'calories' => '30',
                    'elevation' => '40',
                ],
                [
                    'id' => 3,
                    'category' => 1,
                    'athleteId' => 3,
                    'date' => '2023-05-16',
                    'distance' => '50',
                    'time' => '60',
                    'calories' => '70',
                    'elevation' => '80',
                ]
            ]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new DayStatisticsPdoRepository($pdo);

        $entities = $repository->findByCategory($athlete, $fromDateTime, $toDateTime, $category);
        $this->assertCount(2, $entities);

        $entity = $entities[0];

        $this->assertEquals(2, $entity->id);
        $this->assertEquals('2023-05-15', $entity->date->format('Y-m-d'));
        $this->assertEquals($category, $entity->category);
        $this->assertEquals($athlete, $entity->athlete);
        $this->assertEquals(10.0, $entity->distance);
        $this->assertEquals(20.0, $entity->time);
        $this->assertEquals(30.0, $entity->calories);
        $this->assertEquals(40.0, $entity->elevation);
    }

    public function testFindByCategoryWhenFetchAllReturnsEmptyArray(): void
    {
        $athlete = new Athlete(3);
        $fromDateTime = new \DateTimeImmutable('2023-05-15');
        $toDateTime = new \DateTimeImmutable('2023-05-30');
        $category = Category::RUN;

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['athleteId', 3, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->exactly(3))
            ->method('bindValue')
            ->willReturnMap([
                ['categoryId', 1, \PDO::PARAM_STR, true,],
                ['from', '2023-05-15', \PDO::PARAM_STR, true,],
                ['to', '2023-05-30', \PDO::PARAM_STR, true,],
            ]);
        $stmt->expects($this->never())->method('bindColumn');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetchAll')
            ->with()
            ->willReturn([]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new DayStatisticsPdoRepository($pdo);

        $entities = $repository->findByCategory($athlete, $fromDateTime, $toDateTime, $category);
        $this->assertIsArray($entities);
        $this->assertCount(0, $entities);
    }

    public function testFind(): void
    {
        $athlete = new Athlete(3);
        $fromDateTime = new \DateTimeImmutable('2023-05-15');
        $toDateTime = new \DateTimeImmutable('2023-05-30');

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['athleteId', 3, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->exactly(2))
            ->method('bindValue')
            ->willReturnMap([
                ['from', '2023-05-15', \PDO::PARAM_STR, true,],
                ['to', '2023-05-30', \PDO::PARAM_STR, true,],
            ]);
        $stmt->expects($this->never())->method('bindColumn');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetchAll')
            ->with()
            ->willReturn([
                [
                    'id' => 2,
                    'category' => 1,
                    'athleteId' => 3,
                    'date' => '2023-05-15',
                    'distance' => '10',
                    'time' => '20',
                    'calories' => '30',
                    'elevation' => '40',
                    'activitiesCount' => 2,
                ],
                [
                    'id' => 3,
                    'category' => 2,
                    'athleteId' => 3,
                    'date' => '2023-05-15',
                    'distance' => '50',
                    'time' => '60',
                    'calories' => '70',
                    'elevation' => '80',
                    'activitiesCount' => 1,
                ]
            ]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new DayStatisticsPdoRepository($pdo);

        $entities = $repository->find($athlete, $fromDateTime, $toDateTime);
        $this->assertCount(2, $entities);

        $entity = $entities[0];

        $this->assertEquals(2, $entity->id);
        $this->assertEquals('2023-05-15', $entity->date->format('Y-m-d'));
        $this->assertEquals(Category::RUN, $entity->category);
        $this->assertEquals($athlete, $entity->athlete);
        $this->assertEquals(10.0, $entity->distance);
        $this->assertEquals(20.0, $entity->time);
        $this->assertEquals(30.0, $entity->calories);
        $this->assertEquals(40.0, $entity->elevation);
        $this->assertEquals(2, $entity->activitiesCount);
    }

    public function testFindWhenFetchAllReturnsEmptyArray(): void
    {
        $athlete = new Athlete(3);
        $fromDateTime = new \DateTimeImmutable('2023-05-15');
        $toDateTime = new \DateTimeImmutable('2023-05-30');

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(1))
            ->method('bindParam')
            ->willReturnMap([
                ['athleteId', 3, \PDO::PARAM_STR, 0, null, true,],
            ]);
        $stmt
            ->expects($this->exactly(2))
            ->method('bindValue')
            ->willReturnMap([
                ['from', '2023-05-15', \PDO::PARAM_STR, true,],
                ['to', '2023-05-30', \PDO::PARAM_STR, true,],
            ]);
        $stmt->expects($this->never())->method('bindColumn');
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetchAll')
            ->with()
            ->willReturn([]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new DayStatisticsPdoRepository($pdo);

        $entities = $repository->find($athlete, $fromDateTime, $toDateTime);
        $this->assertIsArray($entities);
        $this->assertCount(0, $entities);
    }

    public function testItGetsYearTeamDistanceStatistics(): void
    {
        $team = Team::WEBNODE;
        $year = 2023;
        $minDuration = 55;

        $stmt = $this->createMock(\PDOStatement::class);
        $stmt
            ->expects($this->exactly(4))
            ->method('bindValue')
            ->willReturnMap([
                ['team', $team->value, \PDO::PARAM_STR, true,],
                ['minActivityTime', $minDuration, \PDO::PARAM_STR, true,],
                ['minDatetime', "$year-01-01", \PDO::PARAM_STR, true,],
                ['maxDatetime', "$year-12-31", \PDO::PARAM_STR, true,],
            ]);
        $stmt
            ->expects($this->exactly(1))
            ->method('execute')
            ->with()
            ->willReturn(true);
        $stmt
            ->expects($this->exactly(1))
            ->method('fetch')
            ->with()
            ->willReturn(['distance' => 10000]);

        $pdo = $this->createMock(\PDO::class);
        $pdo->method('prepare')->willReturn($stmt);

        $repository = new DayStatisticsPdoRepository($pdo);

        $distance = $repository->getYearTeamDistanceStatistics($team, $year, $minDuration);
        $this->assertEquals(10, $distance);
    }
}
