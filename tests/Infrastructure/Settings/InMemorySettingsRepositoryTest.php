<?php

declare(strict_types=1);

namespace Tests\Infrastructure\Settings;

use App\Infrastructure\Settings\InMemorySettingsRepository;
use Tests\TestCase;

final class InMemorySettingsRepositoryTest extends TestCase
{
    public function testItGetsSettings(): void
    {
        $configuration = ['test' => 'aaa', 'array' => ['a']];

        $settings = new InMemorySettingsRepository($configuration);

        $this->assertEquals('aaa', $settings->get('test'));
        $this->assertEquals(['a'], $settings->get('array'));
        $this->assertEquals(null, $settings->get('unknown'));
        $this->assertEquals('a', $settings->get('unknown', 'a'));
        $this->assertEquals($configuration, $settings->get());
    }
}
